(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('SignUpController', SignUpController);

	SignUpController.$inject = ['$log', '$scope', '$rootScope', '$state', 'ngToast',
		'seoProvider', 'ApiService', '$location', 'UserFactory', 'session'];

	function SignUpController ($log, $scope, $rootScope, $state, ngToast,
							 seoProvider, ApiService, $location, UserFactory, session) {
		$log.log ('SignUpController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.signUp = signUp;

		vm.userData = UserFactory.getUserData();
		vm.user = {
			password: null,
			passwordConfirm: null,
			tos: null
		};

		$scope.login = null;
		$scope.password = null;
		$scope.password_2 = null;
		$scope.loggined = false;


		function signUp() {
			if ((vm.user.password == null)||(vm.user.passwordConfirm == null)||(vm.user.password !== vm.user.passwordConfirm)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Passwords mismatch'
				});
				return false;
			}
			if (vm.user.tos !== true) {
				ngToast.create({
				  className: 'danger',
				  content: 'You must accept Terms and Conditions'
				});
				return false;
			}
			if(vm.signUpForm.$valid){
				ApiService.signup(vm.userData.login, vm.user.password).then(signUpCallback, null);
			}
			else {
				ngToast.create({
				  className: 'danger',
				  content: 'Email required'
				});
			}
		}

		function signUpCallback(data){
			ApiService.signin(vm.userData.login, vm.user.password).then(signInCallback, null);
		}

		function signInCallback(data){
			vm.userData.token = data.data.data.token;

			UserFactory.setIsLoggedIn(true);
			UserFactory.setUserData(vm.userData);

			session.create(data.data.data.token, vm.userData.login);
			$state.go('account');
		}
	}

})(window.angular);








