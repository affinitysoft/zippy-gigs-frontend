(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('HomeController', HomeController);

	HomeController.$inject = ['$log', '$state',
		'seoProvider', 'UserFactory',  '$sce', '$location', '$anchorScroll'];

	function HomeController ($log, $state,
							 seoProvider, UserFactory, $sce, $location, $anchorScroll) {
		$log.log ('HomeController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.userData = UserFactory.getUser();

		vm.goToSearch = goToSearch;
		
		vm.config = {
                preload: 'none',
                sources: [{src: '../../assets/img/ZippyGigs - Intro.mp4', type: 'video/mp4'}],
                theme: {
          			url: 'https://unpkg.com/videogular@2.1.2/dist/themes/default/videogular.css'
                }
            };

		function goToSearch() {
			$state.go("search");
		}
	}

})(window.angular);






