var gulp = require('gulp');
var chalk = require('chalk');
var gutil = require('gulp-util');
var imageMin = require('gulp-imagemin');
var csso = require('gulp-csso');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var gzip = require('gulp-gzip');
var cache = require('gulp-angular-templatecache');
var livereload = require('gulp-livereload');
var filelog = require('gulp-filelog');
var run = require('gulp-run-command').default;

var config = {
    name: 'zippy',
    appPath: './app/',
    appPathJs: ['./app/**/*.js', '!./app/assets/**/*.js', '!./app/js.js'],
    appPathLibs: [
      './app/assets/libs/jquery/dist/jquery.js',
      './app/assets/libs/angular/angular.js',
      './app/assets/libs/angular-ui-router/release/angular-ui-router.js',
      './app/assets/libs/angular-ui-notification/dist/angular-ui-notification.js',
      './app/assets/libs/angular-sanitize/angular-sanitize.js',
      './app/assets/libs/videogular/videogular.js',
      './app/assets/libs/videogular-controls/vg-controls.js',
      './app/assets/libs/videogular-overlay-play/vg-overlay-play.js',
      './app/assets/libs/ngToast/dist/ngToast.js',
      './app/assets/libs/bootstrap/dist/js/bootstrap.js'
    ],
    appPathCss: [
      './app/assets/css/**/*.css', '!./app/assets/css/main.css'
    ],
    appPathTemplates: ['./app/index.html', './app/components/**/*.html', './app/directives/*.html'],
    appPathImg: './app/assets/img/**/*',
    essentialFiles: './app/index.html',
    devPathCss: './app/assets/styles/'
};

/*dev default*/

// CSS

gulp.task('css', function () {
    return gulp.src('./app/assets/css/style.css')
      .pipe(concat('main.css'))
      .on('error', onErrorRegister)
      .pipe(gulp.dest('./app/assets/css'))
      .on('end', onEndRegister('css'));
});

// JS

gulp.task('appJs', function () {
    gulp.src(config.appPathJs)
        .pipe(concat('js.js'))
        .on('error', onErrorRegister)
        .pipe(gulp.dest('./app'))
        .pipe(livereload())
        .on('end', onEndRegister('appJs'));
});

gulp.task('libsJs',  function () {
    gulp.src(config.appPathLibs)
        .pipe(concat('libs.js'))
        .on('error', onErrorRegister)
        .pipe(gulp.dest('./app/assets/libs'))
        .pipe(livereload())
        .on('end', onEndRegister('libsJs'));
});

gulp.task('cache', function () {
    gulp.src(config.appPathTemplates)
        .pipe(cache({standalone: true}))
        .on('error', onErrorRegister)
        .pipe(gulp.dest('./app/components'))
        .pipe(livereload())
        .on('end', onEndRegister('cache'));
});

// Watch

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(config.appPathCss, {interval: 1000}, ['css']);
  livereload.listen();
    gulp.watch(config.appPathJs, {interval: 300}, ['appJs']);
  livereload.listen();
    gulp.watch(config.appPathLibs, {interval: 1000}, ['libsJs']);
  livereload.listen();
    gulp.watch(config.appPathTemplates, {interval: 300}, ['cache']);
});

// Builds

gulp.task('build', function () {

  gulp.src ('./app/assets/css/main.css')
    .pipe (csso ('main.css'))
    .on ('error', onErrorRegister)
    .pipe (gulp.dest ('./app/assets/css'))
    .on ('end', onEndRegister ('csso'));

  gulp.src ('./app/assets/libs/libs.js')
    .pipe (uglify ())
    .on ('error', onErrorRegister)
    .pipe (gulp.dest ('./app/assets/libs'))
    .pipe (livereload ())
    .on ('end', onEndRegister ('libsJs'));

  gulp.src ('./app/js.js')
    .pipe (uglify ())
    .on ('error', onErrorRegister)
    .pipe (gulp.dest ('./app'))
    .on ('end', onEndRegister ('appJs'));

  gulp.src (config.appPathImg)
    .pipe (imageMin ())
    .on ('error', onErrorRegister)
    .pipe (gulp.dest ('./app/assets/img'));

});

gulp.task('server', run('http-server -a localhost -p 8000 -c-1 ./app'))


gulp.task('default', [
    'css',
    'appJs',
    'libsJs',
    'cache',
    'watch',
]);

gulp.task('npm-start', [
    'server',
    'watch',
]);

function swallowError(error) {
    console.log(error.toString());
    this.emit('end');
}

function onErrorRegister(error) {
    swallowError(error);
}

function onEndRegister(taskName) {
    return function () {
        gutil.log('Task', '\'' + chalk.cyan(taskName) + '\' is done.');
    };
}