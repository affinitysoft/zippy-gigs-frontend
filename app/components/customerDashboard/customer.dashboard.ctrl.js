/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
  'use strict';

  angular
    .module('zippyApp')
    .controller('CustomerDashboardController', CustomerDashboardController);

  CustomerDashboardController.$inject = ['$log', '$scope', '$rootScope',
    'seoProvider', 'ApiService', 'UserFactory', 'NotificationService', 'NOTIFICATION_EVENT', '$state'];

  function CustomerDashboardController($log, $scope, $rootScope,
                                     seoProvider, ApiService, UserFactory, NotificationService, NOTIFICATION_EVENT, $state) {
    $log.log('CustomerDashboardController');

    var vm = this;

    seoProvider.setTitle ('Zippy');
    seoProvider.setDescription ('Zippy');
    seoProvider.setKeywords ('Zippy');
    seoProvider.setAuthor ('Zippy');

    vm.onUserDataUpdated = onUserDataUpdated;
    vm.postGig = postGig;
    vm.selectedGig = selectedGig;

    vm.userData = UserFactory.getUserData();

    vm.jobTypesData = {};
    vm.selectedJob = '';
    vm.selectedHour = 0;
    vm.finishedGigs = [];
    vm.description = '';
    vm.api_img = 'http://host.zippygigs.com/api/v1/client';

    checkPermission();
    initData();


    NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

    function onUserDataUpdated(){
      vm.userData = UserFactory.getUserData();
      initData();
    }

    function checkPermission(){
      if(vm.userData.type === '2' || vm.userData.type === 2){
        $state.transitionTo('home');
      }
    }

    function initData(){
      if(vm.userData.login){
        checkPermission();
        vm.userData.type = String(vm.userData.type);
      }
    }

    ApiService.getJobTypes().then(getJobTypesCallback, null);
    ApiService.getOrders({status: 4, order_type: "customer"}).then(getOrdersCallback, null);

    function postGig(){
      $rootScope.midLoader = true;
      console.log(vm.selectedJob);
      console.log(vm.selectedHour);
      console.log(vm.description);
      var button = $(event.target);
      button.find(".success").fadeIn("slow");
      setTimeout(function(){
        button.find(".success").fadeOut("slow");
      }, 2000);
      $(event.target).parent().parent().find(".desc").toggle(500);
      ApiService.postGig({type: vm.selectedJob.id, description: vm.description, price: vm.selectedHour}).then(postGigCallback, null);
    }

    function selectedGig(item){
      vm.selectedGigItem = item;
    }

    //callbacks

    function getJobTypesCallback(resp){
      vm.jobTypesData = resp.data.data.job_types;
      vm.selectedJob = vm.jobTypesData[0];
    }

    function getOrdersCallback(resp){
      vm.finishedGigs = resp.data.data.orders;
      console.log(vm.finishedGigs);
      console.log(vm.jobTypesData);
    }

    function postGigCallback(resp){
      vm.description = '';
      $rootScope.midLoader = false;
    }

  }

})(window.angular);
