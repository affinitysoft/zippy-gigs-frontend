/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .service('ConfigService', ConfigService);

    ConfigService.$inject = ['$q', '$http', '$state',
        'Notification', 'UserFactory'];

    function ConfigService($q, $http, $state,
                        Notification, UserFactory) {

        var _activeRequestsArr = [];

        var service = {
            getUrl: getUrl,
            sendRequest: sendRequest
        };

        return service;

        function getUrl(_path) {
            return "http://host.zippygigs.com/" + _path; // "http://127.0.0.1:5000/" + _path;
        }

        function sendRequest(_method, _url, _data, _params, _ignoreMultiple, _formData) {
            var deferred = $q.defer();

            if (isActiveRequest(getUrl(_url)) && !_ignoreMultiple && !_formData) {
                deferred.reject();
            }
            else {

                var req = {
                    method: _method,
                    url: getUrl(_url),
                    data: _data,
                    params: _params
                };

                if (_formData) {
                    req.headers = {
                        'Content-Type': undefined
                    };
                }

                pushRequest(req.url);

                $http(req).then(

                    function (response) {

                        popRequest(response.config.url);
                        deferred.resolve(response);
                    },

                    function (response) {

                        deferred.reject(response);
                        popRequest(response.config.url);

                        var _errText = '';

                        if (response.data.error) {
                          _errText = response.data.error;
                            if(_errText === 'Unauthorized Access'){
                                UserFactory.setIsLoggedIn(false);
                                localStorage.removeItem("token");
                                localStorage.removeItem("login");
                                localStorage.removeItem("token_end");
                                $state.reload();
                            }
                        }
                        else {
                            _errText = 'Неизвестная ошибка, попробуйте снова!';
                        }

                        console.log(_errText);

                    });

            }

            return deferred.promise;
        }

        function isActiveRequest(_url) {
            return _activeRequestsArr.indexOf(_url) > -1;
        }

        function pushRequest(_url) {
            _activeRequestsArr.push(_url);
        }

        function popRequest(_url) {
            var reqIdx = _activeRequestsArr.indexOf(_url);
            _activeRequestsArr.splice(reqIdx, 1);
        }
    }

})(window.angular);
