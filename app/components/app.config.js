(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .run(RunConfig)
        .config(MainConfig);

    RunConfig.$inject = ['$rootScope' ,'session', 'UserFactory', '$state'];

    MainConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function RunConfig($rootScope, session, UserFactory, $state){
      if(session.check()){
        session.create(localStorage.token, localStorage.login);
      }

        $rootScope.$on('$locationChangeStart', function(event, next, current) {
             console.log("location change from to:", current, next)
             console.log("$state:", $state.current)             
        });

        $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
            if (toState.authenticate && UserFactory.isUserLoggedIn() === false && !session.check()){
                $state.transitionTo("signIn");
                event.preventDefault();
            }
            if (toState.name == 'mygigs') {
                var userData = UserFactory.getUserData()
                if (!userData.vendor_membership) {
                    $state.transitionTo('membership');
                    event.preventDefault();
                }
            }
            if (UserFactory.isFirstLogin() === true) {
                $state.transitionTo('account');
                event.preventDefault();
            }

        });

    }

    function MainConfig($stateProvider, $urlRouterProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        $stateProvider

            .state('home', {
                url: '/',
                templateUrl: 'home/home.html',
                controller: 'HomeController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('contact', {
                url: '/contact',
                templateUrl: 'contact/contact.html',
                authenticate: false
            })
            .state('about', {
                url: '/about',
                templateUrl: 'about/about.html',
                authenticate: false
            })
            .state('privacy_notice', {
                url: '/privacy-notice',
                templateUrl: 'privacy_notice/privacy_notice.html',
                authenticate: false
            })
            .state('account', {
                url: '/account',
                templateUrl: 'account/account.html',
                controller : 'AccountController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('faq', {
                url: '/faq',
                templateUrl: 'faq/faq.html',
                controller: 'FaqController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('signIn', {
                url: '/sign-in',
                templateUrl: 'signin/signin.html',
                controller: 'SignInController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('terms', {
                url: '/terms',
                templateUrl: 'terms/terms.html',
                authenticate: false
            })
            .state('signUp', {
                url: '/sign-up',
                templateUrl: 'signup/signup.html',
                controller: 'SignUpController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('forgot_password', {
                url: '/forgot-password',
                templateUrl: 'forgot_password/forgot_password.html',
                controller: 'ForgotPasswordController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('reset_password', {
                url: '/reset/:token',
                templateUrl: 'reset_password/reset_password.html',
                controller: 'ResetPasswordController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('membership', {
                url: '/membership',
                templateUrl: 'membership/membership.html',
                controller: 'MembershipController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('search', {
                url: '/search',
                templateUrl: 'search/search.html',
                controller: 'SearchController',
                controllerAs: 'vm',
                authenticate: true
            })
          .state('mygigs', {
                url: '/my-gigs',
                templateUrl: 'mygigs/mygigs.html',
                controller: 'MygigsController',
                controllerAs: 'vm',
                authenticate: true
          })
            .state('vendorDash', {
                url: '/vendor',
                templateUrl: 'vendorDashboard/vendor_dashboard.html',
                controller: 'VendorDashboardController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('customDash', {
                url: '/customer',
                templateUrl: 'customerDashboard/customer_dashboard.html',
              controller: 'CustomerDashboardController',
              controllerAs: 'vm',
                authenticate: true
            })
            .state('customer_orders', {
                url: '/customer/orders',
                templateUrl: 'customer_orders/customer_orders.html',
                controller: 'OrdersController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('vendor_orders', {
                url: '/vendor/orders',
                templateUrl: 'vendor_orders/vendor_orders.html',
                controller: 'OrdersController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('vendor_profile', {
                url: '/vendor/:vendor_id',
                templateUrl: 'vendor_profile/vendor_profile.html',
                controller: 'VendorProfileController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('customer_profile', {
                url: '/customer/:customer_id',
                templateUrl: 'customer_profile/customer_profile.html',
                controller: 'CustomerProfileController',
                controllerAs: 'vm',
                authenticate: true
            });

        $urlRouterProvider.otherwise('/');

    }

})(window.angular);

