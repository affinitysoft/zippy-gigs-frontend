(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('MainController', MainController);

	MainController.$inject = ['$log', '$scope', '$state', '$rootScope', '$window', 'ngToast',
		'seoProvider', 'UserFactory', 'NotificationService', 'ConfigService', 'NOTIFICATION_EVENT', 'session'];

	function MainController ($log, $scope, $state, $rootScope, $window,
							 ngToast, seoProvider, UserFactory, NotificationService, ConfigService, NOTIFICATION_EVENT, session) {
		$log.log ('MainController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.logOut = logOut;
		vm.onUserLoggedInOut = onUserLoggedInOut();
		vm.onUserDataUpdated = onUserDataUpdated;
		vm.userData = UserFactory.getUserData();
		vm.switchTo = switchTo;
		vm.accountType = localStorage.accountType ? localStorage.accountType : 'vendor';

		$rootScope.midLoader = false;

		NotificationService.register($scope, NOTIFICATION_EVENT.USER_LOGGED_IN_OUT, onUserLoggedInOut);

		NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);


		function onUserDataUpdated(){
		    vm.userData = UserFactory.getUserData();
		}

		function switchTo(accountType) {
			if (accountType == 'vendor') {
				vm.accountType = 'customer';
				localStorage.accountType = 'customer';
				$state.go("customDash");
			}
			else {
				vm.accountType = 'vendor';
				localStorage.accountType = 'vendor';
				$state.go("vendorDash");
			}
		}

		function onUserLoggedInOut(){
			vm.userLoggined = UserFactory.isUserLoggedIn();
		}

		function logOut() {
			delete localStorage.accountType;
			session.clear();
		}
	}

})(window.angular);















