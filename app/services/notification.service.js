(function(angular){
    angular
        .module('angularNotification', ['ng']);

    angular
        .module('angularNotification')
      .constant('NOTIFICATION_EVENT', {
        API_ERROR_UNAUTHORIZED: 'apiError:unauthorized',
        SESSION_CREATED: 'session:created',

        USER_LOGGED_IN_OUT: 'UserFactory:userLoggedOut',
        USER_DATA_UPDATED: 'UserFactory:userDataUpdated'
      })
        .service('NotificationService', [function () {

        var callbacks = {};

        var service = {
            register: addCallback,
            unregister: removeCallback,
            notify: notify
        };

        return service;

        function addCallback(scope, channel, cb) {
            if (!callbacks[channel]) {
                callbacks[channel] = [];
            }

            if (callbacks[channel].indexOf(cb) == -1) {
                callbacks[channel].push(cb);

                scope.$on('$destroy', function () {
                    removeCallback(channel, cb);
                });
            }
        }

        function removeCallback(channel, cb) {
            if (callbacks[channel]) {
                var cbidx = callbacks[channel].indexOf(cb);
                if (cbidx != -1) {
                    callbacks[channel].splice(cbidx, 1);
                }
            }
        }

        function notify(channel) {
            var notifyArgs = Array.prototype.slice.call(arguments, 1);

            if (callbacks[channel]) {
                callbacks[channel].forEach(function (cb) {
                    cb.apply(undefined, notifyArgs);
                });
            }
        }

    }]);
})(window.angular);
