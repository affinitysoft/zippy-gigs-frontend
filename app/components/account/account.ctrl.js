(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('AccountController', AccountController);

	AccountController.$inject = ['$log', '$scope', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'NotificationService', 'ConfigService', 'NOTIFICATION_EVENT', '$timeout'];

	function AccountController ($log, $scope, ngToast,
							   seoProvider, UserFactory, ApiService, NotificationService, ConfigService, NOTIFICATION_EVENT, $timeout) {
		$log.log ('AccountController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.accountSave = accountSave;
		vm.avatarSave = avatarSave;
		vm.onUserDataUpdated = onUserDataUpdated;
		$scope.uploadAvatar = uploadAvatar;

    	vm.userData = UserFactory.getUserData();

		vm.accountSaved = false;
		vm.api_img = ConfigService.getUrl('api/v1/client');
		vm.avatar = vm.api_img + vm.userData.avatar_url;
		document.getElementById('avatar').style.backgroundImage = 'url('+vm.avatar+')';	

		NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

		function onUserDataUpdated(){
			vm.userData = UserFactory.getUserData();
			if(vm.userData.login){
				vm.userData.type = String(vm.userData.type);
			}
			if (vm.userData.avatar_url) {
				vm.avatar = vm.api_img + vm.userData.avatar_url;
			}
		}

		function accountSave() {
			vm.accountSaved = true;
			console.log("vm.userData for saving:", vm.userData)
			ApiService.accountSave(vm.userData).then(accountSaveCallback, null);
		}

		function uploadAvatar(element) {
			$scope.$apply(function(scope) {
		        var photofile = element.files[0];
		        var reader = new FileReader();
			        reader.onload = function(e) {
			            // handle onload
			            vm.avatar = reader.result;
			            document.getElementById('avatar').style.backgroundImage = 'url('+vm.avatar+')';	
						var data = new FormData();
							data.append('avatar', photofile);
							ApiService.accountAvatar(data).then(accountAvatarCallback, null);
					};
			        reader.readAsDataURL(photofile);
			});
		}

		function avatarSave() {
			angular.element(document.getElementById('file_avatar')).click();
		}

//Callbacks

		function accountAvatarCallback(data){
			console.log("accountAvatarCallback:", data)
      		vm.userData.avatar_url = data.data.data.url;
      		vm.avatar = vm.api_img + vm.userData.avatar_url;
		}

		function accountSaveCallback(data){
			if (data.data.result === false) {
				ngToast.create({
				  className: 'danger',
				  content: data.data.error
				});
				$timeout(function(){
					vm.accountSaved = false;
				}, 2000);
			} else {
				$timeout(function(){
					vm.accountSaved = false;
					window.location.reload()
				}, 2000);
			}
		}

	}

})(window.angular);







