/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    angular
        .module('zippyApp')
        .service('session', ['$http', 'NotificationService', 'NOTIFICATION_EVENT', 'UserFactory', 'ApiService', '$state', session]);

    function session($http, NotificationService, NOTIFICATION_EVENT, UserFactory, ApiService, $state) {

        var date = new Date();
        var timestamp = Math.floor(date.getTime() / 1000);

        var service = {
            create: create,
            clear: clear,
            check: check
        };

        return service;

        function setAuthHeader(token) {
            $http.defaults.headers.common.Authorization = "Token " + token;
        }

        function create(_oauthToken, _login) {
            setAuthHeader(_oauthToken);
            localStorage.token = _oauthToken;
            localStorage.login = _login;
            localStorage.token_end = timestamp + (10 * 60);
            UserFactory.setIsLoggedIn(true);

            var obj = {};

            ApiService.accountGet().then(function(resp){
                obj = resp.data.data;
                obj.login = localStorage.login;
                UserFactory.setUserData(obj);
            });

            NotificationService.notify(NOTIFICATION_EVENT.USER_LOGGED_IN_OUT);
        }

        function clear() {
            setAuthHeader(null);

            UserFactory.setIsLoggedIn(false);
            UserFactory.resetData();

            localStorage.removeItem("token");
            localStorage.removeItem("login");
            localStorage.removeItem("token_end");
            localStorage.removeItem("accountType");

            NotificationService.notify(NOTIFICATION_EVENT.USER_LOGGED_IN_OUT);
            $state.go('home');
        }

        function check() {
            if(localStorage.token_end > timestamp){
                return true;
            }
            else{
                if (!localStorage.token_end) {
                    return false;
                }
                clear();
                return false;
            }
        }

    }

})(window.angular);