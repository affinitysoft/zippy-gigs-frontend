angular
    .module('zippyApp')
    .directive('loseBlurEvent', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                var el = element;
                element.blur(function(){
                    element.removeClass("editable");
                    element.prop('disabled', true);
                    });
            }
        };
    });