(function (angular) {
	'use strict';

	angular
		.module('zippyApp', [
			'ui.router',
			'ui-notification',
			'angularNotification',
			'seoProvider',
			'templates',
			'ngSanitize',
			'com.2fdevs.videogular',
        	'com.2fdevs.videogular.plugins.controls',
        	'com.2fdevs.videogular.plugins.overlayplay',
        	'ngToast'
		]);

})(window.angular);






