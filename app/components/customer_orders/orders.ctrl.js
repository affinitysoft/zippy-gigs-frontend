(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('OrdersController', OrdersController);

	OrdersController.$inject = ['$log', '$state',
		'seoProvider', 'UserFactory', 'ApiService'];

	function OrdersController ($log, $state,
							seoProvider, UserFactory, ApiService) {
		$log.log ('OrdersController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.api_img = 'http://host.zippygigs.com/api/v1/client';
		vm.orders = [];
		vm.status_select = 1;
		vm.data = {};

		vm.filterByStatus = filterByStatus;
		vm.filterByStatus(1);
		vm.showSubmitForm = showSubmitForm;
		vm.submitWork = submitWork;
		vm.payForGig = payForGig;


		function filterByStatus(status, accountType) {
			console.log("acc type:", accountType)
			vm.orders = [];
			vm.status_select = status;
			ApiService.getOrders({status: status, order_type: accountType}).then(getOrdersCallback, null);
		}

		function showSubmitForm(order) {
			$(event.target).parent().parent().find(".desc").toggle(500);
			$(event.target).parent().parent().find(".hours-worked").toggle(500);
			$(event.target).parent().parent().find(".response-rating").toggle(500);
			$(event.target).parent().parent().find(".satisfaction-rating").toggle(500);
			vm.data.order = order;
		}

		function submitWork() {
			console.log({
					order: vm.data.order,
					hours_worked: vm.data.hours_worked,
					vendor_feedback: vm.data.vendor_feedback,
					response_rating: parseInt(vm.data.response_rating),
					satisfaction_rating: parseInt(vm.data.satisfaction_rating)
				});
			ApiService.finishOrder({
				order: vm.data.order,
				vendor_feedback: vm.data.vendor_feedback,
				hours_worked: vm.data.hours_worked,
				response_rating: parseInt(vm.data.response_rating),
				satisfaction_rating: parseInt(vm.data.satisfaction_rating)
			}).then(finishOrderCallback, null);
		}

		function payForGig(order_id) {
			ApiService.payForWorkDone({
				order_id: order_id
			}).then(payForGigCallback, null)
		}

		// callbacks
		function getOrdersCallback(data){
			var orders = data.data.data.orders;
			orders.forEach(function(item){
				vm.orders.push(item);
			});
			$log.log(vm.orders);
		}

		function finishOrderCallback(data) {
			console.log("finishOrderResult:", data)
		}

		function payForGigCallback(data) {
			console.log("payForGigCallback:", data);
			vm.paymentSuccesUrl = data.data.data;
			console.log("vm.paymentSuccesUrl: ", vm.paymentSuccesUrl)
			if (vm.paymentSuccesUrl) {
				window.location = vm.paymentSuccesUrl;
			}
		}


// 		vm.status_select = 1;
// 		vm.orders = [null,[],[],[],[]];
//
// 		ApiService.getOrders().then(getOrdersCallback, null);
//
// 		function classSelected(status) {
// 			if (vm.status_select === status){
// 				return "selected";
// 			}
// 		}
//
// 		function changeSelected(status) {
// 			vm.status_select = status;
// 		}
//
// //callbacks
//
// 		function getOrdersCallback(data){
// 			var orders = data.data.data.orders;
// 			orders.forEach(function(item){
// 				var status = item.status;
// 				vm.orders[status].push(item);
// 			});
// 			$log.log(vm.orders);
// 		}

	}

})(window.angular);












