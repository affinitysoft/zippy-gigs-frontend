(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('SignInController', SignInController);

	SignInController.$inject = ['$log', '$state', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'session'];

	function SignInController ($log, $state, ngToast,
							 seoProvider, UserFactory, ApiService, session) {
		$log.log ('SignInController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.signIn = singIn;

		vm.userData = UserFactory.getUserData();

		vm.user = {};

		function singIn() {
			ApiService.signin(vm.userData.login, vm.user.password).then(signInCallback, null);
		}

		function signInCallback(data){
			console.log("data:", data)
			if ((!data.data.data)&&(data.data.error)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Name or Password is incorrect'
				});
			}
			else {
				session.create(data.data.data.token, vm.userData.login);
				$state.go('account');
			}
		}

	}

})(window.angular);





