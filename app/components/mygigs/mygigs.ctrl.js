(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('MygigsController', MygigsController);

  MygigsController.$inject = ['$log', '$state', '$scope',
		'seoProvider', 'UserFactory', 'ApiService', 'NotificationService', 'NOTIFICATION_EVENT'];

	function MygigsController ($log, $state, $scope,
							seoProvider, UserFactory, ApiService, NotificationService, NOTIFICATION_EVENT) {
		$log.log ('MygigsController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		//ViewFunctions

    	vm.onUserDataUpdated = onUserDataUpdated;
		vm.search = search;
		vm.selectJobType = selectJobType;
		vm.hire = hire;
		vm.createOrder = createOrder;
		vm.openCustomerProfile = openCustomerProfile;

		//ControllerData

        vm.userData = UserFactory.getUserData();

		vm.api_img = 'http://host.zippygigs.com/api/v1/client';
		vm.params = {
			radius: null,
			gig_type: null,
			price_gt: null,
			price_lt: null
		};

		vm.data = {};

        vm.gig_type = {};
		vm.gigs = {};
		vm.types = {};

		//InitFunctions

    function checkPermission(){
      if(vm.userData.type === '1' || vm.userData.type === 1){
        $state.transitionTo('home');
      }
    }
    checkPermission();

    NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

    function onUserDataUpdated(){
      vm.userData = UserFactory.getUserData();
      if(vm.userData.login){
        checkPermission();
      }
    }

	function openCustomerProfile(id) {
		$state.transitionTo('customer_profile', {'customer_id': id});
	}

    //FunctionDeclarations

		ApiService.getJobTypes().then(getJobTypesCallback, null);

		function selectJobType(gig_id, index, id, rate, event) {
			vm.hireData = {
				type: '',
				description: '',
				price: '',
				account: ''
			};
			if(vm.gigs[gig_id].job_types.length){
				if (vm.gigs[gig_id].job_types[index].id === id) {
					vm.gigs[gig_id].job = {
						type: id,
						price: rate,
						account: vm.userData.login
					};
				}
				else {
					vm.gigs[gig_id].job = {
						type: false,
						price: 10,
						account: vm.userData.login
					};
				}
			}
			$(event.target).parent().parent().find(".desc").toggle(500);
		}

		function search() {
		  vm.params.gig_type = vm.gig_type.id;
			ApiService.searchGigs(vm.params).then(searchCallback, null);
		}

		function hire(user, status) {
			vm.data.gig_id = user;
			vm.data.status_id = status;
			$(event.target).parent().parent().find(".desc").toggle(500);
		}

		function createOrder(){
			var button = $(event.target);
			button.find(".success").fadeIn("slow");
			setTimeout(function(){
				button.find(".success").fadeOut("slow");
			}, 2000);
			$(event.target).parent().parent().find(".desc").toggle(500);
			ApiService.createOrder(vm.data).then(createOrderCallback);
		}

//callbacks

		function getJobTypesCallback(data){
				var default_type = {
					id: null,
					title: '- Not Selected -'
				};

				vm.types = data.data.data.job_types;
				vm.types.unshift(default_type);
				search();
		}

		function searchHireCallback(){
			//TODO: move

		}

		function searchCallback(data){
			vm.gigs = data.data.data;
		}

		function createOrderCallback(){

		}

	}

})(window.angular);




