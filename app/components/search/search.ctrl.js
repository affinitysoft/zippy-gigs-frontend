(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('SearchController', SearchController);

	SearchController.$inject = ['$log', '$state', '$scope',
		'seoProvider', 'UserFactory', 'ApiService', 'NotificationService', 'NOTIFICATION_EVENT'];

	function SearchController ($log, $state, $scope,
							seoProvider, UserFactory, ApiService, NotificationService, NOTIFICATION_EVENT) {
		$log.log ('SearchController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		//ViewFunctions

    	vm.onUserDataUpdated = onUserDataUpdated;
		vm.search = search;
		vm.selectJobType = selectJobType;
		vm.hire = hire;
		vm.openVendorProfile = openVendorProfile;

		//ControllerData

    	vm.userData = UserFactory.getUserData();

		vm.api_img = 'http://host.zippygigs.com/api/v1/client';
		vm.params = {
			status: null,
			radius: null,
			job_type: null,
			hourly_rate_gt: null,
			hourly_rate_lt: null
		};

    	vm.job_type = {};
		vm.users = {};
		vm.types = {};

		//InitFunctions

    function checkPermission(){
      if(vm.userData.type === '2' || vm.userData.type === 2){
        $state.transitionTo('home');
      }
    }
    checkPermission();

    NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

    function onUserDataUpdated(){
      vm.userData = UserFactory.getUserData();
      if(vm.userData.login){
        checkPermission();
      }
    }

    //FunctionDeclarations

		ApiService.getJobTypes().then(getJobTypesCallback, null);

		function selectJobType(userid, index, id, rate, event) {
			vm.hireData = {
				type: '',
				description: '',
				price: '',
				account: ''
			};
			if(vm.users[userid].job_types.length){
				if (vm.users[userid].job_types[index].id === id) {
					vm.users[userid].job = {
						type: id,
						price: rate,
						account: vm.userData.login
					};
				}
				else {
					vm.users[userid].job = {
						type: false,
						price: 10,
						account: vm.userData.login
					};
				}
			}
			$(event.target).parent().parent().find(".desc").toggle(500);
		}

		function search() {
		    vm.params.job_type = vm.job_type.id;
			ApiService.search(vm.params).then(searchCallback, null);
		}

		function hire(user, event) {
			var button = $(event.target);
			button.find(".success").fadeIn("slow");
			setTimeout(function(){
				button.find(".success").fadeOut("slow");
			}, 2000);
			ApiService.searchHire(user.job).then(searchHireCallback, null);
		}

//callbacks

		function getJobTypesCallback(data){
				var default_type = {
					id: null,
					title: '- Not Selected -'
				};

				vm.types = data.data.data.job_types;
				vm.types.unshift(default_type);
				search();
		}

		function searchHireCallback(){
			//TODO: move

		}

		function searchCallback(data){
				vm.users = data.data.data;
			console.log("vm.users: ", vm.users)
		}

		function openVendorProfile(id) {
			$state.transitionTo('vendor_profile', {'vendor_id': id});
		}

	}

})(window.angular);




