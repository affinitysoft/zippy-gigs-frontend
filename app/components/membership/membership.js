(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .controller('MembershipController', MembershipController);

    MembershipController.$inject = ['$log', '$state',
        'seoProvider', 'UserFactory', 'ApiService'];

    function MembershipController ($log, $state, seoProvider,
                                   UserFactory, ApiService) {
        $log.log ('MembershipController');

        var vm = this;

        seoProvider.setTitle ('Zippy');
        seoProvider.setDescription ('Zippy');
        seoProvider.setKeywords ('Zippy');
        seoProvider.setAuthor ('Zippy');

        vm.createSubscription = createSubscription;
        vm.goToGigs = goToGigs;
        vm.userData = UserFactory.getUserData();
        vm.plan = 1

        getBraintreeClientToken();

        function getBraintreeClientToken() {
            ApiService.getBraintreeClientToken().then(getBtTokenCallback, null);
        }

        function createSubscription() {
            console.log('vm.plan:', vm.plan)
            console.log('vm.nonce:', vm.nonce)
            ApiService.createSubscription({'plan': vm.plan, 'nonce': vm.nonce}).then(createSubscriptionCallback, null);
        }

        function getBtTokenCallback(resp) {
            console.log("resp: ", resp);
            vm.bt_client_token = resp.data.data;
            initBraintree()
        }

        function createSubscriptionCallback(resp) {
            console.log("createSubscriptionCallback:", resp)
            window.location.reload()
        }

        function goToGigs() {
            $state.transitionTo('mygigs')
        }

        function initBraintree() {
            var paypalButton = document.querySelector('.paypal-button');
                vm.paypalButton = paypalButton;
                // Create a client.
                braintree.client.create({
                    authorization: vm.bt_client_token
                }, function (clientErr, clientInstance) {
                    console.log("clientInstance:", clientInstance)
                    // Stop if there was a problem creating the client.
                    // This could happen if there is a network error or if the authorization
                    // is invalid.
                    if (clientErr) {
                        console.error('Error creating client:', clientErr);
                        return;
                    }

                    // Create a PayPal component.
                    braintree.paypal.create({
                        client: clientInstance
                    }, function (paypalErr, paypalInstance) {
                        console.log("here!")

                        // Stop if there was a problem creating PayPal.
                        // This could happen if there was a network error or if it's incorrectly
                        // configured.
                        if (paypalErr) {
                            console.error('Error creating PayPal:', paypalErr);
                            return;
                        }

                        // Enable the button.
                        paypalButton.removeAttribute('disabled');

                        // When the button is clicked, attempt to tokenize.
                        paypalButton.addEventListener('click', function (event) {
                            console.log("paypalInstance:", paypalInstance)
                            // Because tokenization opens a popup, this has to be called as a result of
                            // customer action, like clicking a button—you cannot call this at any time.
                            paypalInstance.tokenize({
                                flow: 'vault',
                            }, function (tokenizeErr, payload) {
                                // Stop if there was an error.
                                console.log("payload:", payload)
                                console.log("tokenizeErr:", tokenizeErr)
                                if (tokenizeErr) {
                                    if (tokenizeErr.type !== 'CUSTOMER') {
                                        console.error('Error tokenizing:', tokenizeErr);
                                    }
                                    return;
                                }

                                // Tokenization succeeded!
                                paypalButton.setAttribute('disabled', true);
                                console.log('Got a nonce! You should submit this to your server.');
                                console.log(payload.nonce);
                                vm.nonce = payload.nonce;
                                createSubscription()

                            });

                        }, false);

                    });

                });
        }

    }

})(window.angular);






