(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('ResetPasswordController', ResetPasswordController);

	ResetPasswordController.$inject = ['$stateParams', '$log', '$state', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'session'];

	function ResetPasswordController ($stateParams, $log, $state, ngToast,
							 seoProvider, UserFactory, ApiService, session) {
		$log.log ('ResetPasswordController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.resetPassword = resetPassword;

		vm.userData = UserFactory.getUserData();

		vm.user = {};

		function resetPassword() {
			ApiService.resetPassword($stateParams.token, vm.userData.password).then(resetPasswordCallback, null);
		}

		function resetPasswordCallback(data){
			console.log("data:", data)
			if ((data.data.data)&&(!data.data.error)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Password changed!'
				});
				$state.go('signIn');
			}
		}

	}

})(window.angular);


