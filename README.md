# Zippy Gigs Frontend [Angular]
Zippy Gigs frontend application in **Angular.js**

Used boilerplate: **angular-seed**
## Install & Run:
Clone repository to your local machine:

`git clone https://icegeric@bitbucket.org/affinitysoft/zippy-gigs-frontend.git`

Go to cloned repository:

`cd zippy-gigs-frontend/`

Install all necessary dependencies: 

`npm install`

Start application on localhost:

`npm start`