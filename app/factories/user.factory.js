/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .factory('UserFactory', UserFactory);

    UserFactory.$inject = ['NotificationService', 'NOTIFICATION_EVENT'];

    function User(isLoggedIn) {

        this.userData = {
            id: '',
            email: '',
            emailConfirmed: false,

            login: null
        };

        this.loggined = isLoggedIn || false;

    }

    function UserFactory(NotificationService, NOTIFICATION_EVENT) {

        var user = new User();

        var service = {
            getUser: getUser,
            getUserType: getUserType,
            getUserData: getUserData,
            setUserData: setUserData,
            resetData: resetData,
            isUserLoggedIn: isUserLoggedIn,
            isFirstLogin: isFirstLogin,
            setIsLoggedIn: setIsLoggedIn,
            setUserId: setUserId,
            getUserId: getUserId
        };

        return service;

        function getUser() {
            return user;
        }

        function getUserType() {
            return user.userData.type;
        }

        function setUserData(_dataObj) {
            for (var prop in _dataObj) {
                user.userData[prop] = _dataObj[prop];
            }
            NotificationService.notify(NOTIFICATION_EVENT.USER_DATA_UPDATED);
        }

        function resetData() {
            // user = new User(!!session.get());
            user = new User();
            NotificationService.notify(NOTIFICATION_EVENT.USER_DATA_UPDATED);
        }

        function getUserData() {
            user.userData.type = String(user.userData.type);
            console.log("user.userData:", user.userData)
            return user.userData;
        }

        function setUserId(_id) {
            user.userData.id = _id;
        }

        function getUserId() {
            return user.userData.id;
        }

        function setIsLoggedIn(_loggedIn) {
            user.loggined = _loggedIn;
            NotificationService.notify(NOTIFICATION_EVENT.USER_LOGGED_IN_OUT);
        }

        function isUserLoggedIn() {
            return user.loggined;
        }

        function isFirstLogin() {
            return user.userData.first_login;
        }

    }

})(window.angular);