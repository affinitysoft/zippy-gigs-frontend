/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.service('ApiService', ApiService);

	ApiService.$inject = ['$q', 'ConfigService', '$httpParamSerializerJQLike'];

	function ApiService($q, ConfigService, $httpParamSerializerJQLike) {

		var service = {
			signin: signin,
			signup: signup,
			search: search,
			forgotPassword: forgotPassword,
			resetPassword: resetPassword,
      		searchGigs: searchGigs,
			searchHire: searchHire,
			accountGet: accountGet,
			accountSave: accountSave,
			accountAvatar: accountAvatar,
			getJobTypes: getJobTypes,
			getOrders: getOrders,
			postVendorJobs: postVendorJobs,
			deleteVendorJobs: deleteVendorJobs,
			postVendorDescription: postVendorDescription,
      		postStatus: postStatus,
			postGig: postGig,
			createOrder: createOrder,
			finishOrder: finishOrder,
			payForWorkDone: payForWorkDone,
			getProfile: getProfile,
			getBraintreeClientToken: getBraintreeClientToken,
			createSubscription: createSubscription
		};

		return service;



		function signin (login, password) {
			var deferred = $q.defer();

			var _data = {email: login, password: password};

			ConfigService.sendRequest('post', 'api/v1/auth/token/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function signup (login, password) {
			var deferred = $q.defer();

			var _data = {email: login, password: password};

			ConfigService.sendRequest('post', 'api/v1/auth/signup/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}


		function forgotPassword(login) {
			var deferred = $q.defer();
			
			var _data = {email: login};

			ConfigService.sendRequest('post', 'api/v1/auth/forgot-password/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function resetPassword(token, password) {
			var deferred = $q.defer();
			
			var _data = {password: password};

			ConfigService.sendRequest('post', 'api/v1/auth/reset/'+token, _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

			function search (_params) {
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/client/vendors/', null, _params).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function searchGigs (_params) {
			  var deferred = $q.defer();

			  ConfigService.sendRequest('get', 'api/v1/vendor/gigs/', null, _params).then(
				function (response) {
				  deferred.resolve(response);
				}
			  );

			  return deferred.promise;
			}

			function searchHire (_params) {
				var deferred = $q.defer();

				ConfigService.sendRequest('post', 'api/v1/gig/', _params).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;

			}

			function accountGet () {
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/client/profile/').then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function accountSave (account) {
				var deferred = $q.defer();

				ConfigService.sendRequest('post', 'api/v1/auth/profile/', account).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;

			}

			function accountAvatar (photo) {
				var deferred = $q.defer();

				ConfigService.sendRequest('post', 'api/v1/client/avatar/', photo, null, false, true).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function getJobTypes () {
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/client/job-types/').then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function getOrders (_data) {
				console.log("_data", _data)
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/order/get-orders/', null, _data).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

		function postVendorJobs (_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/vendor/add-job-type/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function deleteVendorJobs(_data){
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/vendor/remove-job-type/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function postVendorDescription(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/vendor/description/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function postStatus(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/vendor/status/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function postGig(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/gig/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function createOrder(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/order/create-order/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function finishOrder(_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/order/finish-order/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);
			return deferred.promise;
		}

		function payForWorkDone(_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/payment/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);
			return deferred.promise;
		}

		function getProfile(id) {
			var deferred = $q.defer();

			ConfigService.sendRequest('get', 'api/v1/client/profile/'+id+'/', null).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function getBraintreeClientToken() {
			var deferred = $q.defer();

			ConfigService.sendRequest('get', 'api/v1/payment/client_token/', null).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function createSubscription(_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/payment/create-subscription/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);
			return deferred.promise;
		}
	}

})(window.angular);
















