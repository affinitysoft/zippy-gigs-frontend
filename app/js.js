(function (angular) {
	'use strict';

	angular
		.module('zippyApp', [
			'ui.router',
			'ui-notification',
			'angularNotification',
			'seoProvider',
			'templates',
			'ngSanitize',
			'com.2fdevs.videogular',
        	'com.2fdevs.videogular.plugins.controls',
        	'com.2fdevs.videogular.plugins.overlayplay',
        	'ngToast'
		]);

})(window.angular);







(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .run(RunConfig)
        .config(MainConfig);

    RunConfig.$inject = ['$rootScope' ,'session', 'UserFactory', '$state'];

    MainConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function RunConfig($rootScope, session, UserFactory, $state){
      if(session.check()){
        session.create(localStorage.token, localStorage.login);
      }

        $rootScope.$on('$locationChangeStart', function(event, next, current) {
             console.log("location change from to:", current, next)
             console.log("$state:", $state.current)             
        });

        $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
            if (toState.authenticate && UserFactory.isUserLoggedIn() === false && !session.check()){
                $state.transitionTo("signIn");
                event.preventDefault();
            }
            if (toState.name == 'mygigs') {
                var userData = UserFactory.getUserData()
                if (!userData.vendor_membership) {
                    $state.transitionTo('membership');
                    event.preventDefault();
                }
            }
            if (UserFactory.isFirstLogin() === true) {
                $state.transitionTo('account');
                event.preventDefault();
            }

        });

    }

    function MainConfig($stateProvider, $urlRouterProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        $stateProvider

            .state('home', {
                url: '/',
                templateUrl: 'home/home.html',
                controller: 'HomeController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('contact', {
                url: '/contact',
                templateUrl: 'contact/contact.html',
                authenticate: false
            })
            .state('about', {
                url: '/about',
                templateUrl: 'about/about.html',
                authenticate: false
            })
            .state('privacy_notice', {
                url: '/privacy-notice',
                templateUrl: 'privacy_notice/privacy_notice.html',
                authenticate: false
            })
            .state('account', {
                url: '/account',
                templateUrl: 'account/account.html',
                controller : 'AccountController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('faq', {
                url: '/faq',
                templateUrl: 'faq/faq.html',
                controller: 'FaqController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('signIn', {
                url: '/sign-in',
                templateUrl: 'signin/signin.html',
                controller: 'SignInController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('terms', {
                url: '/terms',
                templateUrl: 'terms/terms.html',
                authenticate: false
            })
            .state('signUp', {
                url: '/sign-up',
                templateUrl: 'signup/signup.html',
                controller: 'SignUpController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('forgot_password', {
                url: '/forgot-password',
                templateUrl: 'forgot_password/forgot_password.html',
                controller: 'ForgotPasswordController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('reset_password', {
                url: '/reset/:token',
                templateUrl: 'reset_password/reset_password.html',
                controller: 'ResetPasswordController',
                controllerAs: 'vm',
                authenticate: false
            })
            .state('membership', {
                url: '/membership',
                templateUrl: 'membership/membership.html',
                controller: 'MembershipController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('search', {
                url: '/search',
                templateUrl: 'search/search.html',
                controller: 'SearchController',
                controllerAs: 'vm',
                authenticate: true
            })
          .state('mygigs', {
                url: '/my-gigs',
                templateUrl: 'mygigs/mygigs.html',
                controller: 'MygigsController',
                controllerAs: 'vm',
                authenticate: true
          })
            .state('vendorDash', {
                url: '/vendor',
                templateUrl: 'vendorDashboard/vendor_dashboard.html',
                controller: 'VendorDashboardController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('customDash', {
                url: '/customer',
                templateUrl: 'customerDashboard/customer_dashboard.html',
              controller: 'CustomerDashboardController',
              controllerAs: 'vm',
                authenticate: true
            })
            .state('customer_orders', {
                url: '/customer/orders',
                templateUrl: 'customer_orders/customer_orders.html',
                controller: 'OrdersController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('vendor_orders', {
                url: '/vendor/orders',
                templateUrl: 'vendor_orders/vendor_orders.html',
                controller: 'OrdersController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('vendor_profile', {
                url: '/vendor/:vendor_id',
                templateUrl: 'vendor_profile/vendor_profile.html',
                controller: 'VendorProfileController',
                controllerAs: 'vm',
                authenticate: true
            })
            .state('customer_profile', {
                url: '/customer/:customer_id',
                templateUrl: 'customer_profile/customer_profile.html',
                controller: 'CustomerProfileController',
                controllerAs: 'vm',
                authenticate: true
            });

        $urlRouterProvider.otherwise('/');

    }

})(window.angular);


(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('MainController', MainController);

	MainController.$inject = ['$log', '$scope', '$state', '$rootScope', '$window', 'ngToast',
		'seoProvider', 'UserFactory', 'NotificationService', 'ConfigService', 'NOTIFICATION_EVENT', 'session'];

	function MainController ($log, $scope, $state, $rootScope, $window,
							 ngToast, seoProvider, UserFactory, NotificationService, ConfigService, NOTIFICATION_EVENT, session) {
		$log.log ('MainController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.logOut = logOut;
		vm.onUserLoggedInOut = onUserLoggedInOut();
		vm.onUserDataUpdated = onUserDataUpdated;
		vm.userData = UserFactory.getUserData();
		vm.switchTo = switchTo;
		vm.accountType = localStorage.accountType ? localStorage.accountType : 'vendor';

		$rootScope.midLoader = false;

		NotificationService.register($scope, NOTIFICATION_EVENT.USER_LOGGED_IN_OUT, onUserLoggedInOut);

		NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);


		function onUserDataUpdated(){
		    vm.userData = UserFactory.getUserData();
		}

		function switchTo(accountType) {
			if (accountType == 'vendor') {
				vm.accountType = 'customer';
				localStorage.accountType = 'customer';
				$state.go("customDash");
			}
			else {
				vm.accountType = 'vendor';
				localStorage.accountType = 'vendor';
				$state.go("vendorDash");
			}
		}

		function onUserLoggedInOut(){
			vm.userLoggined = UserFactory.isUserLoggedIn();
		}

		function logOut() {
			delete localStorage.accountType;
			session.clear();
		}
	}

})(window.angular);
















angular.module('templates', []).run(['$templateCache', function($templateCache) {$templateCache.put('index.html','<!DOCTYPE html>\n<html lang="en" ng-app="zippyApp" ng-controller="MainController as MainController">\n<head>\n\t<title data-ng-bind="seo.title">Zippy</title>\n\n\t<base href="/">\n\n\t<meta charset="utf-8">\n\t<meta http-equiv="X-UA-Compatible" content="IE=edge">\n\t<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">\n\t<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->\n\t<meta name="title" data-ng-attr-content="{{seo.title}}" />\n\t<meta name="description" content="" data-ng-attr-content="{{seo.description}}" />\n\t<meta name="keywords" content="" data-ng-attr-content="{{seo.keywords}}" />\n\t<meta name="author" content="" data-ng-attr-content="{{seo.author}}" />\n\n\t<link rel="stylesheet" href="assets/libs/ngToast/dist/ngToast.min.css">\n\t<link href=\'assets/css/style.css\' rel=\'stylesheet\' type=\'text/css\'/>\n\n</head>\n<body ng-cloak>\n<div class="loading" ng-if="midLoader">Loading&#8230;</div>\n\t<toast></toast>\n\t<ui-view></ui-view>\n\n</body>\n<script type="text/javascript" src="assets/libs/libs.js"></script>\n<script type="text/javascript" src="js.js"></script>\n<!-- Load the client component. -->\n<script src="https://js.braintreegateway.com/web/3.11.1/js/client.min.js"></script>\n\n<!-- Load the PayPal component. -->\n<script src="https://js.braintreegateway.com/web/3.11.1/js/paypal.min.js"></script>\n</html>\n');
$templateCache.put('about/about.html','\n<div class="header header_pages">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tAbout us\n\t</div>\n</div>\n\n\n<div class="about_page wrap">\n\n\t<h2>Our Features</h2>\n\t<h4>Customers: Zippy Gigs puts you in touch with the right person or business at the right time!</h4>\n\t<div class="list">\n\t\t<div>Easy Search Features</div>\n\t\t<div>Find and hire local Freelancers, Independent Contractors and Small Business Owners</div>\n\t\t<div>Background Screened Vendors Available</div>\n\t</div>\n\n\t<h4>Vendors: Come share your talents and skills with folks in your area.</h4>\n\t<div class="list">\n\t\t<div>Freedom to work on your terms!</div>\n\t\t<div>No Commission Sharing -  only pay membership</div>\n\t\t<div>Ability to schedule appointments</div>\n\t</div>\n\n\t<h3>Our Mission</h3>\n\t<h4>\u201CGet it done NOW!\u201D</h4>\n\t<p>Zippy Gigs was founded under the belief that no one should have to wait to get help when they need it. While empowering Freelancers, Independent Contractors and Small Business Owners to work on their schedule, we help them build their business by connecting them with customers that need help in real time.</p>\n\n\t<div class="shape"></div>\n\n</div>\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>');
$templateCache.put('account/account.html','<div class="header header_pages">\n\t\n\t<div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tAccount settings\n\t</div>\n</div>\n\n\n<div class="account wrap">\n\n\t<div class="avatar">\n\t\t<div class="image"><div id="avatar" ng-style="{\'backgroundImage\': \'url(\'+vm.avatar+\')\'}"></div></div>\n\t\t<input type="file" ng-model="vm.photo" onchange="angular.element(this).scope().uploadAvatar(this)" class="hidden" id="file_avatar">\n\t\t<div class="save" ng-click="vm.avatarSave()">Change Profile Picture</div>\n\n\t\t<!--<div align="center">-->\n\t\t\t<!--<br><br><a ui-sref="orders">ORDERS</a>-->\n\t\t<!--</div>-->\n\t</div>\n\n\t<div class="info">\n\t\t<div class="item">\n\t\t\t<p ng-if="vm.userData.first_login">Choose an account type and fill out your personal information to proceed.</p>\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<span>Account Type:</span>\n\t\t\t<select class="w70 select" ng-model="vm.userData.type">\n\t\t\t\t<option value="1">Customer</option>\n\t\t\t\t<option value="2">Vendor</option>\n\t\t\t\t<option value="3">Customer & Vendor</option>\n\t\t\t</select>\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<span>First name:</span>\n\t\t\t<input type="text" class="w70 upper" ng-model="vm.userData.first_name">\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<span>Last name:</span>\n\t\t\t<input type="text" class="w70 upper" ng-model="vm.userData.last_name">\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<span>Address:</span>\n\t\t\t<input type="text" class="w70" ng-model="vm.userData.address">\n\t\t</div>\n\t\t<div class="item half main">\n\t\t\t<span>Cell phone:</span>\n\t\t\t<input type="text" class="w50" ng-model="vm.userData.phone">\n\t\t</div>\n\t\t<div class="item half alter">\n\t\t\t<span>Alternative phone:</span>\n\t\t\t<input type="text" class="w50" ng-model="vm.userData.alt_phone">\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<span>Skype:</span>\n\t\t\t<input type="text" class="w70" ng-model="vm.userData.skype">\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<span>Paypal:</span>\n\t\t\t<input type="text" class="w70" ng-model="vm.userData.pay_pal">\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<span>Zip Code:</span>\n\t\t\t<input type="text" class="w70" ng-model="vm.userData.zip_code">\n\t\t</div>\n\n\t\t<div class="save" ng-click="vm.accountSave();">Save Settings<div class="success" ng-class="{true : vm.accountSaved == true}">Saved!</div></div>\n\t</div>\n\n\t<div class="clear"></div>\n\n</div>\n\n\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('contact/contact.html','<div class="header header_pages">\n\t\n\t<div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tContact Us\n\t</div>\n</div>\n\n\n<div class="contact_page wrap">\n\n\t<div class="form">\n\t\t<input type="text" placeholder="Your Name">\n\t\t<input type="text" placeholder="Your Email">\n\t\t<input type="text" placeholder="Subject">\n\t\t<textarea placeholder="Message" rows="4"></textarea>\n\t\t<button>Submit</button>\n\t</div>\n\n\t<div class="info">\n\t\t<div class="list address">\n\t\t\t<b>Address:</b> Hampton VA 23664\n\t\t</div>\n\n<!-- \t\t<div class="list phone">\n\t\t\t<b>Telenor:</b> 541-9966-9874\n\t\t</div> -->\n\n\t\t<div class="list email">\n\t\t\t<b>Email:</b> \u0441ontactus@zippygigs.com\n\t\t</div>\n\n\t\t<h3>Follow us on:</h3>\n\n\t\t<div class="links">\n\t\t\t<a ui-sref="home"><img src="../../assets/img/contact_facebook.png"></a>\n\t\t\t<a ui-sref="home"><img src="../../assets/img/contact_twitter.png"></a>\n\t\t\t<a ui-sref="home"><img src="../../assets/img/contact_youtube.png"></a><br>\n<!-- \t\t\t<a ui-sref="#"><img src="../../assets/img/contact_pint.png"></a>\n\t\t\t<a ui-sref="#"><img src="../../assets/img/contact_linkedin.png"></a>\n\t\t\t<a ui-sref="#"><img src="../../assets/img/contact_google.png"></a> -->\n\t\t</div>\n\t</div>\n\n</div>\n\n\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('customerDashboard/customer_dashboard.html','<div ng-if="vm.userData.type == 3 || vm.userData.type == \'3\' || vm.userData.type == 1 || vm.userData.type == \'1\'">\n<div class="header header_pages search_page_header">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tCustomer Dashboard\n\t</div>\n</div>\n\n\n<div class="customdash wrap">\n\n\t<div class="info">\n\t\t<div class="avatar">\n\t\t\t<!--<div style="height: 100%; width: 100%;" ng-if="vm.userData.avatar_url" ng-style="{\'backgroundImage\': \'url(http://26756.s.t4vps.eu/api/v1/client{{vm.userData.avatar_url}})\'}"></div>-->\n\t\t\t<img style="height: 120px; width: 120px;" ng-src="{{ vm.api_img + vm.userData.avatar_url}}" ng-if="vm.userData.avatar_url != null">\n\t\t\t<img style="height: 120px; width: 120px;" src="../../assets/img/no_avatar.jpg" ng-if="vm.userData.avatar_url == null">\n\t\t</div>\n\n\t\t<div class="name">\n\t\t\tFirst name:\n\t\t\t<b>{{vm.userData.first_name}} {{vm.userData.last_name}}</b>\n\t\t</div>\n\n\t\t<div class="clear"></div>\n\n\t\t<div class="about" ng-if="vm.selectedGigItem.id">\n\t\t\t<b>{{vm.selectedGigItem.gig._type.title}} <div class="price">${{vm.selectedGigItem.gig.price}}</div></b>\n\t\t\t<div class="desc">{{vm.selectedGigItem.gig.description}}</div>\n\t\t</div>\n\n\t\t<div class="rating-block">\n\t\t\t<span>Response rating by stars</span>\n\t\t\t<fieldset class="rating-readonly dashboard" ng-show="vm.userData.response_rating">\n\t\t\t\t<span>4.0</span>\n\t\t\t\t<input type="radio" id="1star5" name="rating1" value="5" disabled/><label class = "full" for="1star5" title="Awesome - 5 stars"></label>\n\t\t\t\t<input type="radio" id="1star4half" name="rating1" value="4 and a half" disabled/><label class="half" for="1star4half" title="Pretty good - 4.5 stars"></label>\n\t\t\t\t<input type="radio" id="1star4" name="rating1" value="4" checked/><label class = "full" for="1star4" title="Pretty good - 4 stars"></label>\n\t\t\t\t<input type="radio" id="1star3half" name="rating1" value="3 and a half" /><label class="half" for="1star3half" title="Meh - 3.5 stars"></label>\n\t\t\t\t<input type="radio" id="1star3" name="rating1" value="3" disabled/><label class = "full" for="1star3" title="Meh - 3 stars"></label>\n\t\t\t\t<input type="radio" id="1star2half" name="rating1" value="2 and a half" disabled/><label class="half" for="1star2half" title="Kinda bad - 2.5 stars"></label>\n\t\t\t\t<input type="radio" id="1star2" name="rating1" value="2" disabled/><label class = "full" for="1star2" title="Kinda bad - 2 stars"></label>\n\t\t\t\t<input type="radio" id="1star1half" name="rating1" value="1 and a half" disabled/><label class="half" for="1star1half" title="Meh - 1.5 stars"></label>\n\t\t\t\t<input type="radio" id="1star1" name="rating1" value="1" disabled/><label class = "full" for="1star1" title="Sucks big time - 1 star"></label>\n\t\t\t\t<input type="radio" id="1starhalf" name="rating1" value="half" disabled/><label class="half" for="1starhalf" title="Sucks big time - 0.5 stars"></label>\n\t\t\t</fieldset>\n\t\t\t<span class="rating-empty" ng-show="!vm.userData.response_rating">- No rating yet -</span>\n\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t</div>\n\n\t\t<div class="rating-block">\n\t\t\t<span>Satisfaction rating by stars</span>\n\t\t\t<fieldset class="rating-readonly dashboard" ng-show="vm.userData.satisfaction_rating">\n\t\t\t\t<span>4.5</span>\n\t\t\t\t<input type="radio" id="2star5" name="rating2" value="5" disabled/><label class="full" for="2star5" title="Awesome - 5 stars"></label>\n\t\t\t\t<input type="radio" id="2star4half" name="rating2" value="4 and a half" checked/><label class="half" for="2star4half" title="Pretty good - 4.5 stars"></label>\n\t\t\t\t<input type="radio" id="2star4" name="rating2" value="4" disabled/><label class = "full" for="2star4" title="Pretty good - 4 stars"></label>\n\t\t\t\t<input type="radio" id="2star3half" name="rating2" value="3 and a half" disabled/><label class="half" for="2star3half" title="Meh - 3.5 stars"></label>\n\t\t\t\t<input type="radio" id="2star3" name="rating2" value="3" disabled/><label class = "full" for="2star3" title="Meh - 3 stars"></label>\n\t\t\t\t<input type="radio" id="2star2half" name="rating2" value="2 and a half" disabled/><label class="half" for="2star2half" title="Kinda bad - 2.5 stars"></label>\n\t\t\t\t<input type="radio" id="2star2" name="rating2" value="2" disabled/><label class = "full" for="2star2" title="Kinda bad - 2 stars"></label>\n\t\t\t\t<input type="radio" id="2star1half" name="rating2" value="1 and a half" disabled/><label class="half" for="2star1half" title="Meh - 1.5 stars"></label>\n\t\t\t\t<input type="radio" id="2star1" name="rating2" value="1" disabled/><label class = "full" for="2star1" title="Sucks big time - 1 star"></label>\n\t\t\t\t<input type="radio" id="2starhalf" name="rating2" value="half" disabled/><label class="half" for="2starhalf" title="Sucks big time - 0.5 stars"></label>\n\t\t\t</fieldset>\n\t\t\t<span class="rating-empty" ng-show="!vm.userData.satisfaction_rating">- No rating yet -</span>\n\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t</div>\n\n\t\t<div class="list">\n\t\t\t<p style="font-weight: bold; font-size: 18px;">Need to find help & don\u2019t feel like searching \u2013 Post a \u201CGig\u201D details below, and a message will be sent to all vendors in your area.</p>\n\t\t\t<label>Job type:</label>\n\t\t\t<select ng-options="job.title for job in vm.jobTypesData" ng-model="vm.selectedJob">\n\t\t\t</select>\n\t\t</div>\n\n\t\t<div class="other">\n\t\t\t<div class="rate">\n\t\t\t\t<label>Hourly Rate:</label>\n\t\t\t\t<div class="input">\n\t\t\t\t\t<span>$</span>\n\t\t\t\t\t<input type="text" placeholder="Add Amount" ng-model="vm.selectedHour">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="list">\n\t\t\t<label>Description:</label>\n\t\t\t<textarea  ng-model="vm.description"></textarea>\n\t\t</div>\n\n\n\t\t<div class="buttons">\n\t\t\t<button ng-click="vm.postGig($event)">POST<div class="success">Success</div></button>\n\t\t</div>\n\t\t<!--<div class="profile">Customer Profile</div>-->\n\t</div>\n\n\t<div class="items">\n\t\t<div class="head">\n\t\t\tAll Gigs paid for\n\t\t\t<b>{{vm.finishedGigs.length}}</b>\n\t\t</div>\n\n\t\t<div class="list">\n\t\t\t<div class="item" ng-class="vm.selectedGigItem.id == gig.id ? \'selected\' : \'\'" ng-repeat="gig in vm.finishedGigs">\n\t\t\t\t<div ng-click="vm.selectedGig(gig)">\n\t\t\t\t\t<b>{{gig.gig._type.title}} <div class="price">${{gig.gig.price}}</div></b>\n\t\t\t\t\t<p>{{gig.gig.description}}</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class="clear"></div>\n\n</div>\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>\n</div>');
$templateCache.put('customer_orders/customer_orders.html','<div class="header header_pages">\n\t\n\t<div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tCustomer Orders\n\t</div>\n</div>\n\n\n<div class="orders wrap">\n\n\t<div class="tabs">\n\t\t<div class="tab" ng-class="vm.status_select == 1 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(1, MainController.accountType)">Pending</div>\n\t\t<div class="tab" ng-class="vm.status_select == 2 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(2, MainController.accountType)">Accepted</div>\n\t\t<div class="tab" ng-class="vm.status_select == 3 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(3, MainController.accountType)">Rejected</div>\n\t\t<div class="tab" ng-class="vm.status_select == 4 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(4, MainController.accountType)">Finished</div>\n\t\t<div class="clear"></div>\n\t</div>\n\n\t<div class="empty" ng-show="vm.orders.length == 0">\n\t\tEmpty category.\n\t</div>\n\n\t<div class="users pending">\n\t\t<div class="user" ng-repeat="order in vm.orders">\n\t\t\t<div class="left">\n\t\t\t\t<div class="avatar">\n\t\t\t\t\t<img src="../../assets/img/no_avatar.jpg" ng-if="order.vendor.avatar_url == null">\n\t\t\t\t\t<img ng-src="{{ vm.api_img + order.vendor.avatar_url }}" ng-if="order.vendor.avatar_url != null">\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class="about">\n\t\t\t\t<div class="name">\n\t\t\t\t\t<div class="rating-block">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tResponse rating by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tSatisfaction rating by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<b>{{ order.vendor.first_name }} {{ order.vendor.last_name }}</b>\n\t\t\t\t\t<br>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="info">\n\t\t\t\t\t<b>Vendor response:</b>\n\t\t\t\t\t<p ng-if="vm.status_select != 4">{{ order.description }}</p>\n\t\t\t\t\t<p ng-if="vm.status_select == 4">{{ order.vendor_feedback }}</p>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rate">\n\t\t\t\t\tHourly Rate:\n\t\t\t\t\t<b>${{ order.gig.price }}</b>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rate" ng-if="vm.status_select == 4">\n\t\t\t\t\tHours worked:\n\t\t\t\t\t<b>{{ order.hours_worked }}</b>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rate" ng-if="vm.status_select == 4">\n\t\t\t\t\tMoney to pay\n\t\t\t\t\t<b>${{ order.money_to_pay }}</b>\n\t\t\t\t</div>\n\n\t\t\t\t<!--<div class="buttons" ng-if="order.status == 1">-->\n\t\t\t\t\t<!--<button class="accept">Accept</button>-->\n\t\t\t\t\t<!--<button>Decline</button>-->\n\t\t\t\t<!--</div>-->\n\t\t\t\t<div class="buttons" ng-if="order.status == 2">\n\t\t\t\t\t<button class="accept">Accept</button>\n\t\t\t\t\t<button>Reject</button>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="buttons" ng-if="order.status == 4">\n\t\t\t\t\t<button class="accept" ng-click="vm.payForGig(order.id)">Pay</button>\n\t\t\t\t\t<!--<script src="https://www.paypalobjects.com/api/button.js?"-->\n\t\t\t\t\t\t\t<!--data-merchant="braintree"-->\n\t\t\t\t\t\t\t<!--data-id="paypal-button"-->\n\t\t\t\t\t\t\t<!--data-button="checkout"-->\n\t\t\t\t\t\t\t<!--data-color="gold"-->\n\t\t\t\t\t\t\t<!--data-size="medium"-->\n\t\t\t\t\t\t\t<!--data-shape="pill"-->\n\t\t\t\t\t\t\t<!--data-button_type="submit"-->\n\t\t\t\t\t\t\t<!--data-button_disabled="false"-->\n\t\t\t\t\t<!--&gt;</script>-->\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class="clear"></div>\n\t\t</div>\n\t</div>\n\n\n\n</div>\n\n\n<!-- Load the client component. -->\n<script src="https://js.braintreegateway.com/web/3.11.0/js/client.min.js"></script>\n\n<!-- Load the PayPal component. -->\n<script src="https://js.braintreegateway.com/web/3.11.0/js/paypal.min.js"></script>\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('customer_profile/customer_profile.html','<div ng-if="vm.userData.type == 3 || vm.userData.type == \'3\' || vm.userData.type == 1 || vm.userData.type == \'1\'">\n    <div class="header header_pages search_page_header">\n\n        <div ng-include="\'components/partials/_menu.html\'"></div>\n\n        <div class="page_name wrap">\n            Customer Dashboard\n        </div>\n    </div>\n\n\n    <div class="customdash wrap">\n\n        <div class="info">\n            <div class="avatar">\n                <!--<div style="height: 100%; width: 100%;" ng-if="vm.userData.avatar_url" ng-style="{\'backgroundImage\': \'url(http://26756.s.t4vps.eu/api/v1/client{{vm.userData.avatar_url}})\'}"></div>-->\n                <img style="height: 120px; width: 120px;" ng-src="{{ vm.api_img + vm.profile.avatar_url}}" ng-if="vm.userData.avatar_url != null">\n                <img style="height: 120px; width: 120px;" src="../../assets/img/no_avatar.jpg" ng-if="vm.userData.avatar_url == null">\n            </div>\n\n            <div class="name">\n                Full name:\n                <b>{{vm.profile.first_name}} {{vm.profile.last_name}}</b>\n            </div>\n\n            <div class="clear"></div>\n\n            <div class="about" ng-if="vm.selectedGigItem.id">\n                <b>{{vm.selectedGigItem.gig._type.title}} <div class="price">${{vm.selectedGigItem.gig.price}}</div></b>\n                <div class="desc">{{vm.selectedGigItem.gig.description}}</div>\n            </div>\n\n            <div class="rating-block">\n                <span>Response rating by stars</span>\n                <fieldset class="rating-readonly dashboard" ng-show="vm.userData.response_rating">\n                    <span>4.0</span>\n                    <input type="radio" id="1star5" name="rating1" value="5" disabled/><label class = "full" for="1star5" title="Awesome - 5 stars"></label>\n                    <input type="radio" id="1star4half" name="rating1" value="4 and a half" disabled/><label class="half" for="1star4half" title="Pretty good - 4.5 stars"></label>\n                    <input type="radio" id="1star4" name="rating1" value="4" checked/><label class = "full" for="1star4" title="Pretty good - 4 stars"></label>\n                    <input type="radio" id="1star3half" name="rating1" value="3 and a half" /><label class="half" for="1star3half" title="Meh - 3.5 stars"></label>\n                    <input type="radio" id="1star3" name="rating1" value="3" disabled/><label class = "full" for="1star3" title="Meh - 3 stars"></label>\n                    <input type="radio" id="1star2half" name="rating1" value="2 and a half" disabled/><label class="half" for="1star2half" title="Kinda bad - 2.5 stars"></label>\n                    <input type="radio" id="1star2" name="rating1" value="2" disabled/><label class = "full" for="1star2" title="Kinda bad - 2 stars"></label>\n                    <input type="radio" id="1star1half" name="rating1" value="1 and a half" disabled/><label class="half" for="1star1half" title="Meh - 1.5 stars"></label>\n                    <input type="radio" id="1star1" name="rating1" value="1" disabled/><label class = "full" for="1star1" title="Sucks big time - 1 star"></label>\n                    <input type="radio" id="1starhalf" name="rating1" value="half" disabled/><label class="half" for="1starhalf" title="Sucks big time - 0.5 stars"></label>\n                </fieldset>\n                <span class="rating-empty" ng-show="!vm.userData.response_rating">- No rating yet -</span>\n                <!--<img src="../../assets/img/search_user_star.png">-->\n                <!--<img src="../../assets/img/search_user_star.png">-->\n                <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            </div>\n\n            <div class="rating-block">\n                <span>Satisfaction rating by stars</span>\n                <fieldset class="rating-readonly dashboard" ng-show="vm.userData.satisfaction_rating">\n                    <span>4.5</span>\n                    <input type="radio" id="2star5" name="rating2" value="5" disabled/><label class="full" for="2star5" title="Awesome - 5 stars"></label>\n                    <input type="radio" id="2star4half" name="rating2" value="4 and a half" checked/><label class="half" for="2star4half" title="Pretty good - 4.5 stars"></label>\n                    <input type="radio" id="2star4" name="rating2" value="4" disabled/><label class = "full" for="2star4" title="Pretty good - 4 stars"></label>\n                    <input type="radio" id="2star3half" name="rating2" value="3 and a half" disabled/><label class="half" for="2star3half" title="Meh - 3.5 stars"></label>\n                    <input type="radio" id="2star3" name="rating2" value="3" disabled/><label class = "full" for="2star3" title="Meh - 3 stars"></label>\n                    <input type="radio" id="2star2half" name="rating2" value="2 and a half" disabled/><label class="half" for="2star2half" title="Kinda bad - 2.5 stars"></label>\n                    <input type="radio" id="2star2" name="rating2" value="2" disabled/><label class = "full" for="2star2" title="Kinda bad - 2 stars"></label>\n                    <input type="radio" id="2star1half" name="rating2" value="1 and a half" disabled/><label class="half" for="2star1half" title="Meh - 1.5 stars"></label>\n                    <input type="radio" id="2star1" name="rating2" value="1" disabled/><label class = "full" for="2star1" title="Sucks big time - 1 star"></label>\n                    <input type="radio" id="2starhalf" name="rating2" value="half" disabled/><label class="half" for="2starhalf" title="Sucks big time - 0.5 stars"></label>\n                </fieldset>\n                <span class="rating-empty" ng-show="!vm.userData.satisfaction_rating">- No rating yet -</span>\n                <!--<img src="../../assets/img/search_user_star.png">-->\n                <!--<img src="../../assets/img/search_user_star.png">-->\n                <!--<img src="../../assets/img/search_user_star.png">-->\n                <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            </div>\n\n            <div class="list buttons" style="line-height: 1;">\n                <label>Contact info:</label>\n                <p style="font-size: 12px; line-height: 1; font-family: \'Open Sans\';"><a href=\'/membership\' style="color: #00a3af; cursor: pointer; font-size: 13px; font-weight: bold;">Buy</a> a membership to see contact information</p>\n            </div>\n            <!--<div class="profile">Customer Profile</div>-->\n        </div>\n\n        <div class="items">\n            <div class="head">\n                All Gigs paid for\n                <b>{{vm.finishedGigs.length}}</b>\n            </div>\n\n            <div class="list">\n                <div class="item" ng-class="vm.selectedGigItem.id == gig.id ? \'selected\' : \'\'" ng-repeat="gig in vm.finishedGigs">\n                    <div ng-click="vm.selectedGig(gig)">\n                        <b>{{gig.gig._type.title}} <div class="price">${{gig.gig.price}}</div></b>\n                        <p>{{gig.gig.description}}</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class="clear"></div>\n\n    </div>\n\n\n    <div ng-include="\'components/partials/_footer.html\'"></div>\n</div>\n');
$templateCache.put('faq/faq.html','<div class="header header_pages">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tFAQs\n\t</div>\n</div>\n\n\n<div class="faq_page wrap">\n\n\t<div class="title">Find answers to any question you have.</div>\n\n\t<div class="faq_list">\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>What is Zippy Gigs?</div>\n\t\t\t<div class="answer">\n\t\t\t<ul><li>Zippy Gigs is a community of vendors that are available to help you with those small and large jobs or projects. The difference is that you will find someone that is available NOW! No more waiting around for a phone call back.</li></ul></div>\n\t\t</div>\n\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>Why should I use Zippy Gigs?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li>All Vendors have passed a background screening.</li>\n\t\t\t\t\t<li>No waiting. You hire someone that is available now.</li>\n\t\t\t\t\t<li>Schedule an appointment in advance.</li>\n\t\t\t\t\t<li>Upfront pricing. You know what you are paying or can negotiate prices.</li>\n\t\t\t\t\t<li>No searching several sites for different needs. If you need help with something, Zippy Gigs most likely will have a vendor ready to help you with any project or job.</li>\n\t\t\t\t\t<li>Supporting local Freelancers, Independent Contracts and Small business owners.</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>How do I sign up to become a Vendor?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a ui-sref="signUp">Register Here</a></li>\n\t\t\t\t\t<li>Pay for membership to activate your account.</li>\n\t\t\t\t\t<li>Once you have passed the background check your account will be approved.</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>How much does it cost for a Vendor Membership?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li>First 3 months FREE after background screening approval. Giving you time to build your business and clientele.</li>\n\t\t\t\t\t<li>Never pay a percentage of what you earn. Zippy Gigs only charges you a monthly membership fee.</li>\n\t\t\t\t\t<li>Membership Plans:</li>\n\t\t\t\t\t\t<ul class="inside">\n\t\t\t\t\t\t\t<li>1-month membership: $39.99 a month</li>\n\t\t\t\t\t\t\t<li>3-month membership: $34.99 a month</li>\n\t\t\t\t\t\t\t<li>6-month membership: $29.99 a month</li>\n\t\t\t\t\t\t</ul>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>How much does it cost for a Customer Membership?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul><li>It is always FREE! No cost to sign up, search and hire someone to help you with your small or large projects. Start your search today!</li></ul>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>What is Zippy Gigs\u2019 Background screening standards?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a>Background Screening Standards</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="title">Vendor Frequently Asked Questions and Resources\nAs a small business owner, freelancer and/or Independent Contractor, you may find these tools helpful for your business. If you wish to have anything added to the list below, please send an email to: <b>Resources@zippygigs.com.</b></div>\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>How do I get my background screening?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li>First you must register as a vendor. Once you have registered you will proceed to the Background payment section.</li>\n\t\t\t\t\t<li>Background Payment: you will need to provide</li>\n\t\t\t\t\t\t\t<ul class="inside">\n\t\t\t\t\t\t\t\t<li>First Name</li>\n\t\t\t\t\t\t\t\t<li>Middle Name</li>\n\t\t\t\t\t\t\t\t<li>Last Name</li>\n\t\t\t\t\t\t\t\t<li>Phone number</li>\n\t\t\t\t\t\t\t\t<li>Email Address</li>\n\t\t\t\t\t\t\t\t<li>Background screenings cost $39.00</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t<li>You will receive an email with details on how to submit an application for the background check.</li>\n\t\t\t\t</ul>\n\t\t\t</div> \n\t\t</div>\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>What if I already have a background screening?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li>As long as your background screening is not older than 6 months, you may upload it upon registering for a vendor membership. </li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>What if I already have a background screening?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li>If concerned with having your background screening approved, please review the Background Screening Criteria.</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>Should I keep a log of my miles?</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li>For business and tax purposes you may wish to, try the link below:</li>\n\t\t\t\t\t\t\t<ul class="inside">\n\t\t\t\t\t\t\t\t<li>Mileage Logging: <a href="https://www.mileiq.com/" target="_blank">www.mileiq.com</a></li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>Small Business & Self-Employed IRS Info:</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href="https://www.irs.gov/businesses/small-businesses-self-employed" target="_blank">https://www.irs.gov/businesses/small-businesses-self-employed</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>Bookkeeping Information:</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href="https://www.waveapps.com/accounting/" target="_blank">https://www.waveapps.com/accounting/</a></li>\n\t\t\t\t\t<li><a href="https://www.manager.io/" target="_blank">https://www.manager.io/</a></li>\n\t\t\t\t\t<li><a href="https://www.gnucash.org/" target="_blank">https://www.gnucash.org/</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="item">\n\t\t\t<div class="ask" ask-faq>Getting a Business License:</div>\n\t\t\t<div class="answer">\n\t\t\t\t<ul>\n\t\t\t\t\t<li><a href="http://www.wikihow.com/Obtain-a-Business-License" target="_blank">http://www.wikihow.com/Obtain-a-Business-License</a></li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n</div>\n\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>\n');
$templateCache.put('forgot_password/forgot_password.html','<div class="header header_signup">\n\t\n\t<div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n\t<div class="form_sign login" style="bottom: 250px;">\n\t\t<div class="top">\n\t\t\t<div class="icon login"></div>\n\t\t\t<h2>FORGOT PASSWORD</h2>\n\t\t</div>\n\n\t\t<div class="label"><img src="../../assets/img/icon_sign_login.png">Email</div>\n\t\t<input type="text" ng-model="vm.userData.login">\n\n\t\t<button ng-click="vm.forgotPassword()">Send</button>\n\n\t</div>\t\n\n</div>\n\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('home/home.html','<div class="header">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="search" ng-style="{\'top\': MainController.userData.type == 3 || MainController.userData.type == \'3\' || MainController.userData.type == 1 || MainController.userData.type == \'1\' ? \'22%\' : \'30%\'}">\n\t\t<h2>Get It Done Now!</h2>\n\t\t<p>Looking to get a small project done or just need a helping hand? Hire one of Zippy Gig\u2019s Small Business vendors that are available now! No waiting around \u2013 GET IT DONE NOW!</p>\n\n\t\t<div class="form">\n\t\t\t<!--input type="text" placeholder="Search Vendor">\n\t\t\t<input type="text" placeholder="Enter Your Location"-->\n\t\t\t<button ng-if="MainController.userData.type == 3 || MainController.userData.type == \'3\' || MainController.userData.type == 1 || MainController.userData.type == \'1\'" ng-click="vm.goToSearch()">SEARCH</button>\n\t\t</div>\n\t</div>\n</div>\n\n\n<div class="about">\n\t<div class="find_red" ui-sref="about">FIND OUT ABOUT US</div>\n\n\t<p class="wrap">\n\t\tZippy Gig members are Freelancers, Self-employed individuals and Small Business owners that want to help you out NOW! They want to help you with your small projects or task, and understand that sometimes you just can\u2019t wait for a call back. Here you will find \u2013 Babysitters, Dog walkers, Computer Techs, Painters, Maids, Laborers, Cooks, Personal Shoppers, and even someone to pick you up a Fast Food meal or Take-out! You need something done, there may be a member waiting to help you out!\n\t</p>\n</div>\n\n\n<div class="vendor">\n\t<div class="wrap">\n\t\t<h2>Vendor Resources</h2>\n\t\t<h4>Business Tools and Resources</h4>\n\n\t\t<div class="items">\n\t\t\t<div class="inline">\n\t\t\t\t<b>Freelancer and/or self-employed</b>\n\t\t\t\t<p>If you are a freelance and/or self-employed, this is the place for you! Zippy Gigs gives you the freedom to provide your services when you are available. Just sign on and get connected with customers that need your services NOW! <b><a ui-sref="signUp({\'#\': \'signup_top\'})">Register Today!</a></b></p>\n\t\t\t</div>\n\n\n\t\t\t<div class="inline">\n\t\t\t\t<b>Find tools</b>\n\t\t\t\t<p>Want to provide your services and don\u2019t know where to start. Find all the tools and resources you may need to run a freelancer business, self-employed business and/or a small business owner. </p>\n\t\t\t</div>\n\n\n\t\t\t<div class="inline">\n\t\t\t\t<b>Small business owner</b>\n\t\t\t\t<p>It\u2019s a big responsibility to run a small business. You have yourself and your employees that need to be working.  Zippy Gigs is a great place during those slow and in between job times. Become a member today, and keep the work coming in!</p>\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n</div>\n\n\n<div class="choose">\n\t<div class="wrap">\n\n\t<div class="text" id="why_us">\n\t\t<h2>Why Choose Us</h2>\n\t\t<p>\n\t\t\t<span>\n\t\t\t\tCONVIENENCE\n\t\t\t</span>\n\t\t\t<br>\n\t\t\tConvenience is the #1 reason to use Zippy Gigs. When looking for help, you can search and find someone that is available now or book them in advance \u2013 at your convenience. No calling around to see if someone can help you, get help from those ready to NOW. Start your search now!\n\t\t</p>\n\t</div>\n\n\t</div>\n\n\t<div class="video">\n<!-- \t\t<div class="play" ng-click="vm.pauseOrPlay($event)">Play Video</div>\n -->\t<!-- <video preload="auto" controls>\n\t\t\t<source src="../../assets/img/ZippyGigs - Intro.mp4" type="video/mp4">\n\t\t\tYour browser does not support the video tag.\n\t\t</video> -->\n\t\t<videogular vg-theme="vm.config.theme.url">\n\t        <vg-media vg-src="vm.config.sources">\n\t        </vg-media>\n\n\t        <vg-controls>\n\t            <vg-play-pause-button></vg-play-pause-button>\n\t            <vg-time-display>{{ currentTime | date:\'mm:ss\':\'+0000\' }}</vg-time-display>\n\t            <vg-scrub-bar>\n\t                <vg-scrub-bar-current-time></vg-scrub-bar-current-time>\n\t            </vg-scrub-bar>\n\t            <vg-time-display>{{ timeLeft | date:\'mm:ss\':\'+0000\' }}</vg-time-display>\n\t            <vg-volume>\n\t                <vg-mute-button></vg-mute-button>\n\t                <vg-volume-bar></vg-volume-bar>\n\t            </vg-volume>\n\t            <vg-fullscreen-button></vg-fullscreen-button>\n\t        </vg-controls>\n\n\t        <vg-overlay-play></vg-overlay-play>\n\t    </videogular>\n\t</div>\n</div>\n\n\n<div class="contact">\n\t<div class="box">\n\t\t<div class="wrap">\n\t\t\t<h2>Vendors Join Now</h2>\n\t\t\t<h4>Work on your TERMS!</h4>\n\n\t\t\t<p>Join Zippy Gigs and work on your TERMS! You make your own hours, just sign in when you are available or use our handy scheduling tool, and allow your customers to schedule an appointment \u2013 allowing you to plan your work on your TERMS!</p>\n\n\t\t\t<div class="button_contact">Contact us</div>\n\t\t</div>\n\t</div>\n</div>\n\n\n<div class="comments">\n\t<h2>Out Testimonials</h2>\n\t<div class="slides">\n\t\t<div class="item">\n\t\t\t<div class="user">\n\t\t\t\t<div class="avatar"></div>\n\t\t\t\t<div class="name">Steven Strange<span>@DoctorS</span></div>\n\t\t\t</div>\n\t\t\t<div class="text"><span>@Pixelbuddha</span> Suspendisse sodales sem est, in scelerisque felis scelerisque in. Aenean faucibus mollis risus. Praesent sit amet erat eget eros.</div>\n\t\t\t<div class="time">2 hours ago</div>\n\t\t</div>\n\n\t\t<div class="slide_left"></div>\n\t\t<div class="slide_right"></div>\n\t</div>\n</div>\n\n\n<div class="subs">\n\t<div class="wrap">\n\t\t<h2>Subscribe To Our Newsletter</h2>\n\t\t<div class="right">\n\t\t\t<input type="email" placeholder="Email...">\n\t\t\t<button class="subscribe_button">SUBSCRIBE</button>\n\t\t</div>\n\t</div>\n</div>\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>\n');
$templateCache.put('membership/membership.html','\n<div class="header header_pages">\n\n    <div ng-include="\'components/partials/_menu.html\'"></div>\n\n    <div class="page_name wrap">\n        Choose membership\n    </div>\n</div>\n\n\n<div class="membership wrap">\n    <h4 ng-if="vm.userData.vendor_membership === false">Learn how to get - First 3 months FREE. Giving you time to build your business and clientele. Never pay a percentage of what you earn. Zippy Gigs only charges you a monthly membership fee. <a>Details</a> Submit and pass a background check screen and your business will receive 3 months free! Email background check to: <a href="mailto:backgroundchecks@zippygigs.com">backgroundchecks@zippygigs.com today.</a></h4>\n    <h2 ng-if="vm.userData.vendor_membership === false">Please choose membership plan:</h2>\n    <div class="list" ng-if="vm.userData.vendor_membership === false">\n        <label><input type="radio" name="membership" ng-model="vm.plan" value="1" />1-month membership: $39.99 a month</label>\n        <label><input type="radio" name="membership" ng-model="vm.plan" value="2" />3-month membership: $34.99 a month</label>\n        <label><input type="radio" name="membership" ng-model="vm.plan" value="3" />6-month membership: $29.99 a month</label>\n    </div>\n    <h2 ng-if="vm.userData.vendor_membership === true">Congratulations! Membership charged successfully!</h2>\n    <div ng-if="vm.userData.vendor_membership === true" class="buttons">\n        <button ng-click="vm.goToGigs()">Search gigs</button>\n    </div>\n    <script ng-if="vm.userData.vendor_membership === false" src="https://www.paypalobjects.com/api/button.js?"\n            data-merchant="braintree"\n            data-id="paypal-button"\n            data-button="checkout"\n            data-color="gold"\n            data-size="medium"\n            data-shape="pill"\n            data-button_type="submit"\n            data-button_disabled="false"\n    ></script>\n</div>\n\n<script type="application/javascript">\n    window.onload = function() {\n        var paypalButton = vm.paypalButton;\n        console.log("paypalButton:", paypalButton)\n    }\n</script>\n<div ng-include="\'components/partials/_footer.html\'"></div>');
$templateCache.put('mygigs/mygigs.html','<div ng-if="vm.userData.type == 3 || vm.userData.type == \'3\' || vm.userData.type == 2 || vm.userData.type == \'2\'">\n<div class="header header_pages search_page_header">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tSearch Gigs\n\t</div>\n</div>\n\n\n<div class="search_page wrap">\n\n\t<div class="menu">\n\t\t<div class="form">\n\t\t\t<h2><p ng-if="vm.job_type.id">{{vm.job_type.title}}</p></h2>\n\t\t\t<label>Miles from zip code:</label>\n\t\t\t<input type="number" class="half" ng-model="vm.params.radius">\n\t\t\t<input type="text" class="half" placeholder="from zip">\n\n\t\t\t<label>Gig Type:</label>\n\t\t\t<select ng-model="vm.gig_type" ng-options="type as type.title for type in vm.types">\n\t\t\t</select>\n\n\t\t\t<div ng-if="vm.gig_type.id">\n\t\t\t\t<label>Price:</label>\n\t\t\t\t<input type="number" class="half" placeholder="max" ng-model="vm.params.price_lt">\n\t\t\t\t<input type="number" class="half" placeholder="min" ng-model="vm.params.price_gt">\n\t\t\t</div>\n\n\t\t\t<button ng-click="vm.search()">Update Search</button>\n\t\t</div>\n\t</div>\n\n\t<div class="content">\n\t\t<div class="user" ng-repeat="gig in vm.gigs">\n\t\t\t<div class="left">\n\t\t\t\t<div class="avatar">\n\t\t\t\t\t<img src="../../assets/img/no_avatar.jpg" ng-if="gig.account.avatar_url == null">\n\t\t\t\t\t<img ng-src="{{ vm.api_img + gig.account.avatar_url }}" ng-if="gig.account.avatar_url != null">\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class="about">\n\t\t\t\t<div class="name">\n\t\t\t\t\t<div class="rating-block">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tResponse rating by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tSatisfaction rating by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<b>{{ gig.account.first_name }} {{ gig.account.last_name }}</b>\n\t\t\t\t\t<br>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="info">\n\t\t\t\t\t<b>Gig description:</b>\n\t\t\t\t\t<p ng-if="(gig.description == null) || (gig.description == \'\') ">- Description is not specified -</p>\n\n\t\t\t\t\t<p ng-if="gig.description != null">{{ gig.description }}</p>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="job_types">\n\t\t\t\t\t<div ng-repeat="types in gig.account.job_types" ng-class="gig.job.type == types.id ? \'selected\' : \'\' ">{{ types.title }}</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rate">\n\t\t\t\t\tHourly Rate:\n\t\t\t\t\t<b>${{ gig.price }}</b>\n\t\t\t\t</div>\n\n\n\t\t\t\t<div class="desc">\n\t\t\t\t\t<b>Hire description:</b>\n\t\t\t\t\t<textarea ng-model="vm.data.description" placeholder="Message to client"></textarea>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="buttons">\n\t\t\t\t\t<button ng-click="vm.hire(gig.id, 2)" class="success">Accept</button>\n\t\t\t\t\t<button ng-click="vm.hire(gig.id, 3)">Reject</button>\n\t\t\t\t\t<button ng-if="vm.data.description.length" ng-click="vm.createOrder($event)">Send<div class="success">Success</div></button>\n\t\t\t\t\t<button ng-click="vm.openCustomerProfile(gig.account.id)" class="success">Customer Profile</button>\n\t\t\t\t</div>\n\n\t\t\t</div>\n\n\n\n\t\t\t<div class="clear"></div>\n\t\t</div>\n\n\t\t<!--div class="user">\n\t\t\t<div class="left">\n\t\t\t\t<div class="avatar">\n\t\t\t\t\t<img src="../../assets/img/search_user.jpg">\n\t\t\t\t</div>\n\t\t\t\t<div class="av"><span></span>Available</div>\n\t\t\t\t<div class="av not_av selected"><span></span>Not Available</div>\n\t\t\t</div>\n\n\t\t\t<div class="about">\n\t\t\t\t<div class="name">\n\t\t\t\t\t<div class="rating-block">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tResponse rating-block by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tSatisfaction rating-block by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\tFirst name:\n\t\t\t\t\t<b>Jimmy Johnson</b>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="info">\n\t\t\t\t\t<b>Experience/background description:</b>\n\t\t\t\t\t<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,<br>adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,<br>adipisci velit.</p>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rate">\n\t\t\t\t\tHourly Rate:\n\t\t\t\t\t<b>$10</b>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="buttons">\n\t\t\t\t\t<button>Hire Now</button>\n\t\t\t\t\t<button>View Available Times</button>\n\t\t\t\t\t<button class="vendor">Vendor Profile</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\n\n\t\t\t<div class="clear"></div>\n\t\t</div-->\n\n<!-- \t\t<div class="pagination">\n\t\t\t<div>\xAB Previous</div>\n\t\t\t<div class="selected">1</div>\n\t\t\t<div>2</div>\n\t\t\t<div>3</div>\n\t\t\t<div>4</div>\n\t\t\t<div>...</div>\n\t\t\t<div>6</div>\n\t\t\t<div>Next \xBB</div>\n\t\t</div> -->\n\n\t</div>\n\n\n\t<div class="clear"></div>\n\n</div>\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>\n</div>');
$templateCache.put('partials/_footer.html','<div class="footer wrap footer_pages">\n\t<div class="company col">\n\t\t<img src="../../assets/img/logo_footer.png">\n\t\t<p>&copy;2016, Zippy Gigs, All Rights Reserved</p>\n\t</div>\n\t<div class="navigation col">\n\t\t<b>Navigation</b>\n\t\t<a ui-sref="home({\'#\': \'why_us\'})">Why Us?</a>\n\t\t<a ui-sref="about">About Us</a>\n\t\t<a ui-sref="faq">FAQs</a>\n\t\t<a ui-sref="terms({\'#\': \'terms\'})">Terms & Conditions</a>\n\t\t<a href="privacy-notice" target="_self">Privacy Notice</a>\n\t</div>\n\t<div class="contacts col">\n\t\t<div class="phone">\n\t\t\t<a href="mailto:contactUs@zippygigs.com">ContactUs@zippygigs.com</a>\n\t\t\t<span>Available 10 am EST \u2013 5 pm EST</span>\n\t\t</div>\n\t</div>\n\t<div class="info col">\n\t\t<b>Information</b>\n\t\t<p><b>Have a question?</b> Contact Questions@zippygigs.com<br>\n\t\t\t<b>Need to report issues with website?</b> Contact webmaster@zippygigs.com<br><b>Checking on Background approval?</b>Contact Backgroundchecks@zippygigs.com and please include your Name and email address</p>\n\t</div>\n\n\t<div class="clear"></div>\n\n\t<!--div class="social">\n\t\t<div class="twitter">\n\t\t\t<button>Twitter</button>\n\t\t\t<span>136 312 Followers</span>\n\t\t</div>\n\n\t\t<div class="facebook">\n\t\t\t<button>Facebook</button>\n\t\t\t<span>218 092 Subscribers</span>\n\t\t</div>\n\t</div-->\n</div>\n');
$templateCache.put('partials/_menu.html','<div class="menu wrap">\n\t<a ui-sref="home" class="logo"><img src="../../assets/img/logo.png"></a>\n\n\t<div class="dropdown" ng-show="MainController.userLoggined == true">\n\t\t<button class="dropbtn">Home <span>&#x25BE;</span></button>\n\t\t<div class="dropdown-content">\n\t\t\t<a ui-sref="home">Home</a>\n\t\t\t<a ui-sref="contact">Contact</a>\n\t\t\t<a ui-sref="about">About Us</a>\n\t\t\t<a ui-sref="faq">FAQs</a>\n\t\t\t<!--<a ui-sref="vendor">Vendor Resources</a>-->\n\t\t</div>\n\t</div>\n\t<a ui-sref="home" ng-show="MainController.userLoggined == false">Home</a>\n\t<a ui-sref="contact" ng-show="MainController.userLoggined == false">Contact</a>\n\t<a ui-sref="about" ng-show="MainController.userLoggined == false">About Us</a>\n\t<a ui-sref="faq" ng-show="MainController.userLoggined == false">FAQs</a>\n\t<!--<a ui-sref="vendor">Vendor Resources</a>-->\n\n\t<a ui-sref="customDash" ng-show="MainController.userLoggined == true"\n\t\t ng-if="(MainController.userData.type == 1 || MainController.userData.type == \'1\' || ((MainController.userData.type == 3 || MainController.userData.type == \'3\') && (MainController.accountType == \'customer\'))) && (MainController.userData.first_login == false)">Dashboard</a>\n\t<a ui-sref="customer_orders" ng-show="MainController.userLoggined == true"\n\t   ng-if="(MainController.userData.type == 1 || MainController.userData.type == \'1\' || ((MainController.userData.type == 3 || MainController.userData.type == \'3\') && (MainController.accountType == \'customer\'))) && (MainController.userData.first_login == false)">Orders</a>\n\t<a ui-sref="vendorDash" ng-show="MainController.userLoggined == true"\n\t\t ng-if="(MainController.userData.type == 2 || MainController.userData.type == \'2\' || ((MainController.userData.type == 3 || MainController.userData.type == \'3\') && (MainController.accountType == \'vendor\'))) && (MainController.userData.first_login == false)">Dashboard</a>\n\t<a ui-sref="vendor_orders" ng-show="MainController.userLoggined == true"\n\t   ng-if="(MainController.userData.type == 2 || MainController.userData.type == \'2\' || ((MainController.userData.type == 3 || MainController.userData.type == \'3\') && (MainController.accountType == \'vendor\')) && (MainController.userData.first_login == false)) && (MainController.userData.first_login == false)">Orders</a>\n\t<a ui-sref="account" ng-show="MainController.userLoggined == true">Account</a>\n\t<a ui-sref="search" ng-show="MainController.userLoggined == true"\n\t   ng-if="(MainController.userData.type == 1 || MainController.userData.type == \'1\' || ((MainController.userData.type == 3 || MainController.userData.type == \'3\') && (MainController.accountType == \'customer\'))) && (MainController.userData.first_login == false)">Search</a>\n\t<a ui-sref="mygigs" ng-show="MainController.userLoggined == true"\n\t   ng-if="(MainController.userData.type == 2 || MainController.userData.type == \'2\' || ((MainController.userData.type == 3 || MainController.userData.type == \'3\') && (MainController.accountType == \'vendor\'))) && (MainController.userData.first_login == false)">Gigs</a>\n\t<div class="login" ng-show="MainController.userLoggined == true">\n\t\t<a href="" ng-if="(MainController.userData.type == 3 || MainController.userData.type == \'3\') && (MainController.userData.first_login == false)"\n\t\t   ng-click="MainController.switchTo(MainController.accountType)" style="text-align: left;">User: <span style="color: #59c3cb;">{{ MainController.accountType }}</span></a>\n\t\t<a href="" ng-click="MainController.logOut()">Exit</a>\n\t</div>\n\n\t<div class="login" ng-show="MainController.userLoggined == false">\n\t\t<a ui-sref="signUp" class="reg">Register</a>\n\t\t<a ui-sref="signIn" class="log">Login</a>\n\t</div>\n</div>');
$templateCache.put('privacy_notice/privacy_notice.html','\n<div class="header header_pages">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap terms">\n\t\tWeb Site Privacy Policy\n\t</div>\n</div>\n\n<div class="wrap terms">\n\t<br>\n\t<p>\n\t\tZippyGigs.com is committed to protecting your privacy and developing technology that gives you the most powerful and safe online experience. This Statement of Privacy applies to the ZippyGigs.com Web site and governs data collection and usage. By using the ZippyGigs.com website, you consent to the data practices described in this statement.\n\t</p>\n\n\t<h3>\n\t\tCollection of your Personal Information\n\t</h3>\n\n\t<p>\n\t\tZippyGigs.com collects personally identifiable information, such as your e-mail address, name, home or work address or telephone number. ZippyGigs.com also collects anonymous demographic information, which is not unique to you, such as your ZIP code, age, gender, preferences, interests and favorites.\n\t</p>\n\n\t<p>\n\t\tThere is also information about your computer hardware and software that is automatically collected by ZippyGigs.com. This information can include: your IP address, browser type, domain names, access times and referring Web site addresses. This information is used by ZippyGigs.com for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the ZippyGigs.com Web site.\n\t</p>\n\n\t<p>\n\t\tPlease keep in mind that if you directly disclose personally identifiable information or personally sensitive data through ZippyGigs.com public message boards, this information may be collected and used by others. Note: ZippyGigs.com does not read any of your private online communications.\n\t</p>\n\n\t<p>\n\t\tZippyGigs.com encourages you to review the privacy statements of Web sites you choose to link to from ZippyGigs.com so that you can understand how those Web sites collect, use and share your information. ZippyGigs.com is not responsible for the privacy statements or other content on Web sites outside of the ZippyGigs.com and ZippyGigs.com family of Web sites.\n\t</p>\n\n\t<h3>\n\t\tUse of your Personal Information\n\t</h3>\n\n\t<p>\n\t\tZippyGigs.com collects and uses your personal information to operate the ZippyGigs.com Web site and deliver the services you have requested. ZippyGigs.com also uses your personally identifiable information to inform you of other products or services available from ZippyGigs.com and its affiliates. ZippyGigs.com may also contact you via surveys to conduct research about your opinion of current services or of potential new services that may be offered.\n\t</p>\n\n\t<p>\n\t\tZippyGigs.com does not sell, rent or lease its customer lists to third parties. ZippyGigs.com may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you. In those cases, your unique personally identifiable information (e-mail, name, address, telephone number) is not transferred to the third party. In addition, ZippyGigs.com may share data with trusted partners to help us perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to ZippyGigs.com, and they are required to maintain the confidentiality of your information.\n\t</p>\n\n\t<p>\n\t\tZippyGigs.com does not use or disclose sensitive personal information, such as race, religion, or political affiliations, without your explicit consent.\n\t\tZippyGigs.com keeps track of the Web sites and pages our customers visit within ZippyGigs.com, in order to determine what ZippyGigs.com services are the most popular. This data is used to deliver customized content and advertising within ZippyGigs.com to customers whose behavior indicates that they are interested in a particular subject area.\n\t</p>\n\n\t<p>\n\t\tZippyGigs.com Web sites will disclose your personal information, without notice, only if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on ZippyGigs.com or the site; (b) protect and defend the rights or property of ZippyGigs.com; and, (c) act under exigent circumstances to protect the personal safety of users of ZippyGigs.com, or the public.\n\t</p>\n\n\t<h3>Use of Cookies</h3>\n\n\t<p>\n\t\tThe ZippyGigs.com Web site use "cookies" to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a Web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.\n\t</p>\n\n\t<p>\n\t\tOne of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the Web server that you have returned to a specific page. For example, if you personalize ZippyGigs.com pages, or register with ZippyGigs.com site or services, a cookie helps ZippyGigs.com to recall your specific information on subsequent visits. This simplifies the process of recording your personal information, such as billing addresses, shipping addresses, and so on. When you return to the same ZippyGigs.com Web site, the information you previously provided can be retrieved, so you can easily use the ZippyGigs.com features that you customized.\n\t</p>\n\n\t<p>\n\t\tYou have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the ZippyGigs.com services or Web sites you visit.\n\t</p>\n\n\t<h3>Security of your Personal Information</h3>\n\n\t<p>\n\t\tZippyGigs.com secures your personal information from unauthorized access, use or disclosure. ZippyGigs.com secures the personally identifiable information you provide on computer servers in a controlled, secure environment, protected from unauthorized access, use or disclosure. When personal information (such as a credit card number) is transmitted to other Web sites, it is protected through the use of encryption, such as the Secure Socket Layer (SSL) protocol.\n\t</p>\n\n\t<h3>Changes to this Statement</h3>\n\n\t<p>\n\t\tZippyGigs.com will occasionally update this Statement of Privacy to reflect company and customer feedback. ZippyGigs.com encourages you to periodically review this Statement to be informed of how ZippyGigs.com is protecting your information.\n\t</p>\n\n\t<h3>Contact Information</h3>\n\n\t<p>\n\t\tZippyGigs.com welcomes your comments regarding this Statement of Privacy. If you believe that ZippyGigs.com has not adhered to this Statement, please contact ZippyGigs.com at . We will use commercially reasonable efforts to promptly determine and remedy the problem.\n\t</p>\n</div>\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>');
$templateCache.put('reset_password/reset_password.html','<div class="header header_signup">\n\t\n\t<div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n\t<div class="form_sign login" style="bottom: 250px;">\n\t\t<div class="top">\n\t\t\t<div class="icon login"></div>\n\t\t\t<h2>RESET PASSWORD</h2>\n\t\t</div>\n\n\t\t<div class="label"><img src="../../assets/img/icon_sign_login.png">New Password</div>\n\t\t<input type="password" ng-model="vm.userData.password">\n\n\t\t<button ng-click="vm.resetPassword()">Reset Password</button>\n\n\t</div>\t\n\n</div>\n\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('search/search.html','<div ng-if="vm.userData.type == 3 || vm.userData.type == \'3\' || vm.userData.type == 1 || vm.userData.type == \'1\'">\n<div class="header header_pages search_page_header">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tSearch Result\n\t</div>\n</div>\n\n\n<div class="search_page wrap">\n\n\t<div class="menu">\n\t\t<div class="form">\n\t\t\t<h2><p ng-if="vm.job_type.id">{{vm.job_type.title}}</p></h2>\n\t\t\t<label>Miles from zip code:</label>\n\t\t\t<input type="number" class="half" ng-model="vm.params.radius">\n\t\t\t<input type="text" class="half" placeholder="from zip">\n\n\t\t\t<label>Status Option:</label>\n\t\t\t<select ng-model="vm.params.status">\n\t\t\t\t<option value="">- Not Selected -</option>\n\t\t\t\t<option value="1">Available</option>\n\t\t\t\t<option value="2">Not Available</option>\n\t\t\t</select>\n\n\t\t\t<label>Job Type:</label>\n\t\t\t<select ng-model="vm.job_type" ng-options="type as type.title for type in vm.types">\n\t\t\t</select>\n\n\t\t\t<div ng-if="vm.job_type.id">\n\t\t\t\t<label>Price:</label>\n\t\t\t\t<input type="number" class="half" placeholder="max" ng-model="vm.params.hourly_rate_lt">\n\t\t\t\t<input type="number" class="half" placeholder="min" ng-model="vm.params.hourly_rate_gt">\n\t\t\t</div>\n\n\t\t\t<button ng-click="vm.search()">Update Search</button>\n\t\t</div>\n\t</div>\n\n\t<div class="content">\n\t\t<div class="user" ng-repeat="user in vm.users">\n\t\t\t<div class="left">\n\t\t\t\t<div class="avatar">\n\t\t\t\t\t<img src="../../assets/img/no_avatar.jpg" ng-if="user.avatar_url == null">\n\t\t\t\t\t<img ng-src="{{ vm.api_img + user.avatar_url }}" ng-if="user.avatar_url != null">\n\t\t\t\t</div>\n\t\t\t\t<div class="av"  ng-class="user.vendor_status == 1 ? \'selected\' : \'\'"><span></span>Available</div>\n\t\t\t\t<div class="av not_av" ng-class="user.vendor_status == 2 ? \'selected\' : \'\'"><span></span>Not Available</div>\n\t\t\t</div>\n\n\t\t\t<div class="about">\n\t\t\t\t<div class="name">\n\t\t\t\t\t<div class="rating-block">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tResponse rating by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tSatisfaction rating by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<b>{{ user.first_name }} {{ user.last_name }}</b>\n\t\t\t\t\t<br>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="info">\n\t\t\t\t\t<b>Experience/background description:</b>\n\t\t\t\t\t<p ng-if="user.vendor_description == null">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,<br>adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,<br>adipisci velit.</p>\n\n\t\t\t\t\t<p ng-if="user.vendor_description != null">{{ user.vendor_description }}</p>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="job_types">\n\t\t\t\t\t<div ng-repeat="types in user.job_types" ng-click="vm.selectJobType($parent.$index, $index, types.id, types.hourly_rate, $event)" ng-class="user.job.type == types.id ? \'selected\' : \'\' ">{{ types.title }}</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rate">\n\t\t\t\t\tHourly Rate:\n\t\t\t\t\t<b>${{ user.job.price }}</b>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="desc">\n\t\t\t\t\t<b>Hire description:</b>\n\t\t\t\t\t<textarea ng-model="user.job.description"></textarea>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="buttons">\n\t\t\t\t\t<button ng-disabled="!user.job.description" ng-click="vm.hire(user, $event)">Hire Now<div class="success">Success</div></button>\n\t\t\t\t\t<!-- <button>View Available Times</button> -->\n\t\t\t\t\t<button class="vendor" ng-click="vm.openVendorProfile(user.id)">Vendor Profile</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\n\n\t\t\t<div class="clear"></div>\n\t\t</div>\n\n\t\t<!--div class="user">\n\t\t\t<div class="left">\n\t\t\t\t<div class="avatar">\n\t\t\t\t\t<img src="../../assets/img/search_user.jpg">\n\t\t\t\t</div>\n\t\t\t\t<div class="av"><span></span>Available</div>\n\t\t\t\t<div class="av not_av selected"><span></span>Not Available</div>\n\t\t\t</div>\n\n\t\t\t<div class="about">\n\t\t\t\t<div class="name">\n\t\t\t\t\t<div class="rating-block">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tResponse rating-block by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\tSatisfaction rating-block by stars\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t\t<img src="../../assets/img/search_user_star_empty.png">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\tFirst name:\n\t\t\t\t\t<b>Jimmy Johnson</b>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="info">\n\t\t\t\t\t<b>Experience/background description:</b>\n\t\t\t\t\t<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,<br>adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur,<br>adipisci velit.</p>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rate">\n\t\t\t\t\tHourly Rate:\n\t\t\t\t\t<b>$10</b>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="buttons">\n\t\t\t\t\t<button>Hire Now</button>\n\t\t\t\t\t<button>View Available Times</button>\n\t\t\t\t\t<button class="vendor">Vendor Profile</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\n\n\t\t\t<div class="clear"></div>\n\t\t</div-->\n\n<!-- \t\t<div class="pagination">\n\t\t\t<div>\xAB Previous</div>\n\t\t\t<div class="selected">1</div>\n\t\t\t<div>2</div>\n\t\t\t<div>3</div>\n\t\t\t<div>4</div>\n\t\t\t<div>...</div>\n\t\t\t<div>6</div>\n\t\t\t<div>Next \xBB</div>\n\t\t</div> -->\n\n\t</div>\n\n\n\t<div class="clear"></div>\n\n</div>\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>\n</div>');
$templateCache.put('signin/signin.html','<div class="header header_signup">\n\t\n\t<div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n\t<div class="form_sign login">\n\t\t<div class="top">\n\t\t\t<div class="icon login"></div>\n\t\t\t<h2>LOGIN</h2>\n\t\t</div>\n\n\t\t<div class="label"><img src="../../assets/img/icon_sign_login.png">Email</div>\n\t\t<input type="text" ng-model="vm.userData.login">\n\n\t\t<div class="label"><img src="../../assets/img/icon_sign_pass.png">Password</div>\n\t\t<input type="password" ng-model="vm.user.password">\n\t\t<div class="go_reg" style="float: right; margin: 0; margin-bottom: 10px;"><a ui-sref="forgot_password">Forgot Password?</a></div>\n\n\t\t<button ng-click="vm.signIn()">Login</button>\n\n\t\t<div class="go_reg">\n\t\t\tDon\'t have an account? <a ui-sref="signUp">Register Here</a>\n\t\t</div>\n\n\t</div>\t\n\n</div>\n\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('signup/signup.html','<div class="header header_signup" id="signup_top">\n\t\n\t<div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n\t<div class="form_sign reg">\n\t\t<form name="vm.signUpForm" novalidate>\n\t\t<div class="top">\n\t\t\t<div class="icon reg"></div>\n\t\t\t<h2>REGISTER</h2>\n\t\t</div>\n\n\t\t<div class="label"><img src="../../assets/img/icon_sign_login.png">Email</div>\n\t\t<input type="text" ng-model="vm.userData.login" required>\n\n\t\t<div class="label"><img src="../../assets/img/icon_sign_pass.png">Password</div>\n\t\t<input type="password" ng-model="vm.user.password" required>\n\n\t\t<div class="label"><img src="../../assets/img/icon_sign_pass.png">Confirm Password</div>\n\t\t<input type="password" ng-model="vm.user.passwordConfirm" required>\n\n\t\t<label for="tos"><img src="../../assets/img/header_log.png"/> Accept <a ui-sref="terms">Terms and Conditions</a></label>\n\t\t<input type="checkbox" ng-model="vm.user.tos" required>\n\n\t\t<button ng-click="vm.signUp()">Register Now</button>\n\n\t\t<div class="go_reg">\n\t\t\tHave an account? <a ui-sref="signIn">Login Here</a>\n\t\t</div>\n\n\t\t</form>\n\n\t</div>\t\n\n</div>\n\n\n<div class="wrap terms">\n\t<h2>Terms & Conditions</h2>\n\n<h2>\n\tWeb Site Terms and Conditions of Use\n</h2>\n\n<h3>\n\t1. Terms\n</h3>\n\n<p>\n\tBy accessing this web site, you are agreeing to be bound by these \n\tweb site Terms and Conditions of Use, all applicable laws and regulations, \n\tand agree that you are responsible for compliance with any applicable local \n\tlaws. If you do not agree with any of these terms, you are prohibited from \n\tusing or accessing this site. The materials contained in this web site are \n\tprotected by applicable copyright and trade mark law.\n</p>\n\n<h3>\n\t2. Use License\n</h3>\n\n<ol type="a">\n\t<li>\n\t\tPermission is granted to temporarily download one copy of the materials \n\t\t(information or software) on Zippy Gigs LLC\'s web site for personal, \n\t\tnon-commercial transitory viewing only. This is the grant of a license, \n\t\tnot a transfer of title, and under this license you may not:\n\t\t\n\t\t<ol type="i">\n\t\t\t<li>modify or copy the materials;</li>\n\t\t\t<li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>\n\t\t\t<li>attempt to decompile or reverse engineer any software contained on Zippy Gigs LLC\'s web site;</li>\n\t\t\t<li>remove any copyright or other proprietary notations from the materials; or</li>\n\t\t\t<li>transfer the materials to another person or "mirror" the materials on any other server.</li>\n\t\t</ol>\n\t</li>\n\t<li>\n\t\tThis license shall automatically terminate if you violate any of these restrictions and may be terminated by Zippy Gigs LLC at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.\n\t</li>\n</ol>\n\n<h3>\n\t3. Disclaimer\n</h3>\n\n<ol type="a">\n\t<li>\n\t\tThe materials on Zippy Gigs LLC\'s web site are provided "as is". Zippy Gigs LLC makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Zippy Gigs LLC does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.\n\t</li>\n</ol>\n\n<h3>\n\t4. Limitations\n</h3>\n\n<p>\n\tIn no event shall Zippy Gigs LLC or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Zippy Gigs LLC\'s Internet site, even if Zippy Gigs LLC or a Zippy Gigs LLC authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.\n</p>\n\t\t\t\n<h3>\n\t5. Revisions and Errata\n</h3>\n\n<p>\n\tThe materials appearing on Zippy Gigs LLC\'s web site could include technical, typographical, or photographic errors. Zippy Gigs LLC does not warrant that any of the materials on its web site are accurate, complete, or current. Zippy Gigs LLC may make changes to the materials contained on its web site at any time without notice. Zippy Gigs LLC does not, however, make any commitment to update the materials.\n</p>\n\n<h3>\n\t6. Links\n</h3>\n\n<p>\n\tZippy Gigs LLC has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Zippy Gigs LLC of the site. Use of any such linked web site is at the user\'s own risk.\n</p>\n\n<h3>\n\t7. Site Terms of Use Modifications\n</h3>\n\n<p>\n\tZippy Gigs LLC may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.\n</p>\n\n<h3>\n\t8. Governing Law\n</h3>\n\n<p>\n\tAny claim relating to Zippy Gigs LLC\'s web site shall be governed by the laws of the State of Virginia without regard to its conflict of law provisions.\n</p>\n\n<p>\n\tGeneral Terms and Conditions applicable to Use of a Web Site.\n</p>\n\n\n\n<h2>\n\tPrivacy Policy\n</h2>\n\n<p>\n\tYour privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.\n</p>\n\n<ul>\n\t<li>\n\t\tBefore or at the time of collecting personal information, we will identify the purposes for which information is being collected.\n\t</li>\n\t<li>\n\t\tWe will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.\t\t\n\t</li>\n\t<li>\n\t\tWe will only retain personal information as long as necessary for the fulfillment of those purposes. \n\t</li>\n\t<li>\n\t\tWe will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned. \n\t</li>\n\t<li>\n\t\tPersonal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date. \n\t</li>\n\t<li>\n\t\tWe will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.\n\t</li>\n\t<li>\n\t\tWe will make readily available to customers information about our policies and practices relating to the management of personal information. \n\t</li>\n</ul>\n\n<p>\n\tWe are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. \n</p>\t\t\n\n</div>\n\n\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('terms/terms.html','\n<div class="header header_pages" id="terms">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap terms">\n\t\tWeb Site Terms and Conditions of Use\n\t</div>\n</div>\n\n\n<div class="wrap terms">\n\t<h2>Terms & Conditions</h2>\n\n<h2>\n\tWeb Site Terms and Conditions of Use\n</h2>\n\n<h3>\n\t1. Terms\n</h3>\n\n<p>\n\tBy accessing this web site, you are agreeing to be bound by these \n\tweb site Terms and Conditions of Use, all applicable laws and regulations, \n\tand agree that you are responsible for compliance with any applicable local \n\tlaws. If you do not agree with any of these terms, you are prohibited from \n\tusing or accessing this site. The materials contained in this web site are \n\tprotected by applicable copyright and trade mark law.\n</p>\n\n<h3>\n\t2. Use License\n</h3>\n\n<ol type="a">\n\t<li>\n\t\tPermission is granted to temporarily download one copy of the materials \n\t\t(information or software) on Zippy Gigs LLC\'s web site for personal, \n\t\tnon-commercial transitory viewing only. This is the grant of a license, \n\t\tnot a transfer of title, and under this license you may not:\n\t\t\n\t\t<ol type="i">\n\t\t\t<li>modify or copy the materials;</li>\n\t\t\t<li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>\n\t\t\t<li>attempt to decompile or reverse engineer any software contained on Zippy Gigs LLC\'s web site;</li>\n\t\t\t<li>remove any copyright or other proprietary notations from the materials; or</li>\n\t\t\t<li>transfer the materials to another person or "mirror" the materials on any other server.</li>\n\t\t</ol>\n\t</li>\n\t<li>\n\t\tThis license shall automatically terminate if you violate any of these restrictions and may be terminated by Zippy Gigs LLC at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.\n\t</li>\n</ol>\n\n<h3>\n\t3. Disclaimer\n</h3>\n\n<ol type="a">\n\t<li>\n\t\tThe materials on Zippy Gigs LLC\'s web site are provided "as is". Zippy Gigs LLC makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Zippy Gigs LLC does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.\n\t</li>\n</ol>\n\n<h3>\n\t4. Limitations\n</h3>\n\n<p>\n\tIn no event shall Zippy Gigs LLC or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Zippy Gigs LLC\'s Internet site, even if Zippy Gigs LLC or a Zippy Gigs LLC authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.\n</p>\n\t\t\t\n<h3>\n\t5. Revisions and Errata\n</h3>\n\n<p>\n\tThe materials appearing on Zippy Gigs LLC\'s web site could include technical, typographical, or photographic errors. Zippy Gigs LLC does not warrant that any of the materials on its web site are accurate, complete, or current. Zippy Gigs LLC may make changes to the materials contained on its web site at any time without notice. Zippy Gigs LLC does not, however, make any commitment to update the materials.\n</p>\n\n<h3>\n\t6. Links\n</h3>\n\n<p>\n\tZippy Gigs LLC has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Zippy Gigs LLC of the site. Use of any such linked web site is at the user\'s own risk.\n</p>\n\n<h3>\n\t7. Site Terms of Use Modifications\n</h3>\n\n<p>\n\tZippy Gigs LLC may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.\n</p>\n\n<h3>\n\t8. Governing Law\n</h3>\n\n<p>\n\tAny claim relating to Zippy Gigs LLC\'s web site shall be governed by the laws of the State of Virginia without regard to its conflict of law provisions.\n</p>\n\n<p>\n\tGeneral Terms and Conditions applicable to Use of a Web Site.\n</p>\n\n\n\n<h2>\n\tPrivacy Policy\n</h2>\n\n<p>\n\tYour privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.\n</p>\n\n<ul>\n\t<li>\n\t\tBefore or at the time of collecting personal information, we will identify the purposes for which information is being collected.\n\t</li>\n\t<li>\n\t\tWe will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.\t\t\n\t</li>\n\t<li>\n\t\tWe will only retain personal information as long as necessary for the fulfillment of those purposes. \n\t</li>\n\t<li>\n\t\tWe will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned. \n\t</li>\n\t<li>\n\t\tPersonal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date. \n\t</li>\n\t<li>\n\t\tWe will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.\n\t</li>\n\t<li>\n\t\tWe will make readily available to customers information about our policies and practices relating to the management of personal information. \n\t</li>\n</ul>\n\n<p>\n\tWe are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. \n</p>\t\t\n\n</div>\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>');
$templateCache.put('vendorDashboard/vendor_dashboard.html','<div ng-if="vm.userData.type == 2 || vm.userData.type !== \'2\' || vm.userData.type == 3 || vm.userData.type !== \'3\'">\n<div class="header header_pages search_page_header">\n\t\n\t<div ng-include="\'components/partials/_menu.html\'"></div>\n\n\t<div class="page_name wrap">\n\t\tVendor Dashboard\n\t</div>\n</div>\n\n\n<div class="vendordash wrap">\n\n\t<div class="left">\n\t\t<div class="user">\n\t\t\t<div class="avatar">\n\t\t\t\t<!--<div style="height: 100%; width: 100%;" ng-if="vm.userData.avatar_url" ng-style="{\'backgroundImage\': \'url(http://host.zippygigs.com/api/v1/client{{vm.userData.avatar_url}})\'}"></div>-->\n\t\t\t\t<img style="height: 120px; width: 120px;" ng-src="{{ vm.api_img + vm.userData.avatar_url}}" ng-if="vm.userData.avatar_url != null">\n\t\t\t\t<img style="height: 120px; width: 120px;" src="../../assets/img/no_avatar.jpg" ng-if="vm.userData.avatar_url == null">\n\t\t\t</div>\n\t\t\t<div class="info">\n\t\t\t\t<div class="status">\n\t\t\t\t\t<div ng-class="vm.status == 1 ? \'selected\' : \'\'" class="av" ng-click="vm.changeStatus(1)"><span></span>Available</div>\n\t\t\t\t\t<div ng-class="vm.status == 2 ? \'selected not_av\' : \'\'" class="av" ng-click="vm.changeStatus(2)"><span></span>Not Available</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="clear"></div>\n\n\t\t\t\t<div class="rating-block">\n\t\t\t\t\t<span>Response rating by stars</span>\n\t\t\t\t\t<fieldset class="rating-readonly dashboard" ng-show="vm.userData.response_rating">\n\t\t\t\t\t\t<span>4.0</span>\n\t\t\t\t\t\t<input type="radio" id="1star5" name="rating1" value="5" disabled/><label class = "full" for="1star5" title="Awesome - 5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="1star4half" name="rating1" value="4 and a half" disabled/><label class="half" for="1star4half" title="Pretty good - 4.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="1star3half" name="rating1" value="3 and a half" /><label class="half" for="1star3half" title="Meh - 3.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="1star3" name="rating1" value="3" disabled/><label class = "full" for="1star3" title="Meh - 3 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="1star2half" name="rating1" value="2 and a half" disabled/><label class="half" for="1star2half" title="Kinda bad - 2.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="1star2" name="rating1" value="2" disabled/><label class = "full" for="1star2" title="Kinda bad - 2 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="1star1half" name="rating1" value="1 and a half" disabled/><label class="half" for="1star1half" title="Meh - 1.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="1star1" name="rating1" value="1" disabled/><label class = "full" for="1star1" title="Sucks big time - 1 star"></label>\n\t\t\t\t\t\t<input type="radio" id="1starhalf" name="rating1" value="half" disabled/><label class="half" for="1starhalf" title="Sucks big time - 0.5 stars"></label>\n\t\t\t\t\t</fieldset>\n\t\t\t\t\t<span class="rating-empty" ng-show="!vm.userData.response_rating">- No rating yet -</span>\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t</div>\n\n\t\t\t\t<div class="rating-block">\n\t\t\t\t\t<span>Satisfaction rating by stars</span>\n\t\t\t\t\t<fieldset class="rating-readonly dashboard" ng-show="vm.userData.satisfaction_rating">\n\t\t\t\t\t\t<span>4.5</span>\n\t\t\t\t\t\t<input type="radio" id="2star5" name="rating2" value="5" disabled/><label class="full" for="2star5" title="Awesome - 5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star4half" name="rating2" value="4 and a half" checked/><label class="half" for="2star4half" title="Pretty good - 4.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star4" name="rating2" value="4" disabled/><label class = "full" for="2star4" title="Pretty good - 4 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star3half" name="rating2" value="3 and a half" disabled/><label class="half" for="2star3half" title="Meh - 3.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star3" name="rating2" value="3" disabled/><label class = "full" for="2star3" title="Meh - 3 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star2half" name="rating2" value="2 and a half" disabled/><label class="half" for="2star2half" title="Kinda bad - 2.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star2" name="rating2" value="2" disabled/><label class = "full" for="2star2" title="Kinda bad - 2 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star1half" name="rating2" value="1 and a half" disabled/><label class="half" for="2star1half" title="Meh - 1.5 stars"></label>\n\t\t\t\t\t\t<input type="radio" id="2star1" name="rating2" value="1" disabled/><label class = "full" for="2star1" title="Sucks big time - 1 star"></label>\n\t\t\t\t\t\t<input type="radio" id="2starhalf" name="rating2" value="half" disabled/><label class="half" for="2starhalf" title="Sucks big time - 0.5 stars"></label>\n\t\t\t\t\t</fieldset>\n\t\t\t\t\t<span class="rating-empty" ng-show="!vm.userData.satisfaction_rating">- No rating yet -</span>\n\n\t\t\t\t\t<!--<div class="rating medium star-icon direction-ltr value-1 half label-top">-->\n\t\t\t\t\t\t<!--<div class="star-container">-->\n\t\t\t\t\t\t\t<!--<div class="star">-->\n\t\t\t\t\t\t\t\t<!--<i class="star-empty"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-half"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-filled"></i>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t<!--<div class="star">-->\n\t\t\t\t\t\t\t\t<!--<i class="star-empty"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-half"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-filled"></i>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t<!--<div class="star">-->\n\t\t\t\t\t\t\t\t<!--<i class="star-empty"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-half"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-filled"></i>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t<!--<div class="star">-->\n\t\t\t\t\t\t\t\t<!--<i class="star-empty"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-half"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-filled"></i>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t\t<!--<div class="star">-->\n\t\t\t\t\t\t\t\t<!--<i class="star-empty"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-half"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-filled"></i>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t<!--<div class="label-value">4.5</div>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--<div class="rating value-3">-->\n\t\t\t\t\t\t<!--<div class="star-container">-->\n\t\t\t\t\t\t\t<!--<div class="star">-->\n\t\t\t\t\t\t\t\t<!--<i class="star-empty"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-half"></i>-->\n\t\t\t\t\t\t\t\t<!--<i class="star-filled"></i>-->\n\t\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<p class="name">\n\t\t\t\tFirst name:\n\t\t\t\t<b>{{vm.userData.first_name}} {{vm.userData.last_name}}</b>\n\t\t\t</p>\n\n\t\t\t<div class="description">\n\t\t\t\t<label>Profile Description: <small>(Introduction of who you are, what you can do, and fun facts.)</small><a ng-click="vm.changeDescription()">Save</a></label>\n\t\t\t\t<textarea ng-model="vm.vendor_description"></textarea>\n\t\t\t</div>\n\n\t\t\t<div class="list">\n\t\t\t\t<label>List of Jobs willing to do:</label>\n\t\t\t\t<select ng-options="job.title for job in vm.jobTypesData" ng-model="vm.selectedJob">\n\t\t\t\t</select>\n\t\t\t\t<div class="items">\n\t\t\t\t\t<div ng-repeat="item in vm.viewJobRate">{{item.title}} {{item.hourly_rate | currency}}<span ng-click="vm.deleteItem(item)"></span></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class="other">\n\t\t\t\t<div class="rate">\n\t\t\t\t\t<label>Hourly Rate:</label>\n\t\t\t\t\t<div class="input">\n\t\t\t\t\t\t<span>$</span>\n\t\t\t\t\t\t<input type="text" placeholder="Add Amount" ng-model="vm.selectedHour">\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<!--<div class="awards">-->\n\t\t\t\t\t<!--<label>Upload Degrees, Certificates and Awards:</label>-->\n\t\t\t\t\t<!--<div class="input">-->\n\t\t\t\t\t\t<!--<input type="text" placeholder="Browse File" readonly>-->\n\t\t\t\t\t\t<!--<input type="file">-->\n\t\t\t\t\t\t<!--<button>Upload</button>-->\n\t\t\t\t\t<!--</div>-->\n\t\t\t\t<!--</div>-->\n\t\t\t</div>\n\t\t\t<br>\n\t\t\t<button class="profile" ng-click="vm.jobSelectEvent()">Add job</button>\n<!-- \t\t\t<button class="shedule">Schedule Available Time</button>\n -->\n\t\t</div>\n\t</div>\n\n\n\t<div class="right">\n\t\t<div class="completed">\n\t\t\tAll Gigs completed\n\t\t\t<div class="stats">\n\t\t\t\t<div class="earning">\n\t\t\t\t\tEarning\n\t\t\t\t\t<b>$129</b>\n\t\t\t\t</div>\n\n\t\t\t\t<hr>\n\n\t\t\t\t<div class="jobs">\n\t\t\t\t\tPast jobs completed\n\t\t\t\t\t<b>2</b>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<!--<div class="ratings">-->\n\t\t\t<!--<h2>Ratings and Comments-->\n\t\t\t<!--<div class="slide_buttons">-->\n\t\t\t\t<!--<img src="../../assets/img/small_slide_left.png">-->\n\t\t\t\t<!--<img src="../../assets/img/small_slide_right.png">-->\n\t\t\t<!--</div></h2>-->\n\n\t\t\t<!--<div class="item">-->\n\t\t\t\t<!--<b>Sam Billings</b>-->\n\t\t\t\t<!--<p>Neque porro quisquam est qui dolorem ipsum quia dolor Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.sit amet,</p>-->\n\n\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t<!--</div>-->\n\n\t\t\t<!--<hr>-->\n\n\t\t\t<!--<div class="item">-->\n\t\t\t\t<!--<b>Sam Billings</b>-->\n\t\t\t\t<!--<p>Neque porro quisquam est qui dolorem ipsum quia dolor Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.sit amet,</p>-->\n\n\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t\t<!--<img src="../../assets/img/search_user_star_empty.png">-->\n\t\t\t<!--</div>-->\n\t\t<!--</div>-->\n\n\t\t<!--<div class="background">-->\n\t\t\t<!--<img src="../../assets/img/success.png">-->\n\t\t\t<!--<h2>Background Check Passed</h2>-->\n\n\t\t\t<!--<div class="input">-->\n\t\t\t\t<!--<input type="text" placeholder="Upload Background Check" readonly>-->\n\t\t\t\t<!--<input type="file">-->\n\t\t\t\t<!--<button>Upload</button>-->\n\t\t\t<!--</div>-->\n\t\t<!--</div>-->\n\n\t</div>\n\n\n\t<div class="clear"></div>\n\n</div>\n\n\n\n<div ng-include="\'components/partials/_footer.html\'"></div>\n</div>');
$templateCache.put('vendor_orders/vendor_orders.html','<div class="header header_pages">\n\n    <div ng-include="\'../../components/partials/_menu.html\'"></div>\n\n    <div class="page_name wrap">\n        Vendor Orders\n    </div>\n</div>\n\n\n<div class="orders wrap">\n\n    <div class="tabs">\n        <div class="tab" ng-class="vm.status_select == 1 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(1, MainController.accountType)">Pending</div>\n        <div class="tab" ng-class="vm.status_select == 2 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(2, MainController.accountType)">Accepted</div>\n        <div class="tab" ng-class="vm.status_select == 3 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(3, MainController.accountType)">Rejected</div>\n        <div class="tab" ng-class="vm.status_select == 4 ? \'selected\' : \'\'" ng-click="vm.filterByStatus(4, MainController.accountType)">Finished</div>\n        <div class="clear"></div>\n    </div>\n\n    <div class="empty" ng-show="vm.orders.length == 0">\n        Empty category.\n    </div>\n\n    <div class="users pending">\n        <div class="user" ng-repeat="order in vm.orders">\n            <div class="left">\n                <div class="avatar">\n                    <img src="../../assets/img/no_avatar.jpg" ng-if="order.gig.account.avatar_url == null">\n                    <img ng-src="{{ vm.api_img + order.gig.account.avatar_url }}" ng-if="order.gig.account.avatar_url != null">\n                </div>\n            </div>\n\n            <div class="about">\n                <div class="name">\n                    <div class="rating-block">\n                        <div>\n                            <span>Response rating by stars</span>\n                            <fieldset class="rating-readonly" ng-show="order.gig.account.response_rating">\n                                <span>4.0</span>\n                                <input type="radio" id="{{ order.id }}1star5" name="rating1" value="5" disabled/><label class="full" for="{{ order.id }}1star5" title="Awesome - 5 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star4half" name="rating1" value="4 and a half" disabled/><label class="half" for="{{ order.id }}1star4half" title="Pretty good - 4.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star4" name="rating1" value="4" checked/><label class="full" for="{{ order.id }}1star4" title="Pretty good - 4 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star3half" name="rating1" value="3 and a half" /><label class="half" for="1star3half" title="Meh - 3.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star3" name="rating1" value="3" disabled/><label class="full" for="{{ order.id }}1star3" title="Meh - 3 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star2half" name="rating1" value="2 and a half" disabled/><label class="half" for="{{ order.id }}1star2half" title="Kinda bad - 2.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star2" name="rating1" value="2" disabled/><label class="full" for="{{ order.id }}1star2" title="Kinda bad - 2 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star1half" name="rating1" value="1 and a half" disabled/><label class="half" for="{{ order.id }}1star1half" title="Meh - 1.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}1star1" name="rating1" value="1" disabled/><label class="full" for="{{ order.id }}1star1" title="Sucks big time - 1 star"></label>\n                                <input type="radio" id="{{ order.id }}1starhalf" name="rating1" value="half" disabled/><label class="half" for="{{ order.id }}1starhalf" title="Sucks big time - 0.5 stars"></label>\n                            </fieldset>\n                            <span class="rating-empty" ng-show="!order.gig.account.response_rating">- No rating -</span>\n                            <!--<img src="../../assets/img/search_user_star.png">-->\n                            <!--<img src="../../assets/img/search_user_star.png">-->\n                            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                        </div>\n\n                        <div>\n                            <span>Satisfaction rating by stars</span>\n                            <fieldset class="rating-readonly" ng-show="order.gig.account.response_rating">\n                                <span>4.0</span>\n                                <input type="radio" id="{{ order.id }}2star5" name="rating1" value="5" disabled/><label class="full" for="{{ order.id }}2star5" title="Awesome - 5 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star4half" name="rating1" value="4 and a half" disabled/><label class="half" for="{{ order.id }}2star4half" title="Pretty good - 4.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star4" name="rating1" value="4" checked/><label class="full" for="{{ order.id }}2star4" title="Pretty good - 4 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star3half" name="rating1" value="3 and a half" /><label class="half" for="{{ order.id }}2star3half" title="Meh - 3.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star3" name="rating1" value="3" disabled/><label class="full" for="{{ order.id }}2star3" title="Meh - 3 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star2half" name="rating1" value="2 and a half" disabled/><label class="half" for="{{ order.id }}2star2half" title="Kinda bad - 2.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star2" name="rating1" value="2" disabled/><label class="full" for="{{ order.id }}2star2" title="Kinda bad - 2 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star1half" name="rating1" value="1 and a half" disabled/><label class="half" for="{{ order.id }}2star1half" title="Meh - 1.5 stars"></label>\n                                <input type="radio" id="{{ order.id }}2star1" name="rating1" value="1" disabled/><label class="full" for="{{ order.id }}2star1" title="Sucks big time - 1 star"></label>\n                                <input type="radio" id="{{ order.id }}2starhalf" name="rating1" value="half" disabled/><label class="half" for="{{ order.id }}2starhalf" title="Sucks big time - 0.5 stars"></label>\n                            </fieldset>\n                            <span class="rating-empty" ng-show="!order.gig.account.response_rating">- No rating -</span>\n                            <!--<img src="../../assets/img/search_user_star.png">-->\n                            <!--<img src="../../assets/img/search_user_star.png">-->\n                            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                        </div>\n                    </div>\n\n                    <b>{{ order.gig.account.first_name }} {{ order.gig.account.last_name }}</b>\n                    <br>\n                </div>\n\n                <div class="info">\n                    <b>Gig description:</b>\n                    <p>{{ order.gig.description }}</p>\n                </div>\n\n                <div class="rate">\n                    Hourly Rate:\n                    <b>${{ order.gig.price }}</b>\n                </div>\n\n                <div class="hours-worked">\n                    <b>Hours worked:</b>\n                    <input ng-model="vm.data.hours_worked" placeholder="Hours worked" />\n                </div>\n\n                <div class="desc">\n                    <b>Comments:</b>\n                    <textarea ng-model="vm.data.vendor_feedback" placeholder="Message to client"></textarea>\n                </div>\n\n                <div class="response-rating">\n                    <b>Response rating:</b>\n                    <!--<input ng-model="vm.data.response_rating" placeholder="Response rating" />-->\n                    <fieldset class="rating">\n                        <input type="radio" id="1star5" ng-model="vm.data.response_rating" name="rating1" value="5" /><label class = "full" for="1star5" title="Awesome - 5 stars"></label>\n                        <input type="radio" id="1star4half" ng-model="vm.data.response_rating" name="rating1" value="4.5" /><label class="half" for="1star4half" title="Pretty good - 4.5 stars"></label>\n                        <input type="radio" id="1star4" ng-model="vm.data.response_rating" name="rating1" value="4" /><label class = "full" for="1star4" title="Pretty good - 4 stars"></label>\n                        <input type="radio" id="1star3half" ng-model="vm.data.response_rating" name="rating1" value="3.5" /><label class="half" for="1star3half" title="Meh - 3.5 stars"></label>\n                        <input type="radio" id="1star3" ng-model="vm.data.response_rating" name="rating1" value="3" /><label class = "full" for="1star3" title="Meh - 3 stars"></label>\n                        <input type="radio" id="1star2half" ng-model="vm.data.response_rating" name="rating1" value="2.5" /><label class="half" for="1star2half" title="Kinda bad - 2.5 stars"></label>\n                        <input type="radio" id="1star2" ng-model="vm.data.response_rating" name="rating1" value="2" /><label class = "full" for="1star2" title="Kinda bad - 2 stars"></label>\n                        <input type="radio" id="1star1half" ng-model="vm.data.response_rating" name="rating1" value="1.5" /><label class="half" for="1star1half" title="Meh - 1.5 stars"></label>\n                        <input type="radio" id="1star1" ng-model="vm.data.response_rating" name="rating1" value="1" /><label class = "full" for="1star1" title="Sucks big time - 1 star"></label>\n                        <input type="radio" id="1starhalf" ng-model="vm.data.response_rating" name="rating1" value="0.5" /><label class="half" for="1starhalf" title="Sucks big time - 0.5 stars"></label>\n                    </fieldset>\n                </div>\n\n                <div class="satisfaction-rating">\n                    <b>Satisfaction rating:</b>\n                    <!--<input ng-model="vm.data.satisfaction_rating" placeholder="Satisfaction rating" />-->\n                    <fieldset class="rating">\n                        <input type="radio" id="2star5" ng-model="vm.data.satisfaction_rating" name="rating2" value="5" /><label class="full" for="2star5" title="Awesome - 5 stars"></label>\n                        <input type="radio" id="2star4half" ng-model="vm.data.satisfaction_rating" name="rating2" value="4.5" /><label class="half" for="2star4half" title="Pretty good - 4.5 stars"></label>\n                        <input type="radio" id="2star4" ng-model="vm.data.satisfaction_rating" name="rating2" value="4" /><label class = "full" for="2star4" title="Pretty good - 4 stars"></label>\n                        <input type="radio" id="2star3half" ng-model="vm.data.satisfaction_rating" name="rating2" value="3.5" /><label class="half" for="2star3half" title="Meh - 3.5 stars"></label>\n                        <input type="radio" id="2star3" ng-model="vm.data.satisfaction_rating" name="rating2" value="3" /><label class = "full" for="2star3" title="Meh - 3 stars"></label>\n                        <input type="radio" id="2star2half" ng-model="vm.data.satisfaction_rating" name="rating2" value="2.5" /><label class="half" for="2star2half" title="Kinda bad - 2.5 stars"></label>\n                        <input type="radio" id="2star2" ng-model="vm.data.satisfaction_rating" name="rating2" value="2" /><label class = "full" for="2star2" title="Kinda bad - 2 stars"></label>\n                        <input type="radio" id="2star1half" ng-model="vm.data.satisfaction_rating" name="rating2" value="1.5" /><label class="half" for="2star1half" title="Meh - 1.5 stars"></label>\n                        <input type="radio" id="2star1" ng-model="vm.data.satisfaction_rating" name="rating2" value="1" /><label class = "full" for="2star1" title="Sucks big time - 1 star"></label>\n                        <input type="radio" id="2starhalf" ng-model="vm.data.satisfaction_rating" name="rating2" value="0.5" /><label class="half" for="2starhalf" title="Sucks big time - 0.5 stars"></label>\n                    </fieldset>\n                </div>\n\n                <div class="buttons" ng-if="order.status == 1">\n                    <button class="accept">Accept</button>\n                    <button>Decline</button>\n                </div>\n                <div class="buttons" ng-if="order.status == 2">\n                    <button class="accept" ng-click="vm.showSubmitForm(order.id)">Submit work</button>\n                    <button ng-if="vm.data.vendor_feedback.length" ng-click="vm.submitWork()">Send</button>\n                </div>\n                <!--<div class="buttons" ng-if="order.status == 2">-->\n                    <!--<button class="accept">Accept</button>-->\n                    <!--<button>Reject</button>-->\n                <!--</div>-->\n            </div>\n\n            <div class="clear"></div>\n        </div>\n    </div>\n\n\n\n</div>\n\n\n\n<div ng-include="\'../../components/partials/_footer.html\'"></div>\n');
$templateCache.put('vendor_profile/vendor_profile.html','<div ng-if="vm.userData.type == 1 || vm.userData.type !== \'1\' || vm.userData.type == 3 || vm.userData.type !== \'3\'">\n    <div class="header header_pages search_page_header">\n\n        <div ng-include="\'components/partials/_menu.html\'"></div>\n\n        <div class="page_name wrap">\n            Vendor Profile\n        </div>\n    </div>\n\n\n    <div class="vendordash wrap">\n\n        <div class="left">\n            <div class="user">\n                <div class="avatar">\n                    <!--<div style="height: 100%; width: 100%;" ng-if="vm.userData.avatar_url" ng-style="{\'backgroundImage\': \'url(http://26756.s.t4vps.eu/api/v1/client{{vm.userData.avatar_url}})\'}"></div>-->\n                    <img style="height: 120px; width: 120px;" ng-src="{{ vm.api_img + vm.profile.avatar_url}}" ng-if="vm.profile.avatar_url != null">\n                    <img style="height: 120px; width: 120px;" src="../../assets/img/no_avatar.jpg" ng-if="vm.profile.avatar_url == null">\n                </div>\n                <div class="info">\n                    <div class="status">\n                        <div ng-class="vm.profile.vendor_status == 1 ? \'selected\' : \'\'" class="av readlonly"><span></span>Available</div>\n                        <div ng-class="vm.profile.vendor_status == 2 ? \'selected not_av\' : \'\'" class="av readlonly"><span></span>Not Available</div>\n                    </div>\n                    <div class="clear"></div>\n\n                    <div class="rating-block">\n                        <span>Response rating by stars</span>\n                        <fieldset class="rating-readonly dashboard" ng-show="vm.profile.response_rating">\n                            <span>4.0</span>\n                            <input type="radio" id="1star5" name="rating1" value="5" disabled/><label class = "full" for="1star5" title="Awesome - 5 stars"></label>\n                            <input type="radio" id="1star4half" name="rating1" value="4 and a half" disabled/><label class="half" for="1star4half" title="Pretty good - 4.5 stars"></label>\n                            <input type="radio" id="1star3half" name="rating1" value="3 and a half" /><label class="half" for="1star3half" title="Meh - 3.5 stars"></label>\n                            <input type="radio" id="1star3" name="rating1" value="3" disabled/><label class = "full" for="1star3" title="Meh - 3 stars"></label>\n                            <input type="radio" id="1star2half" name="rating1" value="2 and a half" disabled/><label class="half" for="1star2half" title="Kinda bad - 2.5 stars"></label>\n                            <input type="radio" id="1star2" name="rating1" value="2" disabled/><label class = "full" for="1star2" title="Kinda bad - 2 stars"></label>\n                            <input type="radio" id="1star1half" name="rating1" value="1 and a half" disabled/><label class="half" for="1star1half" title="Meh - 1.5 stars"></label>\n                            <input type="radio" id="1star1" name="rating1" value="1" disabled/><label class = "full" for="1star1" title="Sucks big time - 1 star"></label>\n                            <input type="radio" id="1starhalf" name="rating1" value="half" disabled/><label class="half" for="1starhalf" title="Sucks big time - 0.5 stars"></label>\n                        </fieldset>\n                        <span class="rating-empty" ng-show="!vm.profile.response_rating">- No rating yet -</span>\n                        <!--<img src="../../assets/img/search_user_star.png">-->\n                        <!--<img src="../../assets/img/search_user_star.png">-->\n                        <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                        <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                        <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                    </div>\n\n                    <div class="rating-block">\n                        <span>Satisfaction rating by stars</span>\n                        <fieldset class="rating-readonly dashboard" ng-show="vm.profile.satisfaction_rating">\n                            <span>4.5</span>\n                            <input type="radio" id="2star5" name="rating2" value="5" disabled/><label class="full" for="2star5" title="Awesome - 5 stars"></label>\n                            <input type="radio" id="2star4half" name="rating2" value="4 and a half" checked/><label class="half" for="2star4half" title="Pretty good - 4.5 stars"></label>\n                            <input type="radio" id="2star4" name="rating2" value="4" disabled/><label class = "full" for="2star4" title="Pretty good - 4 stars"></label>\n                            <input type="radio" id="2star3half" name="rating2" value="3 and a half" disabled/><label class="half" for="2star3half" title="Meh - 3.5 stars"></label>\n                            <input type="radio" id="2star3" name="rating2" value="3" disabled/><label class = "full" for="2star3" title="Meh - 3 stars"></label>\n                            <input type="radio" id="2star2half" name="rating2" value="2 and a half" disabled/><label class="half" for="2star2half" title="Kinda bad - 2.5 stars"></label>\n                            <input type="radio" id="2star2" name="rating2" value="2" disabled/><label class = "full" for="2star2" title="Kinda bad - 2 stars"></label>\n                            <input type="radio" id="2star1half" name="rating2" value="1 and a half" disabled/><label class="half" for="2star1half" title="Meh - 1.5 stars"></label>\n                            <input type="radio" id="2star1" name="rating2" value="1" disabled/><label class = "full" for="2star1" title="Sucks big time - 1 star"></label>\n                            <input type="radio" id="2starhalf" name="rating2" value="half" disabled/><label class="half" for="2starhalf" title="Sucks big time - 0.5 stars"></label>\n                        </fieldset>\n                        <span class="rating-empty" ng-show="!vm.profile.satisfaction_rating">- No rating yet -</span>\n\n                        <!--<div class="rating medium star-icon direction-ltr value-1 half label-top">-->\n                        <!--<div class="star-container">-->\n                        <!--<div class="star">-->\n                        <!--<i class="star-empty"></i>-->\n                        <!--<i class="star-half"></i>-->\n                        <!--<i class="star-filled"></i>-->\n                        <!--</div>-->\n                        <!--<div class="star">-->\n                        <!--<i class="star-empty"></i>-->\n                        <!--<i class="star-half"></i>-->\n                        <!--<i class="star-filled"></i>-->\n                        <!--</div>-->\n                        <!--<div class="star">-->\n                        <!--<i class="star-empty"></i>-->\n                        <!--<i class="star-half"></i>-->\n                        <!--<i class="star-filled"></i>-->\n                        <!--</div>-->\n                        <!--<div class="star">-->\n                        <!--<i class="star-empty"></i>-->\n                        <!--<i class="star-half"></i>-->\n                        <!--<i class="star-filled"></i>-->\n                        <!--</div>-->\n                        <!--<div class="star">-->\n                        <!--<i class="star-empty"></i>-->\n                        <!--<i class="star-half"></i>-->\n                        <!--<i class="star-filled"></i>-->\n                        <!--</div>-->\n                        <!--</div>-->\n                        <!--<div class="label-value">4.5</div>-->\n                        <!--</div>-->\n                        <!--<div class="rating value-3">-->\n                        <!--<div class="star-container">-->\n                        <!--<div class="star">-->\n                        <!--<i class="star-empty"></i>-->\n                        <!--<i class="star-half"></i>-->\n                        <!--<i class="star-filled"></i>-->\n                        <!--</div>-->\n                        <!--</div>-->\n                        <!--</div>-->\n                        <!--<img src="../../assets/img/search_user_star.png">-->\n                        <!--<img src="../../assets/img/search_user_star.png">-->\n                        <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                        <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                        <!--<img src="../../assets/img/search_user_star_empty.png">-->\n                    </div>\n                </div>\n\n                <p class="name">\n                    Full name:\n                    <b>{{vm.profile.first_name}} {{vm.profile.last_name}}</b>\n                </p>\n\n                <div class="description" ng-show="vm.profile.vendor_description">\n                    <label>Profile Description:</label>\n                    <p>{{ vm.profile.vendor_description }}</p>\n                </div>\n\n                <div class="list">\n                    <label>List of Jobs willing to do:</label>\n                    <!--<select ng-options="job.title for job in vm.jobTypesData" ng-model="vm.selectedJob">-->\n                    <!--</select>-->\n                    <div class="items-jobs">\n                        <div ng-repeat="item in vm.profile.job_types">{{item.title}} {{item.hourly_rate | currency}}</div>\n                    </div>\n                </div>\n                <div class="list buttons" style="line-height: 1;">\n                    <label>Contact info:</label>\n                    <p style="font-size: 12px; line-height: 1; font-family: \'Open Sans\';">Phone: {{vm.profile.phone}}</p>\n                    <p style="font-size: 12px; line-height: 1; font-family: \'Open Sans\';">Alternative phone: {{vm.profile.alt_phone}}</p>\n                    <p style="font-size: 12px; line-height: 1; font-family: \'Open Sans\';">Skype: {{vm.profile.skype}}</p>\n                </div>\n\n                <!--<div class="other">-->\n                    <!--<div class="rate">-->\n                        <!--<label>Hourly Rate:</label>-->\n                        <!--<div class="input">-->\n                            <!--<span>$</span>-->\n                            <!--<input type="text" placeholder="Add Amount" ng-model="vm.selectedHour">-->\n                        <!--</div>-->\n                    <!--</div>-->\n\n                    <!--<div class="awards">-->\n                    <!--<label>Upload Degrees, Certificates and Awards:</label>-->\n                    <!--<div class="input">-->\n                    <!--<input type="text" placeholder="Browse File" readonly>-->\n                    <!--<input type="file">-->\n                    <!--<button>Upload</button>-->\n                    <!--</div>-->\n                    <!--</div>-->\n                <!--</div>-->\n                <!--<br>-->\n                <!--<button class="shedule">Schedule Available Time</button>-->\n\n            </div>\n        </div>\n\n\n        <div class="right">\n            <div class="completed">\n                All Gigs completed\n                <div class="stats">\n                    <div class="earning">\n                        Earning\n                        <b>$129</b>\n                    </div>\n\n                    <hr>\n\n                    <div class="jobs">\n                        Past jobs completed\n                        <b>2</b>\n                    </div>\n                </div>\n            </div>\n\n            <!--<div class="ratings">-->\n            <!--<h2>Ratings and Comments-->\n            <!--<div class="slide_buttons">-->\n            <!--<img src="../../assets/img/small_slide_left.png">-->\n            <!--<img src="../../assets/img/small_slide_right.png">-->\n            <!--</div></h2>-->\n\n            <!--<div class="item">-->\n            <!--<b>Sam Billings</b>-->\n            <!--<p>Neque porro quisquam est qui dolorem ipsum quia dolor Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.sit amet,</p>-->\n\n            <!--<img src="../../assets/img/search_user_star.png">-->\n            <!--<img src="../../assets/img/search_user_star.png">-->\n            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            <!--</div>-->\n\n            <!--<hr>-->\n\n            <!--<div class="item">-->\n            <!--<b>Sam Billings</b>-->\n            <!--<p>Neque porro quisquam est qui dolorem ipsum quia dolor Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.sit amet,</p>-->\n\n            <!--<img src="../../assets/img/search_user_star.png">-->\n            <!--<img src="../../assets/img/search_user_star.png">-->\n            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            <!--<img src="../../assets/img/search_user_star_empty.png">-->\n            <!--</div>-->\n            <!--</div>-->\n\n            <!--<div class="background">-->\n            <!--<img src="../../assets/img/success.png">-->\n            <!--<h2>Background Check Passed</h2>-->\n\n            <!--<div class="input">-->\n            <!--<input type="text" placeholder="Upload Background Check" readonly>-->\n            <!--<input type="file">-->\n            <!--<button>Upload</button>-->\n            <!--</div>-->\n            <!--</div>-->\n\n        </div>\n\n\n        <div class="clear"></div>\n\n    </div>\n\n\n\n    <div ng-include="\'components/partials/_footer.html\'"></div>\n</div>');}]);
/**
 * Created by Eduard Latsevich on 004 04.12.16.
 */
angular
  .module('zippyApp')
  .directive('askFaq', function() {
    return {
      restrict: 'AE',
      link: function(scope, element, attrs) {
        var el = element;

        if ( el ) {
          el.click(function(){
            el.parent().toggleClass("open");
            el.parent().find(".answer").toggle("slow");
          });
        }
      }
    };
  });
/**
 * Created by Eduard Latsevich on 001 01.12.16.
 */
angular
    .module('zippyApp')
    .directive('editButtonEvent', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                var el = element;
                element.click(function(){
                    var input = element.parent().find("input");

                    if (input.length === 0){
                        input = element.parent().find("select");
                    }

                    var disabled = input.prop('disabled');
                    input.prop('disabled', !disabled);
                    input.toggleClass("editable");
                    input.focus();

                    if (!input.hasClass("select")) {
                        var length = input.val().length;
                        input[0].setSelectionRange(length, length);
                    }
                });
            }
        };
    });
/**
 * Created by Eduard Latsevich on 001 01.12.16.
 */
angular
    .module('zippyApp')
    .directive("fileRead", [function () {
        return {
            scope: {
                fileRead: "="
            },
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.bind("change", function (changeEvent) {
                    var file = changeEvent.target.files[0];
                    var reader = new FileReader();

                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileRead = {
                                file: file,
                                data: loadEvent.target.result
                            };

                            if (ngModel) {
                                ngModel.$setViewValue(file);
                            }
                        });
                    };

                    reader.readAsDataURL(file);
                });
            }
        };
    }]);
angular
    .module('zippyApp')
    .directive('loseBlurEvent', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                var el = element;
                element.blur(function(){
                    element.removeClass("editable");
                    element.prop('disabled', true);
                    });
            }
        };
    });
/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .factory('UserFactory', UserFactory);

    UserFactory.$inject = ['NotificationService', 'NOTIFICATION_EVENT'];

    function User(isLoggedIn) {

        this.userData = {
            id: '',
            email: '',
            emailConfirmed: false,

            login: null
        };

        this.loggined = isLoggedIn || false;

    }

    function UserFactory(NotificationService, NOTIFICATION_EVENT) {

        var user = new User();

        var service = {
            getUser: getUser,
            getUserType: getUserType,
            getUserData: getUserData,
            setUserData: setUserData,
            resetData: resetData,
            isUserLoggedIn: isUserLoggedIn,
            isFirstLogin: isFirstLogin,
            setIsLoggedIn: setIsLoggedIn,
            setUserId: setUserId,
            getUserId: getUserId
        };

        return service;

        function getUser() {
            return user;
        }

        function getUserType() {
            return user.userData.type;
        }

        function setUserData(_dataObj) {
            for (var prop in _dataObj) {
                user.userData[prop] = _dataObj[prop];
            }
            NotificationService.notify(NOTIFICATION_EVENT.USER_DATA_UPDATED);
        }

        function resetData() {
            // user = new User(!!session.get());
            user = new User();
            NotificationService.notify(NOTIFICATION_EVENT.USER_DATA_UPDATED);
        }

        function getUserData() {
            user.userData.type = String(user.userData.type);
            console.log("user.userData:", user.userData)
            return user.userData;
        }

        function setUserId(_id) {
            user.userData.id = _id;
        }

        function getUserId() {
            return user.userData.id;
        }

        function setIsLoggedIn(_loggedIn) {
            user.loggined = _loggedIn;
            NotificationService.notify(NOTIFICATION_EVENT.USER_LOGGED_IN_OUT);
        }

        function isUserLoggedIn() {
            return user.loggined;
        }

        function isFirstLogin() {
            return user.userData.first_login;
        }

    }

})(window.angular);
/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.service('ApiService', ApiService);

	ApiService.$inject = ['$q', 'ConfigService', '$httpParamSerializerJQLike'];

	function ApiService($q, ConfigService, $httpParamSerializerJQLike) {

		var service = {
			signin: signin,
			signup: signup,
			search: search,
			forgotPassword: forgotPassword,
			resetPassword: resetPassword,
      		searchGigs: searchGigs,
			searchHire: searchHire,
			accountGet: accountGet,
			accountSave: accountSave,
			accountAvatar: accountAvatar,
			getJobTypes: getJobTypes,
			getOrders: getOrders,
			postVendorJobs: postVendorJobs,
			deleteVendorJobs: deleteVendorJobs,
			postVendorDescription: postVendorDescription,
      		postStatus: postStatus,
			postGig: postGig,
			createOrder: createOrder,
			finishOrder: finishOrder,
			payForWorkDone: payForWorkDone,
			getProfile: getProfile,
			getBraintreeClientToken: getBraintreeClientToken,
			createSubscription: createSubscription
		};

		return service;



		function signin (login, password) {
			var deferred = $q.defer();

			var _data = {email: login, password: password};

			ConfigService.sendRequest('post', 'api/v1/auth/token/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function signup (login, password) {
			var deferred = $q.defer();

			var _data = {email: login, password: password};

			ConfigService.sendRequest('post', 'api/v1/auth/signup/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}


		function forgotPassword(login) {
			var deferred = $q.defer();
			
			var _data = {email: login};

			ConfigService.sendRequest('post', 'api/v1/auth/forgot-password/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function resetPassword(token, password) {
			var deferred = $q.defer();
			
			var _data = {password: password};

			ConfigService.sendRequest('post', 'api/v1/auth/reset/'+token, _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

			function search (_params) {
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/client/vendors/', null, _params).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function searchGigs (_params) {
			  var deferred = $q.defer();

			  ConfigService.sendRequest('get', 'api/v1/vendor/gigs/', null, _params).then(
				function (response) {
				  deferred.resolve(response);
				}
			  );

			  return deferred.promise;
			}

			function searchHire (_params) {
				var deferred = $q.defer();

				ConfigService.sendRequest('post', 'api/v1/gig/', _params).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;

			}

			function accountGet () {
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/client/profile/').then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function accountSave (account) {
				var deferred = $q.defer();

				ConfigService.sendRequest('post', 'api/v1/auth/profile/', account).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;

			}

			function accountAvatar (photo) {
				var deferred = $q.defer();

				ConfigService.sendRequest('post', 'api/v1/client/avatar/', photo, null, false, true).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function getJobTypes () {
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/client/job-types/').then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

			function getOrders (_data) {
				console.log("_data", _data)
				var deferred = $q.defer();

				ConfigService.sendRequest('get', 'api/v1/order/get-orders/', null, _data).then(
					function (response) {
						deferred.resolve(response);
					}
				);

				return deferred.promise;
			}

		function postVendorJobs (_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/vendor/add-job-type/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function deleteVendorJobs(_data){
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/vendor/remove-job-type/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function postVendorDescription(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/vendor/description/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function postStatus(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/vendor/status/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function postGig(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/gig/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function createOrder(_data){
		  var deferred = $q.defer();

		  ConfigService.sendRequest('post', 'api/v1/order/create-order/', _data).then(
			function (response) {
			  deferred.resolve(response);
			}
		  );

		  return deferred.promise;
		}

		function finishOrder(_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/order/finish-order/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);
			return deferred.promise;
		}

		function payForWorkDone(_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/payment/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);
			return deferred.promise;
		}

		function getProfile(id) {
			var deferred = $q.defer();

			ConfigService.sendRequest('get', 'api/v1/client/profile/'+id+'/', null).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function getBraintreeClientToken() {
			var deferred = $q.defer();

			ConfigService.sendRequest('get', 'api/v1/payment/client_token/', null).then(
				function (response) {
					deferred.resolve(response);
				}
			);

			return deferred.promise;
		}

		function createSubscription(_data) {
			var deferred = $q.defer();

			ConfigService.sendRequest('post', 'api/v1/payment/create-subscription/', _data).then(
				function (response) {
					deferred.resolve(response);
				}
			);
			return deferred.promise;
		}
	}

})(window.angular);

















/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .service('ConfigService', ConfigService);

    ConfigService.$inject = ['$q', '$http', '$state',
        'Notification', 'UserFactory'];

    function ConfigService($q, $http, $state,
                        Notification, UserFactory) {

        var _activeRequestsArr = [];

        var service = {
            getUrl: getUrl,
            sendRequest: sendRequest
        };

        return service;

        function getUrl(_path) {
            return "http://127.0.0.1:5000/" + _path; //"http://host.zippygigs.com/" + _path; 
        }

        function sendRequest(_method, _url, _data, _params, _ignoreMultiple, _formData) {
            var deferred = $q.defer();

            if (isActiveRequest(getUrl(_url)) && !_ignoreMultiple && !_formData) {
                deferred.reject();
            }
            else {

                var req = {
                    method: _method,
                    url: getUrl(_url),
                    data: _data,
                    params: _params
                };

                if (_formData) {
                    req.headers = {
                        'Content-Type': undefined
                    };
                }

                pushRequest(req.url);

                $http(req).then(

                    function (response) {

                        popRequest(response.config.url);
                        deferred.resolve(response);
                    },

                    function (response) {

                        deferred.reject(response);
                        popRequest(response.config.url);

                        var _errText = '';

                        if (response.data.error) {
                          _errText = response.data.error;
                            if(_errText === 'Unauthorized Access'){
                                UserFactory.setIsLoggedIn(false);
                                localStorage.removeItem("token");
                                localStorage.removeItem("login");
                                localStorage.removeItem("token_end");
                                $state.reload();
                            }
                        }
                        else {
                            _errText = 'Неизвестная ошибка, попробуйте снова!';
                        }

                        console.log(_errText);

                    });

            }

            return deferred.promise;
        }

        function isActiveRequest(_url) {
            return _activeRequestsArr.indexOf(_url) > -1;
        }

        function pushRequest(_url) {
            _activeRequestsArr.push(_url);
        }

        function popRequest(_url) {
            var reqIdx = _activeRequestsArr.indexOf(_url);
            _activeRequestsArr.splice(reqIdx, 1);
        }
    }

})(window.angular);

(function(angular){
    angular
        .module('angularNotification', ['ng']);

    angular
        .module('angularNotification')
      .constant('NOTIFICATION_EVENT', {
        API_ERROR_UNAUTHORIZED: 'apiError:unauthorized',
        SESSION_CREATED: 'session:created',

        USER_LOGGED_IN_OUT: 'UserFactory:userLoggedOut',
        USER_DATA_UPDATED: 'UserFactory:userDataUpdated'
      })
        .service('NotificationService', [function () {

        var callbacks = {};

        var service = {
            register: addCallback,
            unregister: removeCallback,
            notify: notify
        };

        return service;

        function addCallback(scope, channel, cb) {
            if (!callbacks[channel]) {
                callbacks[channel] = [];
            }

            if (callbacks[channel].indexOf(cb) == -1) {
                callbacks[channel].push(cb);

                scope.$on('$destroy', function () {
                    removeCallback(channel, cb);
                });
            }
        }

        function removeCallback(channel, cb) {
            if (callbacks[channel]) {
                var cbidx = callbacks[channel].indexOf(cb);
                if (cbidx != -1) {
                    callbacks[channel].splice(cbidx, 1);
                }
            }
        }

        function notify(channel) {
            var notifyArgs = Array.prototype.slice.call(arguments, 1);

            if (callbacks[channel]) {
                callbacks[channel].forEach(function (cb) {
                    cb.apply(undefined, notifyArgs);
                });
            }
        }

    }]);
})(window.angular);

/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    'use strict';

    angular
        .module('seoProvider', [])
        .provider('seoProvider', function () {
            return {
                $get: ['$rootScope', 'seoProvider.helper', seoProvider]
            };
        })
        .factory("seoProvider.helper", [seoHelper]);

    function seoProvider($rootScope, stringHelperService) {

        $rootScope.seo = {
            title: '',
            description: '',
            keywords: '',
            author: ''
        };

        function setTitle(newTitle) {
            $rootScope.seo.title = newTitle;
        }

        function setDescription(description) {
            var result = '';
            if (description && description.length) {
                var softLength =
                    stringHelperService.getSoftLengthOfSentenceEnd(description, 255, true);
                result = description.slice(0, softLength ? softLength : 100).trim();
            }

            $rootScope.seo.description = result;
        }

        function setKeywords(keywords) {
            var result = '';
            if (keywords && keywords.length) {
                var softLength =
                    stringHelperService.getSoftLengthOfSentenceEnd(keywords, 100, true);
                result = keywords.slice(0, softLength ? softLength : 100).trim();
            }

            $rootScope.seo.keywords = result;
        }

        function setAuthor(author) {
            var result = '';
            if (author && author.length) {
                result = author.trim();
            }

            $rootScope.seo.author = result;
        }

        return {
            setTitle: setTitle,
            setDescription: setDescription,
            setKeywords: setKeywords,
            setAuthor: setAuthor
        };

    }

    function seoHelper() {

        function getSoftLengthOfWordEnd(string, preferLength, hardLimit) {

            if (!string || string.length < preferLength) {
                return preferLength;
            }

            var lastDotBefore = string.lastIndexOf(' ', preferLength);

            if (hardLimit) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                return false;
            }

            var firstDotAfter = string.indexOf(' ', preferLength);

            if (lastDotBefore === -1 || firstDotAfter === -1) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                if (firstDotAfter !== -1) {
                    return firstDotAfter + 1;
                }
                return false;
            }

            if (preferLength - lastDotBefore > firstDotAfter - preferLength) {
                return firstDotAfter + 1;
            }

            return lastDotBefore + 1;
        }

        function getSoftLengthOfSentenceEnd(string, preferLength, hardLimit) {

            if (!string || string.length < preferLength) {
                return preferLength;
            }

            var lastDotBefore = string.lastIndexOf('.', preferLength);

            if (lastDotBefore < string.lastIndexOf('!', preferLength)) {
                lastDotBefore = string.lastIndexOf('!', preferLength);
            }
            if (lastDotBefore < string.lastIndexOf('?', preferLength)) {
                lastDotBefore = string.lastIndexOf('?', preferLength);
            }

            if (hardLimit) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                return false;
            }

            var firstDotAfter = string.indexOf('.', preferLength);
            var firstExclamationBefore = string.indexOf('!', preferLength);
            if (firstExclamationBefore !== -1 && firstDotAfter > firstExclamationBefore) {
                firstDotAfter = firstExclamationBefore;
            }
            var firstQuestionBefore = string.indexOf('?', preferLength);
            if (firstQuestionBefore !== -1 && firstDotAfter > firstQuestionBefore) {
                firstDotAfter = firstQuestionBefore;
            }

            if (lastDotBefore === -1 || firstDotAfter === -1) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                if (firstDotAfter !== -1) {
                    return firstDotAfter + 1;
                }
                return false;
            }

            if (preferLength - lastDotBefore > firstDotAfter - preferLength) {
                return firstDotAfter + 1;
            }

            return lastDotBefore + 1;
        }


        return {
            getSoftLengthOfWordEnd: getSoftLengthOfWordEnd,
            getSoftLengthOfSentenceEnd: getSoftLengthOfSentenceEnd
        };

    }

})(window.angular);

/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    angular
        .module('zippyApp')
        .service('session', ['$http', 'NotificationService', 'NOTIFICATION_EVENT', 'UserFactory', 'ApiService', '$state', session]);

    function session($http, NotificationService, NOTIFICATION_EVENT, UserFactory, ApiService, $state) {

        var date = new Date();
        var timestamp = Math.floor(date.getTime() / 1000);

        var service = {
            create: create,
            clear: clear,
            check: check
        };

        return service;

        function setAuthHeader(token) {
            $http.defaults.headers.common.Authorization = "Token " + token;
        }

        function create(_oauthToken, _login) {
            setAuthHeader(_oauthToken);
            localStorage.token = _oauthToken;
            localStorage.login = _login;
            localStorage.token_end = timestamp + (10 * 60);
            UserFactory.setIsLoggedIn(true);

            var obj = {};

            ApiService.accountGet().then(function(resp){
                obj = resp.data.data;
                obj.login = localStorage.login;
                UserFactory.setUserData(obj);
            });

            NotificationService.notify(NOTIFICATION_EVENT.USER_LOGGED_IN_OUT);
        }

        function clear() {
            setAuthHeader(null);

            UserFactory.setIsLoggedIn(false);
            UserFactory.resetData();

            localStorage.removeItem("token");
            localStorage.removeItem("login");
            localStorage.removeItem("token_end");
            localStorage.removeItem("accountType");

            NotificationService.notify(NOTIFICATION_EVENT.USER_LOGGED_IN_OUT);
            $state.go('home');
        }

        function check() {
            if(localStorage.token_end > timestamp){
                return true;
            }
            else{
                if (!localStorage.token_end) {
                    return false;
                }
                clear();
                return false;
            }
        }

    }

})(window.angular);
(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('AccountController', AccountController);

	AccountController.$inject = ['$log', '$scope', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'NotificationService', 'ConfigService', 'NOTIFICATION_EVENT', '$timeout'];

	function AccountController ($log, $scope, ngToast,
							   seoProvider, UserFactory, ApiService, NotificationService, ConfigService, NOTIFICATION_EVENT, $timeout) {
		$log.log ('AccountController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.accountSave = accountSave;
		vm.avatarSave = avatarSave;
		vm.onUserDataUpdated = onUserDataUpdated;
		$scope.uploadAvatar = uploadAvatar;

    	vm.userData = UserFactory.getUserData();

		vm.accountSaved = false;
		vm.api_img = ConfigService.getUrl('api/v1/client');
		vm.avatar = vm.api_img + vm.userData.avatar_url;
		document.getElementById('avatar').style.backgroundImage = 'url('+vm.avatar+')';	

		NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

		function onUserDataUpdated(){
			vm.userData = UserFactory.getUserData();
			if(vm.userData.login){
				vm.userData.type = String(vm.userData.type);
			}
			if (vm.userData.avatar_url) {
				vm.avatar = vm.api_img + vm.userData.avatar_url;
			}
		}

		function accountSave() {
			vm.accountSaved = true;
			console.log("vm.userData for saving:", vm.userData)
			ApiService.accountSave(vm.userData).then(accountSaveCallback, null);
		}

		function uploadAvatar(element) {
			$scope.$apply(function(scope) {
		        var photofile = element.files[0];
		        var reader = new FileReader();
			        reader.onload = function(e) {
			            // handle onload
			            vm.avatar = reader.result;
			            document.getElementById('avatar').style.backgroundImage = 'url('+vm.avatar+')';	
						var data = new FormData();
							data.append('avatar', photofile);
							ApiService.accountAvatar(data).then(accountAvatarCallback, null);
					};
			        reader.readAsDataURL(photofile);
			});
		}

		function avatarSave() {
			angular.element(document.getElementById('file_avatar')).click();
		}

//Callbacks

		function accountAvatarCallback(data){
			console.log("accountAvatarCallback:", data)
      		vm.userData.avatar_url = data.data.data.url;
      		vm.avatar = vm.api_img + vm.userData.avatar_url;
		}

		function accountSaveCallback(data){
			if (data.data.result === false) {
				ngToast.create({
				  className: 'danger',
				  content: data.data.error
				});
				$timeout(function(){
					vm.accountSaved = false;
				}, 2000);
			} else {
				$timeout(function(){
					vm.accountSaved = false;
					window.location.reload()
				}, 2000);
			}
		}

	}

})(window.angular);








/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
  'use strict';

  angular
    .module('zippyApp')
    .controller('CustomerDashboardController', CustomerDashboardController);

  CustomerDashboardController.$inject = ['$log', '$scope', '$rootScope',
    'seoProvider', 'ApiService', 'UserFactory', 'NotificationService', 'NOTIFICATION_EVENT', '$state'];

  function CustomerDashboardController($log, $scope, $rootScope,
                                     seoProvider, ApiService, UserFactory, NotificationService, NOTIFICATION_EVENT, $state) {
    $log.log('CustomerDashboardController');

    var vm = this;

    seoProvider.setTitle ('Zippy');
    seoProvider.setDescription ('Zippy');
    seoProvider.setKeywords ('Zippy');
    seoProvider.setAuthor ('Zippy');

    vm.onUserDataUpdated = onUserDataUpdated;
    vm.postGig = postGig;
    vm.selectedGig = selectedGig;

    vm.userData = UserFactory.getUserData();

    vm.jobTypesData = {};
    vm.selectedJob = '';
    vm.selectedHour = 0;
    vm.finishedGigs = [];
    vm.description = '';
    vm.api_img = 'http://host.zippygigs.com/api/v1/client';

    checkPermission();
    initData();


    NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

    function onUserDataUpdated(){
      vm.userData = UserFactory.getUserData();
      initData();
    }

    function checkPermission(){
      if(vm.userData.type === '2' || vm.userData.type === 2){
        $state.transitionTo('home');
      }
    }

    function initData(){
      if(vm.userData.login){
        checkPermission();
        vm.userData.type = String(vm.userData.type);
      }
    }

    ApiService.getJobTypes().then(getJobTypesCallback, null);
    ApiService.getOrders({status: 4, order_type: "customer"}).then(getOrdersCallback, null);

    function postGig(){
      $rootScope.midLoader = true;
      console.log(vm.selectedJob);
      console.log(vm.selectedHour);
      console.log(vm.description);
      var button = $(event.target);
      button.find(".success").fadeIn("slow");
      setTimeout(function(){
        button.find(".success").fadeOut("slow");
      }, 2000);
      $(event.target).parent().parent().find(".desc").toggle(500);
      ApiService.postGig({type: vm.selectedJob.id, description: vm.description, price: vm.selectedHour}).then(postGigCallback, null);
    }

    function selectedGig(item){
      vm.selectedGigItem = item;
    }

    //callbacks

    function getJobTypesCallback(resp){
      vm.jobTypesData = resp.data.data.job_types;
      vm.selectedJob = vm.jobTypesData[0];
    }

    function getOrdersCallback(resp){
      vm.finishedGigs = resp.data.data.orders;
      console.log(vm.finishedGigs);
      console.log(vm.jobTypesData);
    }

    function postGigCallback(resp){
      vm.description = '';
      $rootScope.midLoader = false;
    }

  }

})(window.angular);

(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('OrdersController', OrdersController);

	OrdersController.$inject = ['$log', '$state',
		'seoProvider', 'UserFactory', 'ApiService'];

	function OrdersController ($log, $state,
							seoProvider, UserFactory, ApiService) {
		$log.log ('OrdersController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.api_img = 'http://host.zippygigs.com/api/v1/client';
		vm.orders = [];
		vm.status_select = 1;
		vm.data = {};

		vm.filterByStatus = filterByStatus;
		vm.filterByStatus(1);
		vm.showSubmitForm = showSubmitForm;
		vm.submitWork = submitWork;
		vm.payForGig = payForGig;


		function filterByStatus(status, accountType) {
			console.log("acc type:", accountType)
			vm.orders = [];
			vm.status_select = status;
			ApiService.getOrders({status: status, order_type: accountType}).then(getOrdersCallback, null);
		}

		function showSubmitForm(order) {
			$(event.target).parent().parent().find(".desc").toggle(500);
			$(event.target).parent().parent().find(".hours-worked").toggle(500);
			$(event.target).parent().parent().find(".response-rating").toggle(500);
			$(event.target).parent().parent().find(".satisfaction-rating").toggle(500);
			vm.data.order = order;
		}

		function submitWork() {
			console.log({
					order: vm.data.order,
					hours_worked: vm.data.hours_worked,
					vendor_feedback: vm.data.vendor_feedback,
					response_rating: parseInt(vm.data.response_rating),
					satisfaction_rating: parseInt(vm.data.satisfaction_rating)
				});
			ApiService.finishOrder({
				order: vm.data.order,
				vendor_feedback: vm.data.vendor_feedback,
				hours_worked: vm.data.hours_worked,
				response_rating: parseInt(vm.data.response_rating),
				satisfaction_rating: parseInt(vm.data.satisfaction_rating)
			}).then(finishOrderCallback, null);
		}

		function payForGig(order_id) {
			ApiService.payForWorkDone({
				order_id: order_id
			}).then(payForGigCallback, null)
		}

		// callbacks
		function getOrdersCallback(data){
			var orders = data.data.data.orders;
			orders.forEach(function(item){
				vm.orders.push(item);
			});
			$log.log(vm.orders);
		}

		function finishOrderCallback(data) {
			console.log("finishOrderResult:", data)
		}

		function payForGigCallback(data) {
			console.log("payForGigCallback:", data);
			vm.paymentSuccesUrl = data.data.data;
			console.log("vm.paymentSuccesUrl: ", vm.paymentSuccesUrl)
			if (vm.paymentSuccesUrl) {
				window.location = vm.paymentSuccesUrl;
			}
		}


// 		vm.status_select = 1;
// 		vm.orders = [null,[],[],[],[]];
//
// 		ApiService.getOrders().then(getOrdersCallback, null);
//
// 		function classSelected(status) {
// 			if (vm.status_select === status){
// 				return "selected";
// 			}
// 		}
//
// 		function changeSelected(status) {
// 			vm.status_select = status;
// 		}
//
// //callbacks
//
// 		function getOrdersCallback(data){
// 			var orders = data.data.data.orders;
// 			orders.forEach(function(item){
// 				var status = item.status;
// 				vm.orders[status].push(item);
// 			});
// 			$log.log(vm.orders);
// 		}

	}

})(window.angular);













(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('FaqController', FaqController);

	FaqController.$inject = ['$log', '$state',
		'seoProvider', 'UserFactory', 'ApiService'];

	function FaqController ($log, $state,
							   seoProvider, UserFactory, ApiService) {
		$log.log ('FaqController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');



	}

})(window.angular);










(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('ForgotPasswordController', ForgotPasswordController);

	ForgotPasswordController.$inject = ['$log', '$state', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'session'];

	function ForgotPasswordController ($log, $state, ngToast,
							 seoProvider, UserFactory, ApiService, session) {
		$log.log ('ForgotPasswordController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.forgotPassword = forgotPassword;

		vm.userData = UserFactory.getUserData();

		vm.user = {};

		function forgotPassword() {
			ApiService.forgotPassword(vm.userData.login).then(sendEmailCallback, null);
		}

		function sendEmailCallback(data){
			console.log("data:", data)
			if ((data.data.data)&&(!data.data.error)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Reset url was sent to your email'
				});
			}
		}

	}

})(window.angular);



(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .controller('CustomerProfileController', CustomerProfileController);

    CustomerProfileController.$inject = ['$log', '$scope', '$rootScope', '$stateParams',
        'seoProvider', 'ApiService', 'UserFactory', 'NotificationService', 'NOTIFICATION_EVENT', '$state'];

    function CustomerProfileController($log, $scope, $rootScope, $stateParams,
                                         seoProvider, ApiService, UserFactory, NotificationService, NOTIFICATION_EVENT, $state) {
        $log.log('CustomerProfileController');

        var vm = this;

        seoProvider.setTitle ('Zippy');
        seoProvider.setDescription ('Zippy');
        seoProvider.setKeywords ('Zippy');
        seoProvider.setAuthor ('Zippy');

        vm.onUserDataUpdated = onUserDataUpdated;
        vm.postGig = postGig;
        vm.selectedGig = selectedGig;

        vm.userData = UserFactory.getUserData();

        vm.jobTypesData = {};
        vm.selectedJob = '';
        vm.selectedHour = 0;
        vm.finishedGigs = [];
        vm.description = '';
        vm.api_img = 'http://host.zippygigs.com/api/v1/client';

        checkPermission();
        initData();
        getProfile();

        NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

        function onUserDataUpdated(){
            vm.userData = UserFactory.getUserData();
            initData();
        }

        function checkPermission(){
            if(vm.userData.type === '2' || vm.userData.type === 2){
                $state.transitionTo('home');
            }
        }

        function getProfile() {
            ApiService.getProfile($stateParams.customer_id).then(getProfileCallback, null);
        }

        function initData(){
            console.log("vm:", vm)
            if(vm.userData.login){
                checkPermission();
                vm.userData.type = String(vm.userData.type);
            }
        }

        ApiService.getJobTypes().then(getJobTypesCallback, null);
        ApiService.getOrders({status: 4, order_type: "customer"}).then(getOrdersCallback, null);

        function postGig(){
            $rootScope.midLoader = true;
            console.log(vm.selectedJob);
            console.log(vm.selectedHour);
            console.log(vm.description);
            var button = $(event.target);
            button.find(".success").fadeIn("slow");
            setTimeout(function(){
                button.find(".success").fadeOut("slow");
            }, 2000);
            $(event.target).parent().parent().find(".desc").toggle(500);
            ApiService.postGig({type: vm.selectedJob.id, description: vm.description, price: vm.selectedHour}).then(postGigCallback, null);
        }

        function selectedGig(item){
            vm.selectedGigItem = item;
        }

        //callbacks

        function getJobTypesCallback(resp){
            vm.jobTypesData = resp.data.data.job_types;
            vm.selectedJob = vm.jobTypesData[0];
        }

        function getOrdersCallback(resp){
            vm.finishedGigs = resp.data.data.orders;
            console.log(vm.finishedGigs);
            console.log(vm.jobTypesData);
        }

        function postGigCallback(resp){
            $rootScope.midLoader = false;
        }

        function getProfileCallback(resp) {
            vm.profile = resp.data.data;
            console.log("profile: ", resp)
        }

    }

})(window.angular);

(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('HomeController', HomeController);

	HomeController.$inject = ['$log', '$state',
		'seoProvider', 'UserFactory',  '$sce', '$location', '$anchorScroll'];

	function HomeController ($log, $state,
							 seoProvider, UserFactory, $sce, $location, $anchorScroll) {
		$log.log ('HomeController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.userData = UserFactory.getUser();

		vm.goToSearch = goToSearch;
		
		vm.config = {
                preload: 'none',
                sources: [{src: '../../assets/img/ZippyGigs - Intro.mp4', type: 'video/mp4'}],
                theme: {
          			url: 'https://unpkg.com/videogular@2.1.2/dist/themes/default/videogular.css'
                }
            };

		function goToSearch() {
			$state.go("search");
		}
	}

})(window.angular);







(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .controller('MembershipController', MembershipController);

    MembershipController.$inject = ['$log', '$state',
        'seoProvider', 'UserFactory', 'ApiService'];

    function MembershipController ($log, $state, seoProvider,
                                   UserFactory, ApiService) {
        $log.log ('MembershipController');

        var vm = this;

        seoProvider.setTitle ('Zippy');
        seoProvider.setDescription ('Zippy');
        seoProvider.setKeywords ('Zippy');
        seoProvider.setAuthor ('Zippy');

        vm.createSubscription = createSubscription;
        vm.goToGigs = goToGigs;
        vm.userData = UserFactory.getUserData();
        vm.plan = 1

        getBraintreeClientToken();

        function getBraintreeClientToken() {
            ApiService.getBraintreeClientToken().then(getBtTokenCallback, null);
        }

        function createSubscription() {
            console.log('vm.plan:', vm.plan)
            console.log('vm.nonce:', vm.nonce)
            ApiService.createSubscription({'plan': vm.plan, 'nonce': vm.nonce}).then(createSubscriptionCallback, null);
        }

        function getBtTokenCallback(resp) {
            console.log("resp: ", resp);
            vm.bt_client_token = resp.data.data;
            initBraintree()
        }

        function createSubscriptionCallback(resp) {
            console.log("createSubscriptionCallback:", resp)
            window.location.reload()
        }

        function goToGigs() {
            $state.transitionTo('mygigs')
        }

        function initBraintree() {
            var paypalButton = document.querySelector('.paypal-button');
                vm.paypalButton = paypalButton;
                // Create a client.
                braintree.client.create({
                    authorization: vm.bt_client_token
                }, function (clientErr, clientInstance) {
                    console.log("clientInstance:", clientInstance)
                    // Stop if there was a problem creating the client.
                    // This could happen if there is a network error or if the authorization
                    // is invalid.
                    if (clientErr) {
                        console.error('Error creating client:', clientErr);
                        return;
                    }

                    // Create a PayPal component.
                    braintree.paypal.create({
                        client: clientInstance
                    }, function (paypalErr, paypalInstance) {
                        console.log("here!")

                        // Stop if there was a problem creating PayPal.
                        // This could happen if there was a network error or if it's incorrectly
                        // configured.
                        if (paypalErr) {
                            console.error('Error creating PayPal:', paypalErr);
                            return;
                        }

                        // Enable the button.
                        paypalButton.removeAttribute('disabled');

                        // When the button is clicked, attempt to tokenize.
                        paypalButton.addEventListener('click', function (event) {
                            console.log("paypalInstance:", paypalInstance)
                            // Because tokenization opens a popup, this has to be called as a result of
                            // customer action, like clicking a button—you cannot call this at any time.
                            paypalInstance.tokenize({
                                flow: 'vault',
                            }, function (tokenizeErr, payload) {
                                // Stop if there was an error.
                                console.log("payload:", payload)
                                console.log("tokenizeErr:", tokenizeErr)
                                if (tokenizeErr) {
                                    if (tokenizeErr.type !== 'CUSTOMER') {
                                        console.error('Error tokenizing:', tokenizeErr);
                                    }
                                    return;
                                }

                                // Tokenization succeeded!
                                paypalButton.setAttribute('disabled', true);
                                console.log('Got a nonce! You should submit this to your server.');
                                console.log(payload.nonce);
                                vm.nonce = payload.nonce;
                                createSubscription()

                            });

                        }, false);

                    });

                });
        }

    }

})(window.angular);







(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('MygigsController', MygigsController);

  MygigsController.$inject = ['$log', '$state', '$scope',
		'seoProvider', 'UserFactory', 'ApiService', 'NotificationService', 'NOTIFICATION_EVENT'];

	function MygigsController ($log, $state, $scope,
							seoProvider, UserFactory, ApiService, NotificationService, NOTIFICATION_EVENT) {
		$log.log ('MygigsController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		//ViewFunctions

    	vm.onUserDataUpdated = onUserDataUpdated;
		vm.search = search;
		vm.selectJobType = selectJobType;
		vm.hire = hire;
		vm.createOrder = createOrder;
		vm.openCustomerProfile = openCustomerProfile;

		//ControllerData

        vm.userData = UserFactory.getUserData();

		vm.api_img = 'http://host.zippygigs.com/api/v1/client';
		vm.params = {
			radius: null,
			gig_type: null,
			price_gt: null,
			price_lt: null
		};

		vm.data = {};

        vm.gig_type = {};
		vm.gigs = {};
		vm.types = {};

		//InitFunctions

    function checkPermission(){
      if(vm.userData.type === '1' || vm.userData.type === 1){
        $state.transitionTo('home');
      }
    }
    checkPermission();

    NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

    function onUserDataUpdated(){
      vm.userData = UserFactory.getUserData();
      if(vm.userData.login){
        checkPermission();
      }
    }

	function openCustomerProfile(id) {
		$state.transitionTo('customer_profile', {'customer_id': id});
	}

    //FunctionDeclarations

		ApiService.getJobTypes().then(getJobTypesCallback, null);

		function selectJobType(gig_id, index, id, rate, event) {
			vm.hireData = {
				type: '',
				description: '',
				price: '',
				account: ''
			};
			if(vm.gigs[gig_id].job_types.length){
				if (vm.gigs[gig_id].job_types[index].id === id) {
					vm.gigs[gig_id].job = {
						type: id,
						price: rate,
						account: vm.userData.login
					};
				}
				else {
					vm.gigs[gig_id].job = {
						type: false,
						price: 10,
						account: vm.userData.login
					};
				}
			}
			$(event.target).parent().parent().find(".desc").toggle(500);
		}

		function search() {
		  vm.params.gig_type = vm.gig_type.id;
			ApiService.searchGigs(vm.params).then(searchCallback, null);
		}

		function hire(user, status) {
			vm.data.gig_id = user;
			vm.data.status_id = status;
			$(event.target).parent().parent().find(".desc").toggle(500);
		}

		function createOrder(){
			var button = $(event.target);
			button.find(".success").fadeIn("slow");
			setTimeout(function(){
				button.find(".success").fadeOut("slow");
			}, 2000);
			$(event.target).parent().parent().find(".desc").toggle(500);
			ApiService.createOrder(vm.data).then(createOrderCallback);
		}

//callbacks

		function getJobTypesCallback(data){
				var default_type = {
					id: null,
					title: '- Not Selected -'
				};

				vm.types = data.data.data.job_types;
				vm.types.unshift(default_type);
				search();
		}

		function searchHireCallback(){
			//TODO: move

		}

		function searchCallback(data){
			vm.gigs = data.data.data;
		}

		function createOrderCallback(){

		}

	}

})(window.angular);





(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('ResetPasswordController', ResetPasswordController);

	ResetPasswordController.$inject = ['$stateParams', '$log', '$state', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'session'];

	function ResetPasswordController ($stateParams, $log, $state, ngToast,
							 seoProvider, UserFactory, ApiService, session) {
		$log.log ('ResetPasswordController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.resetPassword = resetPassword;

		vm.userData = UserFactory.getUserData();

		vm.user = {};

		function resetPassword() {
			ApiService.resetPassword($stateParams.token, vm.userData.password).then(resetPasswordCallback, null);
		}

		function resetPasswordCallback(data){
			console.log("data:", data)
			if ((data.data.data)&&(!data.data.error)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Password changed!'
				});
				$state.go('signIn');
			}
		}

	}

})(window.angular);



(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('SearchController', SearchController);

	SearchController.$inject = ['$log', '$state', '$scope',
		'seoProvider', 'UserFactory', 'ApiService', 'NotificationService', 'NOTIFICATION_EVENT'];

	function SearchController ($log, $state, $scope,
							seoProvider, UserFactory, ApiService, NotificationService, NOTIFICATION_EVENT) {
		$log.log ('SearchController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		//ViewFunctions

    	vm.onUserDataUpdated = onUserDataUpdated;
		vm.search = search;
		vm.selectJobType = selectJobType;
		vm.hire = hire;
		vm.openVendorProfile = openVendorProfile;

		//ControllerData

    	vm.userData = UserFactory.getUserData();

		vm.api_img = 'http://host.zippygigs.com/api/v1/client';
		vm.params = {
			status: null,
			radius: null,
			job_type: null,
			hourly_rate_gt: null,
			hourly_rate_lt: null
		};

    	vm.job_type = {};
		vm.users = {};
		vm.types = {};

		//InitFunctions

    function checkPermission(){
      if(vm.userData.type === '2' || vm.userData.type === 2){
        $state.transitionTo('home');
      }
    }
    checkPermission();

    NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

    function onUserDataUpdated(){
      vm.userData = UserFactory.getUserData();
      if(vm.userData.login){
        checkPermission();
      }
    }

    //FunctionDeclarations

		ApiService.getJobTypes().then(getJobTypesCallback, null);

		function selectJobType(userid, index, id, rate, event) {
			vm.hireData = {
				type: '',
				description: '',
				price: '',
				account: ''
			};
			if(vm.users[userid].job_types.length){
				if (vm.users[userid].job_types[index].id === id) {
					vm.users[userid].job = {
						type: id,
						price: rate,
						account: vm.userData.login
					};
				}
				else {
					vm.users[userid].job = {
						type: false,
						price: 10,
						account: vm.userData.login
					};
				}
			}
			$(event.target).parent().parent().find(".desc").toggle(500);
		}

		function search() {
		    vm.params.job_type = vm.job_type.id;
			ApiService.search(vm.params).then(searchCallback, null);
		}

		function hire(user, event) {
			var button = $(event.target);
			button.find(".success").fadeIn("slow");
			setTimeout(function(){
				button.find(".success").fadeOut("slow");
			}, 2000);
			ApiService.searchHire(user.job).then(searchHireCallback, null);
		}

//callbacks

		function getJobTypesCallback(data){
				var default_type = {
					id: null,
					title: '- Not Selected -'
				};

				vm.types = data.data.data.job_types;
				vm.types.unshift(default_type);
				search();
		}

		function searchHireCallback(){
			//TODO: move

		}

		function searchCallback(data){
				vm.users = data.data.data;
			console.log("vm.users: ", vm.users)
		}

		function openVendorProfile(id) {
			$state.transitionTo('vendor_profile', {'vendor_id': id});
		}

	}

})(window.angular);





(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('SignInController', SignInController);

	SignInController.$inject = ['$log', '$state', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'session'];

	function SignInController ($log, $state, ngToast,
							 seoProvider, UserFactory, ApiService, session) {
		$log.log ('SignInController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.signIn = singIn;

		vm.userData = UserFactory.getUserData();

		vm.user = {};

		function singIn() {
			ApiService.signin(vm.userData.login, vm.user.password).then(signInCallback, null);
		}

		function signInCallback(data){
			console.log("data:", data)
			if ((!data.data.data)&&(data.data.error)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Name or Password is incorrect'
				});
			}
			else {
				session.create(data.data.data.token, vm.userData.login);
				$state.go('account');
			}
		}

	}

})(window.angular);






(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('SignUpController', SignUpController);

	SignUpController.$inject = ['$log', '$scope', '$rootScope', '$state', 'ngToast',
		'seoProvider', 'ApiService', '$location', 'UserFactory', 'session'];

	function SignUpController ($log, $scope, $rootScope, $state, ngToast,
							 seoProvider, ApiService, $location, UserFactory, session) {
		$log.log ('SignUpController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.signUp = signUp;

		vm.userData = UserFactory.getUserData();
		vm.user = {
			password: null,
			passwordConfirm: null,
			tos: null
		};

		$scope.login = null;
		$scope.password = null;
		$scope.password_2 = null;
		$scope.loggined = false;


		function signUp() {
			if ((vm.user.password == null)||(vm.user.passwordConfirm == null)||(vm.user.password !== vm.user.passwordConfirm)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Passwords mismatch'
				});
				return false;
			}
			if (vm.user.tos !== true) {
				ngToast.create({
				  className: 'danger',
				  content: 'You must accept Terms and Conditions'
				});
				return false;
			}
			if(vm.signUpForm.$valid){
				ApiService.signup(vm.userData.login, vm.user.password).then(signUpCallback, null);
			}
			else {
				ngToast.create({
				  className: 'danger',
				  content: 'Email required'
				});
			}
		}

		function signUpCallback(data){
			ApiService.signin(vm.userData.login, vm.user.password).then(signInCallback, null);
		}

		function signInCallback(data){
			vm.userData.token = data.data.data.token;

			UserFactory.setIsLoggedIn(true);
			UserFactory.setUserData(vm.userData);

			session.create(data.data.data.token, vm.userData.login);
			$state.go('account');
		}
	}

})(window.angular);









(function (angular) {
  'use strict';

  angular
    .module('zippyApp')
    .controller('VendorDashboardController', VendorDashboardController);

  VendorDashboardController.$inject = ['$log', '$scope', '$rootScope', '$timeout',
    'seoProvider', 'ApiService', 'UserFactory', 'NotificationService', 'NOTIFICATION_EVENT', '$state'];

  function VendorDashboardController($log, $scope, $rootScope, $timeout,
                                     seoProvider, ApiService, UserFactory, NotificationService, NOTIFICATION_EVENT, $state) {
    $log.log('VendorDashboardController');

    var vm = this;

    seoProvider.setTitle ('Zippy');
    seoProvider.setDescription ('Zippy');
    seoProvider.setKeywords ('Zippy');
    seoProvider.setAuthor ('Zippy');


    vm.onUserDataUpdated = onUserDataUpdated;
    vm.jobSelectEvent = jobSelectEvent;
    vm.deleteItem = deleteItem;
    vm.changeDescription = changeDescription;
    vm.changeStatus = changeStatus;

    var checkJobs = [];
    var unique = true;

    vm.userData = UserFactory.getUserData();
    console.log("vm.userData:", vm.userData)

    vm.jobTypesData = {};
    vm.selectedJob = '';
    vm.viewJobRate = [];
    vm.selectedHour = 0;
    vm.api_img = 'http://host.zippygigs.com/api/v1/client';


    checkPermission();
    initData();


    NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

    function onUserDataUpdated(){
      vm.userData = UserFactory.getUserData();
      initData();
    }

    function checkPermission(){
      if(vm.userData.type === '1' || vm.userData.type === 1){
        $state.transitionTo('home');
      }
    }

    function initData(){
      if(vm.userData.login){
        checkPermission();
        vm.userData.type = String(vm.userData.type);
        vm.status = vm.userData.vendor_status;
        vm.vendor_description = vm.userData.vendor_description;
        if(vm.userData.job_types.length){
          vm.viewJobRate = vm.userData.job_types;
          for (var i = vm.viewJobRate.length - 1; i >= 0; i--) {
            checkJobs.push(vm.viewJobRate[i].title);
          }
        }
      }
    }

    //

    ApiService.getJobTypes().then(getJobTypesCallback, null);

    function jobSelectEvent(){
      for (var i = 0; i < checkJobs.length; i++) {
        if (vm.selectedJob.title === checkJobs[i]) {
          unique = false;
        }
      }
      if (!unique) {
        unique = !unique;
        return false;
      }
      else {
        vm.selectedJob.hourly_rate = vm.selectedHour;
        vm.viewJobRate.push(vm.selectedJob);
        checkJobs.push(vm.selectedJob.title);
        ApiService.postVendorJobs({job_type_id: vm.selectedJob.id, hourly_rate: vm.selectedJob.hourly_rate});

      }

    }

    function deleteItem(point) {
      var index = vm.viewJobRate.indexOf(point);
      if (index !== -1) {
        vm.viewJobRate.splice(index, 1);
        checkJobs.splice(index, 1);
        ApiService.deleteVendorJobs({job_type_id: point.id});
      }
    }

    function changeDescription(){
      $rootScope.midLoader = true;
      ApiService.postVendorDescription({vendor_description: vm.vendor_description}).then(postVendorDescriptionCallback, null);
    }

    function changeStatus(state){
      vm.status = state;
      ApiService.postStatus({vendor_status:state});
    }

    //callbacks

    function getJobTypesCallback(resp){
      vm.jobTypesData = resp.data.data.job_types;
      vm.selectedJob = vm.jobTypesData[0];
    }

    function postVendorDescriptionCallback(resp){
      vm.vendor_description = resp.data.data.vendor_description;
      $timeout(function(){
          $rootScope.midLoader = false;
      }, 1000);
    }

  }

})(window.angular);

(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .controller('VendorProfileController', VendorProfileController);

    VendorProfileController.$inject = ['$log', '$scope', '$rootScope', '$timeout', '$stateParams',
        'seoProvider', 'ApiService', 'UserFactory', 'NotificationService', 'NOTIFICATION_EVENT', '$state'];

    function VendorProfileController($log, $scope, $rootScope, $timeout, $stateParams,
                                       seoProvider, ApiService, UserFactory, NotificationService, NOTIFICATION_EVENT, $state) {
        $log.log('VendorProfileController');

        var vm = this;

        seoProvider.setTitle ('Zippy');
        seoProvider.setDescription ('Zippy');
        seoProvider.setKeywords ('Zippy');
        seoProvider.setAuthor ('Zippy');


        vm.onUserDataUpdated = onUserDataUpdated;
        vm.jobSelectEvent = jobSelectEvent;
        vm.deleteItem = deleteItem;
        vm.changeDescription = changeDescription;
        vm.changeStatus = changeStatus;

        var checkJobs = [];
        var unique = true;

        vm.userData = UserFactory.getUserData();
        console.log("vm.userData:", vm.userData)

        vm.jobTypesData = {};
        vm.selectedJob = '';
        vm.viewJobRate = [];
        vm.selectedHour = 0;
        vm.api_img = 'http://host.zippygigs.com/api/v1/client';


        checkPermission();
        initData();
        getProfile();


        NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

        function getProfile() {
            ApiService.getProfile($stateParams.vendor_id).then(getProfileCallback, null);
        }
        function onUserDataUpdated(){
            vm.userData = UserFactory.getUserData();
            initData();
        }

        function checkPermission(){
            if(vm.userData.type === '1' || vm.userData.type === 1){
                $state.transitionTo('home');
            }
        }

        function initData(){
            if(vm.userData.login){
                checkPermission();
                vm.userData.type = String(vm.userData.type);
                vm.status = vm.userData.vendor_status;
                vm.vendor_description = vm.userData.vendor_description;
                if(vm.userData.job_types.length){
                    vm.viewJobRate = vm.userData.job_types;
                    for (var i = vm.viewJobRate.length - 1; i >= 0; i--) {
                        checkJobs.push(vm.viewJobRate[i].title);
                    }
                }
            }
        }

        //

        ApiService.getJobTypes().then(getJobTypesCallback, null);

        function jobSelectEvent(){
            for (var i = 0; i < checkJobs.length; i++) {
                if (vm.selectedJob.title === checkJobs[i]) {
                    unique = false;
                }
            }
            if (!unique) {
                unique = !unique;
                return false;
            }
            else {
                vm.selectedJob.hourly_rate = vm.selectedHour;
                vm.viewJobRate.push(vm.selectedJob);
                checkJobs.push(vm.selectedJob.title);
                ApiService.postVendorJobs({job_type_id: vm.selectedJob.id, hourly_rate: vm.selectedJob.hourly_rate});

            }

        }

        function deleteItem(point) {
            var index = vm.viewJobRate.indexOf(point);
            if (index !== -1) {
                vm.viewJobRate.splice(index, 1);
                checkJobs.splice(index, 1);
                ApiService.deleteVendorJobs({job_type_id: point.id});
            }
        }

        function changeDescription(){
            $rootScope.midLoader = true;
            ApiService.postVendorDescription({vendor_description: vm.vendor_description}).then(postVendorDescriptionCallback, null);
        }

        function changeStatus(state){
            vm.status = state;
            ApiService.postStatus({vendor_status:state});
        }

        //callbacks

        function getJobTypesCallback(resp){
            vm.jobTypesData = resp.data.data.job_types;
            vm.selectedJob = vm.jobTypesData[0];
        }

        function postVendorDescriptionCallback(resp){
            vm.vendor_description = resp.data.data.vendor_description;
            $timeout(function(){
                $rootScope.midLoader = false;
            }, 1000);
        }

        function getProfileCallback(resp) {
            vm.profile = resp.data.data;
            console.log("profile: ", resp)
        }

    }

})(window.angular);
