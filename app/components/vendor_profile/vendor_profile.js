(function (angular) {
    'use strict';

    angular
        .module('zippyApp')
        .controller('VendorProfileController', VendorProfileController);

    VendorProfileController.$inject = ['$log', '$scope', '$rootScope', '$timeout', '$stateParams',
        'seoProvider', 'ApiService', 'UserFactory', 'NotificationService', 'NOTIFICATION_EVENT', '$state'];

    function VendorProfileController($log, $scope, $rootScope, $timeout, $stateParams,
                                       seoProvider, ApiService, UserFactory, NotificationService, NOTIFICATION_EVENT, $state) {
        $log.log('VendorProfileController');

        var vm = this;

        seoProvider.setTitle ('Zippy');
        seoProvider.setDescription ('Zippy');
        seoProvider.setKeywords ('Zippy');
        seoProvider.setAuthor ('Zippy');


        vm.onUserDataUpdated = onUserDataUpdated;
        vm.jobSelectEvent = jobSelectEvent;
        vm.deleteItem = deleteItem;
        vm.changeDescription = changeDescription;
        vm.changeStatus = changeStatus;

        var checkJobs = [];
        var unique = true;

        vm.userData = UserFactory.getUserData();
        console.log("vm.userData:", vm.userData)

        vm.jobTypesData = {};
        vm.selectedJob = '';
        vm.viewJobRate = [];
        vm.selectedHour = 0;
        vm.api_img = 'http://host.zippygigs.com/api/v1/client';


        checkPermission();
        initData();
        getProfile();


        NotificationService.register($scope, NOTIFICATION_EVENT.USER_DATA_UPDATED, onUserDataUpdated);

        function getProfile() {
            ApiService.getProfile($stateParams.vendor_id).then(getProfileCallback, null);
        }
        function onUserDataUpdated(){
            vm.userData = UserFactory.getUserData();
            initData();
        }

        function checkPermission(){
            if(vm.userData.type === '1' || vm.userData.type === 1){
                $state.transitionTo('home');
            }
        }

        function initData(){
            if(vm.userData.login){
                checkPermission();
                vm.userData.type = String(vm.userData.type);
                vm.status = vm.userData.vendor_status;
                vm.vendor_description = vm.userData.vendor_description;
                if(vm.userData.job_types.length){
                    vm.viewJobRate = vm.userData.job_types;
                    for (var i = vm.viewJobRate.length - 1; i >= 0; i--) {
                        checkJobs.push(vm.viewJobRate[i].title);
                    }
                }
            }
        }

        //

        ApiService.getJobTypes().then(getJobTypesCallback, null);

        function jobSelectEvent(){
            for (var i = 0; i < checkJobs.length; i++) {
                if (vm.selectedJob.title === checkJobs[i]) {
                    unique = false;
                }
            }
            if (!unique) {
                unique = !unique;
                return false;
            }
            else {
                vm.selectedJob.hourly_rate = vm.selectedHour;
                vm.viewJobRate.push(vm.selectedJob);
                checkJobs.push(vm.selectedJob.title);
                ApiService.postVendorJobs({job_type_id: vm.selectedJob.id, hourly_rate: vm.selectedJob.hourly_rate});

            }

        }

        function deleteItem(point) {
            var index = vm.viewJobRate.indexOf(point);
            if (index !== -1) {
                vm.viewJobRate.splice(index, 1);
                checkJobs.splice(index, 1);
                ApiService.deleteVendorJobs({job_type_id: point.id});
            }
        }

        function changeDescription(){
            $rootScope.midLoader = true;
            ApiService.postVendorDescription({vendor_description: vm.vendor_description}).then(postVendorDescriptionCallback, null);
        }

        function changeStatus(state){
            vm.status = state;
            ApiService.postStatus({vendor_status:state});
        }

        //callbacks

        function getJobTypesCallback(resp){
            vm.jobTypesData = resp.data.data.job_types;
            vm.selectedJob = vm.jobTypesData[0];
        }

        function postVendorDescriptionCallback(resp){
            vm.vendor_description = resp.data.data.vendor_description;
            $timeout(function(){
                $rootScope.midLoader = false;
            }, 1000);
        }

        function getProfileCallback(resp) {
            vm.profile = resp.data.data;
            console.log("profile: ", resp)
        }

    }

})(window.angular);
