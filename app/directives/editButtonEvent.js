/**
 * Created by Eduard Latsevich on 001 01.12.16.
 */
angular
    .module('zippyApp')
    .directive('editButtonEvent', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                var el = element;
                element.click(function(){
                    var input = element.parent().find("input");

                    if (input.length === 0){
                        input = element.parent().find("select");
                    }

                    var disabled = input.prop('disabled');
                    input.prop('disabled', !disabled);
                    input.toggleClass("editable");
                    input.focus();

                    if (!input.hasClass("select")) {
                        var length = input.val().length;
                        input[0].setSelectionRange(length, length);
                    }
                });
            }
        };
    });