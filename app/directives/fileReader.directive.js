/**
 * Created by Eduard Latsevich on 001 01.12.16.
 */
angular
    .module('zippyApp')
    .directive("fileRead", [function () {
        return {
            scope: {
                fileRead: "="
            },
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.bind("change", function (changeEvent) {
                    var file = changeEvent.target.files[0];
                    var reader = new FileReader();

                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileRead = {
                                file: file,
                                data: loadEvent.target.result
                            };

                            if (ngModel) {
                                ngModel.$setViewValue(file);
                            }
                        });
                    };

                    reader.readAsDataURL(file);
                });
            }
        };
    }]);