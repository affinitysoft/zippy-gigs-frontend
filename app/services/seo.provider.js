/**
 * Created by Eduard Latsevich on 029 29.11.16.
 * unrealisted@gmail.com
 */
(function (angular) {
    'use strict';

    angular
        .module('seoProvider', [])
        .provider('seoProvider', function () {
            return {
                $get: ['$rootScope', 'seoProvider.helper', seoProvider]
            };
        })
        .factory("seoProvider.helper", [seoHelper]);

    function seoProvider($rootScope, stringHelperService) {

        $rootScope.seo = {
            title: '',
            description: '',
            keywords: '',
            author: ''
        };

        function setTitle(newTitle) {
            $rootScope.seo.title = newTitle;
        }

        function setDescription(description) {
            var result = '';
            if (description && description.length) {
                var softLength =
                    stringHelperService.getSoftLengthOfSentenceEnd(description, 255, true);
                result = description.slice(0, softLength ? softLength : 100).trim();
            }

            $rootScope.seo.description = result;
        }

        function setKeywords(keywords) {
            var result = '';
            if (keywords && keywords.length) {
                var softLength =
                    stringHelperService.getSoftLengthOfSentenceEnd(keywords, 100, true);
                result = keywords.slice(0, softLength ? softLength : 100).trim();
            }

            $rootScope.seo.keywords = result;
        }

        function setAuthor(author) {
            var result = '';
            if (author && author.length) {
                result = author.trim();
            }

            $rootScope.seo.author = result;
        }

        return {
            setTitle: setTitle,
            setDescription: setDescription,
            setKeywords: setKeywords,
            setAuthor: setAuthor
        };

    }

    function seoHelper() {

        function getSoftLengthOfWordEnd(string, preferLength, hardLimit) {

            if (!string || string.length < preferLength) {
                return preferLength;
            }

            var lastDotBefore = string.lastIndexOf(' ', preferLength);

            if (hardLimit) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                return false;
            }

            var firstDotAfter = string.indexOf(' ', preferLength);

            if (lastDotBefore === -1 || firstDotAfter === -1) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                if (firstDotAfter !== -1) {
                    return firstDotAfter + 1;
                }
                return false;
            }

            if (preferLength - lastDotBefore > firstDotAfter - preferLength) {
                return firstDotAfter + 1;
            }

            return lastDotBefore + 1;
        }

        function getSoftLengthOfSentenceEnd(string, preferLength, hardLimit) {

            if (!string || string.length < preferLength) {
                return preferLength;
            }

            var lastDotBefore = string.lastIndexOf('.', preferLength);

            if (lastDotBefore < string.lastIndexOf('!', preferLength)) {
                lastDotBefore = string.lastIndexOf('!', preferLength);
            }
            if (lastDotBefore < string.lastIndexOf('?', preferLength)) {
                lastDotBefore = string.lastIndexOf('?', preferLength);
            }

            if (hardLimit) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                return false;
            }

            var firstDotAfter = string.indexOf('.', preferLength);
            var firstExclamationBefore = string.indexOf('!', preferLength);
            if (firstExclamationBefore !== -1 && firstDotAfter > firstExclamationBefore) {
                firstDotAfter = firstExclamationBefore;
            }
            var firstQuestionBefore = string.indexOf('?', preferLength);
            if (firstQuestionBefore !== -1 && firstDotAfter > firstQuestionBefore) {
                firstDotAfter = firstQuestionBefore;
            }

            if (lastDotBefore === -1 || firstDotAfter === -1) {
                if (lastDotBefore !== -1) {
                    return lastDotBefore + 1;
                }
                if (firstDotAfter !== -1) {
                    return firstDotAfter + 1;
                }
                return false;
            }

            if (preferLength - lastDotBefore > firstDotAfter - preferLength) {
                return firstDotAfter + 1;
            }

            return lastDotBefore + 1;
        }


        return {
            getSoftLengthOfWordEnd: getSoftLengthOfWordEnd,
            getSoftLengthOfSentenceEnd: getSoftLengthOfSentenceEnd
        };

    }

})(window.angular);
