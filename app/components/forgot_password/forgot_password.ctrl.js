(function (angular) {
	'use strict';

	angular
		.module('zippyApp')
		.controller('ForgotPasswordController', ForgotPasswordController);

	ForgotPasswordController.$inject = ['$log', '$state', 'ngToast',
		'seoProvider', 'UserFactory', 'ApiService', 'session'];

	function ForgotPasswordController ($log, $state, ngToast,
							 seoProvider, UserFactory, ApiService, session) {
		$log.log ('ForgotPasswordController');

		var vm = this;

		seoProvider.setTitle ('Zippy');
		seoProvider.setDescription ('Zippy');
		seoProvider.setKeywords ('Zippy');
		seoProvider.setAuthor ('Zippy');

		vm.forgotPassword = forgotPassword;

		vm.userData = UserFactory.getUserData();

		vm.user = {};

		function forgotPassword() {
			ApiService.forgotPassword(vm.userData.login).then(sendEmailCallback, null);
		}

		function sendEmailCallback(data){
			console.log("data:", data)
			if ((data.data.data)&&(!data.data.error)) {
				ngToast.create({
				  className: 'danger',
				  content: 'Reset url was sent to your email'
				});
			}
		}

	}

})(window.angular);


