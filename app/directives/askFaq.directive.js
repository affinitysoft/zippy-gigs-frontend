/**
 * Created by Eduard Latsevich on 004 04.12.16.
 */
angular
  .module('zippyApp')
  .directive('askFaq', function() {
    return {
      restrict: 'AE',
      link: function(scope, element, attrs) {
        var el = element;

        if ( el ) {
          el.click(function(){
            el.parent().toggleClass("open");
            el.parent().find(".answer").toggle("slow");
          });
        }
      }
    };
  });